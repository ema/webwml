#use wml::debian::template title="Debian Mirrors" MAINPAGE="true"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Debian mirrors are maintained by volunteers. If you are in the position to donate disk space and connectivity, please consider creating a mirror to make Debian more accessible. For more information, please visit <a href="ftpmirror">this page</a>.<p>
</aside>

<p>Debian is distributed (aka mirrored) on hundreds of servers worldwide, all offering the same content.
This way we can provide better access to our archive.</p>

<p>You can find the following content on our mirror servers:</p>

<dl>
<dt><strong>Debian Packages</strong> (<code>debian/</code>)</dt>
  <dd>The Debian package pool: this includes the vast majority of <code>.deb</code>
      packages, installation material, and the sources.
      <br>
      See the list of <a href="list">Debian mirrors</a> that include the
      <code>debian/</code> archive.
  </dd>
<dt><strong>CD Images</strong> (<code>debian-cd/</code>)</dt>
  <dd>The repository of CD images: Jigdo files and ISO image files.
      <br>
      See the list of <a href="$(HOME)/CD/http-ftp/#mirrors">Debian mirrors</a> that
      include the <code>debian-cd/</code> archive.
  </dd>
<dt><strong>Old Releases</strong> (<code>debian-archive/</code>)</dt>
  <dd>The archive of older Debian versions, released in the past.
      <br>
      See <a href="$(HOME)/distrib/archive">Distribution archives</a>
      for more information.
  </dd>
</dl>
