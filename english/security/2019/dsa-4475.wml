<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Joran Dirk Greef discovered that overly long nonces used with
ChaCha20-Poly1305 were incorrectly processed and could result in nonce
reuse. This doesn't affect OpenSSL-internal uses of ChaCha20-Poly1305
such as TLS.</p>

<p>For the stable distribution (stretch), this problem has been fixed in
version 1.1.0k-1~deb9u1. This DSA also upgrades openssl1.0 (which
itself is not affected by <a href="https://security-tracker.debian.org/tracker/CVE-2019-1543">\
CVE-2019-1543</a>) to 1.0.2s-1~deb9u1</p>

<p>We recommend that you upgrade your openssl packages.</p>

<p>For the detailed security status of openssl please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/openssl">\
https://security-tracker.debian.org/tracker/openssl</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4475.data"
# $Id: $
