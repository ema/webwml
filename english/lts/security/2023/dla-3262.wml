<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a potential cross-site scripting
vulnerability in smarty3, a widely-used PHP templating engine.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-25047">CVE-2018-25047</a>

    <p>In Smarty before 3.1.47 and 4.x before 4.2.1,
    libs/plugins/function.mailto.php allows XSS. A web page that uses
    smarty_function_mailto, and that could be parameterized using GET or POST
    input parameters, could allow injection of JavaScript code by a
    user.</p></li>

</ul>

<p>For Debian 10 <q>Buster</q>, this problem has been fixed in version
3.1.33+20180830.1.3a78a21f+selfpack1-1+deb10u2.</p>

<p>We recommend that you upgrade your smarty3 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3262.data"
# $Id: $
