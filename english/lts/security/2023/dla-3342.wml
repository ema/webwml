<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several flaws were found in freeradius, a high-performance and highly
configurable RADIUS server.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-41859">CVE-2022-41859</a>

    <p>In freeradius, the EAP-PWD function compute_password_element() leaks
    information about the password which allows an attacker to substantially
    reduce the size of an offline dictionary attack.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-41860">CVE-2022-41860</a>

    <p>In freeradius, when an EAP-SIM supplicant sends an unknown SIM option, the
    server will try to look that option up in the internal dictionaries. This
    lookup will fail, but the SIM code will not check for that failure.
    Instead, it will dereference a NULL pointer, and cause the server to crash.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-41861">CVE-2022-41861</a>

    <p>A malicious RADIUS client or home server can send a malformed attribute
    which can cause the server to crash.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
3.0.17+dfsg-1.1+deb10u2.</p>

<p>We recommend that you upgrade your freeradius packages.</p>

<p>For the detailed security status of freeradius please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/freeradius">https://security-tracker.debian.org/tracker/freeradius</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3342.data"
# $Id: $
