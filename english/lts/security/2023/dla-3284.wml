<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>In Apache::Session::LDAP before 0.5, validity of the X.509 certificate
is not checked by default when connecting to remote LDAP backends,
because the default configuration of the Net::LDAPS module for Perl is
used.</p>

<p>This update changes the default behavior to require X.509 validation
against the distribution bundle <code>/etc/ssl/certs/ca-certificates.crt</code>.
Previous behavior can reverted by setting <code>ldapVerify =&gt; "none"</code> when
initializing the Apache::Session::LDAP object.
<p><b>Note</b>: this update is a prerequisite for LemonLDAP::NG's <a href="https://security-tracker.debian.org/tracker/CVE-2020-16093">CVE-2020-16093</a>
fix when its session backend is set to Apache::Session::LDAP.</p>

<p>For Debian 10 buster, this problem has been fixed in version
0.4-1+deb10u1.</p>

<p>We recommend that you upgrade your libapache-session-ldap-perl packages.</p>

<p>For the detailed security status of libapache-session-ldap-perl please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libapache-session-ldap-perl">https://security-tracker.debian.org/tracker/libapache-session-ldap-perl</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3284.data"
# $Id: $
