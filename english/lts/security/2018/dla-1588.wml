<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>A buffer overflow was discovered in the URL-authentication backend of
the icecast2, the popular open source streaming media server. If the
backend is enabled, then any malicious HTTP client can send a request
for specific resource including a crafted header which can overwrite
the server's stack contents, leading to denial of service and
potentially remote code execution.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
2.4.0-1.1+deb8u2.</p>

<p>We recommend that you upgrade your icecast2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1588.data"
# $Id: $
