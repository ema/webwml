<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in Squid, a proxy caching
server.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-28651">CVE-2021-28651</a>

    <p>Due to a buffer-management bug, it allows a denial of service.
    When resolving a request with the urn: scheme, the parser leaks a
    small amount of memory. However, there is an unspecified attack
    methodology that can easily trigger a large amount of memory
    consumption.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-28652">CVE-2021-28652</a>

    <p>Due to incorrect parser validation, it allows a Denial of Service
    attack against the Cache Manager API. This allows a trusted client
    to trigger memory leaks that. over time, lead to a Denial of
    Service via an unspecified short query string. This attack is
    limited to clients with Cache Manager API access privilege.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-31806">CVE-2021-31806</a>

    <p>Due to a memory-management bug, it is vulnerable to a Denial of
    Service attack (against all clients using the proxy) via HTTP
    Range request processing.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-31807">CVE-2021-31807</a>

    <p>An integer overflow problem allows a remote server to achieve
    Denial of Service when delivering responses to HTTP Range
    requests. The issue trigger is a header that can be expected to
    exist in HTTP traffic without any malicious intent.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-31808">CVE-2021-31808</a>

    <p>Due to an input-validation bug, it is vulnerable to a Denial of
    Service attack (against all clients using the proxy). A client
    sends an HTTP Range request to trigger this.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-33620">CVE-2021-33620</a>

    <p>Remote servers to cause a denial of service (affecting
    availability to all clients) via an HTTP response. The issue
    trigger is a header that can be expected to exist in HTTP traffic
    without any malicious intent by the server.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
3.5.23-5+deb9u7.</p>

<p>We recommend that you upgrade your squid3 packages.</p>

<p>For the detailed security status of squid3 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/squid3">https://security-tracker.debian.org/tracker/squid3</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2685.data"
# $Id: $
