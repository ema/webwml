<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several problems were corrected in TagLib,
a library for reading and editing audio meta data.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-12678">CVE-2017-12678</a>

    <p>A crafted audio file could result in a crash.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-11439">CVE-2018-11439</a>

    <p>A crafted audio file could result in information disclosure.</p></li>

<li><p>Additionally, a bug that can lead to corruption of ogg files
has been fixed.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
1.11.1+dfsg.1-0.3+deb9u1.</p>

<p>We recommend that you upgrade your taglib packages.</p>

<p>For the detailed security status of taglib please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/taglib">https://security-tracker.debian.org/tracker/taglib</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2772.data"
# $Id: $
