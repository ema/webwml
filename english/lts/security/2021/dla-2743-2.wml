<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>DLA-2743-1 was issued for <a href="https://security-tracker.debian.org/tracker/CVE-2017-5715">CVE-2017-5715</a>, affecting amd64-microcode,
processor microcode firmware for AMD CPUs. However, the binaries for
the resulting upload weren't built and published, thereby preventing
the users to upgrade to a fixed version.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
3.20181128.1~deb9u2.</p>

<p>We recommend that you upgrade your amd64-microcode packages.</p>

<p>For the detailed security status of amd64-microcode please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/amd64-microcode">https://security-tracker.debian.org/tracker/amd64-microcode</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2743-2.data"
# $Id: $
