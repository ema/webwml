<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Roman Fiedler found that libX11, the X11 protocol client library, was
vulnerable to protocol command injection due to insufficient validation
of arguments to some functions.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
2:1.6.4-3+deb9u4.</p>

<p>We recommend that you upgrade your libx11 packages.</p>

<p>For the detailed security status of libx11 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libx11">https://security-tracker.debian.org/tracker/libx11</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2666.data"
# $Id: $
