<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several issues have been found in faad2, a freeware Advanced Audio Decoder
player. They are related to heap buffer overflows or null pointer
dereferences, which both might allow an attacker to execute code by
providing crafted files.</p>


<p>For Debian 9 stretch, these problems have been fixed in version
2.8.0~cvs20161113-1+deb9u3.</p>

<p>We recommend that you upgrade your faad2 packages.</p>

<p>For the detailed security status of faad2 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/faad2">https://security-tracker.debian.org/tracker/faad2</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2792.data"
# $Id: $
