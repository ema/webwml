<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two vulnerabilities were fixed in monit, a utility for monitoring and
managing Unix systems.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11454">CVE-2019-11454</a>

    <p>Persistent cross-site scripting in http/cervlet.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11455">CVE-2019-11455</a>

    <p>Buffer over-read in Util_urlDecode in util.c</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
1:5.20.0-6+deb9u2.</p>

<p>We recommend that you upgrade your monit packages.</p>

<p>For the detailed security status of monit please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/monit">https://security-tracker.debian.org/tracker/monit</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2855.data"
# $Id: $
