<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in the Go programming
language. An attacker could trigger a denial-of-service (DoS), bypasss
access control, and execute arbitrary code on the developer's
computer.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15041">CVE-2017-15041</a>

     <p>Go allows <q>go get</q> remote command execution. Using custom
     domains, it is possible to arrange things so that
     example.com/pkg1 points to a Subversion repository but
     example.com/pkg1/pkg2 points to a Git repository. If the
     Subversion repository includes a Git checkout in its pkg2
     directory and some other work is done to ensure the proper
     ordering of operations, <q>go get</q> can be tricked into reusing this
     Git checkout for the fetch of code from pkg2. If the Subversion
     repository's Git checkout has malicious commands in .git/hooks/,
     they will execute on the system running "go get."</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16873">CVE-2018-16873</a>

    <p>The <q>go get</q> command is vulnerable to remote code execution when
    executed with the -u flag and the import path of a malicious Go
    package, as it may treat the parent directory as a Git repository
    root, containing malicious configuration.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16874">CVE-2018-16874</a>

    <p>The <q>go get</q> command is vulnerable to directory traversal when
    executed with the import path of a malicious Go package which
    contains curly braces (both '{' and '}' characters). The attacker
    can cause an arbitrary filesystem write, which can lead to code
    execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9741">CVE-2019-9741</a>

    <p>In net/http, CRLF injection is possible if the attacker controls a
    url parameter, as demonstrated by the second argument to
    http.NewRequest with \r\n followed by an HTTP header or a Redis
    command.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16276">CVE-2019-16276</a>

    <p>Go allows HTTP Request Smuggling.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17596">CVE-2019-17596</a>

    <p>Go can panic upon an attempt to process network traffic containing
    an invalid DSA public key. There are several attack scenarios,
    such as traffic from a client to a server that verifies client
    certificates.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3114">CVE-2021-3114</a>

    <p>crypto/elliptic/p224.go can generate incorrect outputs, related to
    an underflow of the lowest limb during the final complete
    reduction in the P-224 field.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
1.7.4-2+deb9u3.</p>

<p>We recommend that you upgrade your golang-1.7 packages.</p>

<p>For the detailed security status of golang-1.7 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/golang-1.7">https://security-tracker.debian.org/tracker/golang-1.7</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2591.data"
# $Id: $
