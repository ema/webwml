<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An issue has been found in graphicsmagick, a collection of image
processing tools.
Due to missing checks, a crafted MIFF file could result in a heap buffer
overflow when parsing it.</p>


<p>For Debian 10 buster, this problem has been fixed in version
1.4+really1.3.35-1~deb10u3.</p>

<p>We recommend that you upgrade your graphicsmagick packages.</p>

<p>For the detailed security status of graphicsmagick please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/graphicsmagick">https://security-tracker.debian.org/tracker/graphicsmagick</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3200.data"
# $Id: $
