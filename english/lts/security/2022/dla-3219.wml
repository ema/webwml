<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Jhead, a tool for manipulating EXIF data embedded in JPEG images, allowed
attackers to execute arbitrary OS commands by placing them in a JPEG filename
and then using the regeneration -rgt50, -autorot or -ce option. In addition a
buffer overflow error in exif.c has been addressed which could lead to a denial
of service (application crash).</p>

<p>For Debian 10 buster, these problems have been fixed in version
1:3.00-8+deb10u1.</p>

<p>We recommend that you upgrade your jhead packages.</p>

<p>For the detailed security status of jhead please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/jhead">https://security-tracker.debian.org/tracker/jhead</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3219.data"
# $Id: $
