<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities have been discovered in isync, an IMAP and
MailDir mailbox synchronizer. An malicious attacker who can control an IMAP
server may exploit these flaws for remote code execution.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
1.2.1-2+deb9u1.</p>

<p>We recommend that you upgrade your isync packages.</p>

<p>For the detailed security status of isync please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/isync">https://security-tracker.debian.org/tracker/isync</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3066.data"
# $Id: $
