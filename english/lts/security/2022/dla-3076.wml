<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A command injection vulnerability was found in FreeCAD, a parametric 3D
modeler, when importing DWG files with crafted filenames.</p>

<p>For Debian 10 buster, this problem has been fixed in version
0.18~pre1+dfsg1-5+deb10u1.</p>

<p>We recommend that you upgrade your freecad packages.</p>

<p>For the detailed security status of freecad please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/freecad">https://security-tracker.debian.org/tracker/freecad</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3076.data"
# $Id: $
