<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that the Cyrus IMAP server was prone to a denial of service
attack via input that is mishandled during hash-table interaction. Furthermore
it allowed privilege escalation because an HTTP request may be interpreted in
the authentication context of an unrelated previous request that arrived over
the same connection.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
2.5.10-3+deb9u3.</p>

<p>We recommend that you upgrade your cyrus-imapd packages.</p>

<p>For the detailed security status of cyrus-imapd please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/cyrus-imapd">https://security-tracker.debian.org/tracker/cyrus-imapd</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3052.data"
# $Id: $
