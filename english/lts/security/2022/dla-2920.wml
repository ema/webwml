<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>James Kettle discovered that a request smuggling attack can be performed on
HTTP/1 connections on Varnish servers, high-performance web accelerators. The
smuggled request would be treated as an additional request by the Varnish
server which may lead to information disclosure and cache poisoning.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
5.0.0-7+deb9u3.</p>

<p>We recommend that you upgrade your varnish packages.</p>

<p>For the detailed security status of varnish please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/varnish">https://security-tracker.debian.org/tracker/varnish</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2920.data"
# $Id: $
