<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A file traversal vulnerability was discovered in src:ruby-sinatra, a
popular web server often used with Ruby on Rails. We now validate that
any expanded paths match the allowed `public_dir` when serving static
files.</p>

<p>For Debian 10 buster, this problem has been fixed in version
2.0.5-4+deb10u1.</p>

<p>We recommend that you upgrade your ruby-sinatra packages.</p>

<p>For the detailed security status of ruby-sinatra please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/ruby-sinatra">https://security-tracker.debian.org/tracker/ruby-sinatra</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3166.data"
# $Id: $
