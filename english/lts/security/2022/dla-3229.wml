<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>In node-log4js, a port of log4js in Node.js, default file permissions
for log files created by the file, fileSync, and dateFile appenders are
world-readable. This could cause problems if log files contain sensitive
information. This would affect any users that have not supplied their
own permissions for the files via the mode parameter in the config.</p>

<p>For Debian 10 buster, this problem has been fixed in version
4.0.2-2+deb10u1.</p>

<p>We recommend that you upgrade your node-log4js packages.</p>

<p>For the detailed security status of node-log4js please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/node-log4js">https://security-tracker.debian.org/tracker/node-log4js</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3229.data"
# $Id: $
