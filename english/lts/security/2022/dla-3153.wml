<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>An integer overflow flaw was discovered in the CRL parser in libksba, an
X.509 and CMS support library, which could result in denial of service or
the execution of arbitrary code.</p>

<p>For Debian 10 buster, this problem has been fixed in version
1.3.5-2+deb10u1.</p>

<p>We recommend that you upgrade your libksba packages.</p>

<p>For the detailed security status of libksba please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/libksba">https://security-tracker.debian.org/tracker/libksba</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3153.data"
# $Id: $
