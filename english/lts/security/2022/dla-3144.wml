<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in ConnMan, a network manager
for embedded devices, which could result in denial of service or the
execution of arbitrary code.</p>

<p>This update also fixes an issue with reference counting in the
<a href="https://security-tracker.debian.org/tracker/CVE-2022-32293">CVE-2022-32293</a> fix introduced in DLA-3105-1.</p>

<p>For Debian 10 buster, these problems have been fixed in version
1.36-2.1~deb10u4.</p>

<p>We recommend that you upgrade your connman packages.</p>

<p>For the detailed security status of connman please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/connman">https://security-tracker.debian.org/tracker/connman</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3144.data"
# $Id: $
