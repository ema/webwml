<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities were discovered in inetutils, a
collection of common network programs.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0053">CVE-2019-0053</a>

    <p>inetutils' telnet client doesn't sufficiently validate environment
    variables, which can lead to stack-based buffer overflows.  This
    issue is limited to local exploitation from restricted shells.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-40491">CVE-2021-40491</a>

    <p>inetutils' ftp client before 2.2 does not validate addresses
    returned by PASV/LSPV responses to make sure they match the server
    address.  A malicious server can exploit this flaw to reach services
    in the client's private network.  (This is similar to curl's
    <a href="https://security-tracker.debian.org/tracker/CVE-2020-8284">CVE-2020-8284</a>.)</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-39028">CVE-2022-39028</a>

    <p>inetutils's telnet server through 2.3 has a NULL pointer dereference
    which a client can trigger by sending 0xff 0xf7 or 0xff 0xf8.  In a
    typical installation, the telnetd application would crash but the
    telnet service would remain available through inetd.  However, if the
    telnetd application has many crashes within a short time interval,
    the telnet service would become unavailable after inetd logs a
    "telnet/tcp server failing (looping), service terminated" error.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
2:1.9.4-7+deb10u2.</p>

<p>We recommend that you upgrade your inetutils packages.</p>

<p>For the detailed security status of inetutils please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/inetutils">https://security-tracker.debian.org/tracker/inetutils</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3205.data"
# $Id: $
