<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was found that Barbican, a service for secret management and storage,
was vulnerable to access bypass via query string injection.</p>

<p>For Debian 10 buster, this problem has been fixed in version
1:7.0.0-1+deb10u1.</p>

<p>We recommend that you upgrade your barbican packages.</p>

<p>For the detailed security status of barbican please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/barbican">https://security-tracker.debian.org/tracker/barbican</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3136.data"
# $Id: $
