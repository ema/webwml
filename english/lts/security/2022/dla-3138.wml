<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in BIND, a DNS server
implementation.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2795">CVE-2022-2795</a>

    <p>Yehuda Afek, Anat Bremler-Barr and Shani Stajnrod discovered that a
    flaw in the resolver code can cause named to spend excessive amounts
    of time on processing large delegations, significantly degrade
    resolver performance and result in denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-38177">CVE-2022-38177</a>

    <p>It was discovered that the DNSSEC verification code for the ECDSA
    algorithm is susceptible to a memory leak flaw. A remote attacker
    can take advantage of this flaw to cause BIND to consume resources,
    resulting in a denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-38178">CVE-2022-38178</a>

    <p>It was discovered that the DNSSEC verification code for the EdDSA
    algorithm is susceptible to a memory leak flaw. A remote attacker
    can take advantage of this flaw to cause BIND to consume resources,
    resulting in a denial of service.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
1:9.11.5.P4+dfsg-5.1+deb10u8.</p>

<p>We recommend that you upgrade your bind9 packages.</p>

<p>For the detailed security status of bind9 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/bind9">https://security-tracker.debian.org/tracker/bind9</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3138.data"
# $Id: $
