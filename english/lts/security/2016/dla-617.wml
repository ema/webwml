<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several security vulnerabilities have been discovered in libarchive,
a multi-format archive and compression library. An attacker could
take advantage of these flaws to cause an out of bounds read or a
denial of service against an application using the libarchive12
library using a carefully crafted input file.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8915">CVE-2015-8915</a>

    <p>Paris Zoumpouloglou of Project Zero labs discovered a flaw in
    libarchive bsdtar. Using a crafted file bsdtar can perform an
    out-of-bounds memory read which will lead to a SEGFAULT.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7166">CVE-2016-7166</a>

    <p>Alexander Cherepanov discovered a flaw in libarchive compression
    handling. Using a crafted gzip file, one can get libarchive to
    invoke an infinite chain of gzip compressors until all the memory
    has been exhausted or another resource limit kicks in.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.0.4-3+wheezy3.</p>

<p>We recommend that you upgrade your libarchive packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-617.data"
# $Id: $
