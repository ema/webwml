<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was an issue in node-ini, a .ini format parser
and serializer for Node.js, where an application could be exploited by a
malicious input file.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-7788">CVE-2020-7788</a>

    <p>This affects the package ini before 1.3.6. If an attacker submits a
    malicious INI file to an application that parses it with ini.parse, they
    will pollute the prototype on the application. This can be exploited
    further depending on the context.</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
1.1.0-1+deb9u1.</p>

<p>We recommend that you upgrade your node-ini packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2503.data"
# $Id: $
