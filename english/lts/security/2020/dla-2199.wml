<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>A vulnerability was discovered in OpenLDAP, a free implementation of the
Lightweight Directory Access Protocol. LDAP search filters with nested
boolean expressions can result in denial of service (slapd daemon
crash).</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
2.4.40+dfsg-1+deb8u6.</p>

<p>We recommend that you upgrade your openldap packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2199.data"
# $Id: $
