<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An issue has been found in luajit, a just in time compiler for Lua.</p>

<p>An out-of-bounds read could happen because __gc handler frame traversal is
mishandled.</p>


<p>For Debian 9 stretch, this problem has been fixed in version
2.0.4+dfsg-1+deb9u1.</p>

<p>We recommend that you upgrade your luajit packages.</p>

<p>For the detailed security status of luajit please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/luajit">https://security-tracker.debian.org/tracker/luajit</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2296.data"
# $Id: $
