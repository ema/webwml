<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>A vulnerability was discovered in python-bleach, a whitelist-based
HTML-sanitizing library.  Calls to bleach.clean with an allowed tag with
an allowed style attribute are vulnerable to a regular expression denial
of service (ReDoS).</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.4-1+deb8u1.</p>

<p>We recommend that you upgrade your python-bleach packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2167.data"
# $Id: $
