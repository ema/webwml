<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A regression was introduced in DLA-2332-1, where changes in the Debian
package building process triggered a bug in the sane-backends
packages, causing missing files.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1.0.25-4.1+deb9u2.</p>

<p>We recommend that you upgrade your sane-backends packages.</p>

<p>For the detailed security status of sane-backends please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/sane-backends">https://security-tracker.debian.org/tracker/sane-backends</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2332-2.data"
# $Id: $
