<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Three issues have been found in php5, a server-side, HTML-embedded
scripting language.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-7064">CVE-2020-7064</a>

      <p>A one byte out-of-bounds read, which could potentially lead to
      information disclosure or crash.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-7066">CVE-2020-7066</a>

      <p>An URL containing zero (\0) character will be truncated at it, which
      may cause some software to make incorrect assumptions and possibly
      send some information to a wrong server.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-7067">CVE-2020-7067</a>

      <p>Using a malformed url-encoded string an Out-of-Bounds read can occur.</p>


<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
5.6.40+dfsg-0+deb8u11.</p>

<p>We recommend that you upgrade your php5 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2188.data"
# $Id: $
