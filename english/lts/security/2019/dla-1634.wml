<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several issues in wireshark, a tool that captures and analyzes packets
off the wire, have been found by different people.
These are basically issues with length checks or invalid memory access in
different dissectors. This could result in infinite loops or crashes by
malicious packets.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.12.1+g01b65bf-4+deb8u16.</p>

<p>We recommend that you upgrade your wireshark packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1634.data"
# $Id: $
