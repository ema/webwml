<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Volker Lendecke of SerNet and the Samba team discovered that Samba, a
SMB/CIFS file, print, and login server for Unix, is prone to a heap
memory information leak, where server allocated heap memory may be
returned to the client without being cleared.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2:3.6.6-6+deb7u15.</p>

<p>We recommend that you upgrade your samba packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1183.data"
# $Id: $
