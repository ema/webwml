# vendors.CD - CD Vendor list

#use wml::debian::common_translation
#include "$(ENGLISHDIR)/CD/vendors/vendors.CD.def"

# WARNING: When you edit this file, make sure to test your changes!
# The vendors.CD.def file will *abort* with an error if
#  - the entries of a country are not sorted alphabetically
#  - <vendor country="xx"> is not inside the <country code="xx"> element
#    (the duplicated country information makes mistakes less probable when editing the file)

# this is to fix a bug with WML 1.7.4 which makes it very slow on large files
<protect pass=5>

<country code="ar" name="<country-name AR>">
<vendor name="lavorato IT"
	url="https://lavorato.wordpress.com/"
	deburl="https://lavorato.wordpress.com/debian/"
	contacturl="mailto:lavoratoit@gmail.com"
	country="ar"
	contribution="no"
	ship="no"
	dvd="yes"
	architectures="amd64 i386" />
<vendor name="SW Computacion"
	country="ar"
	url="http://swcomputacion.com/"
	deburl="http://swcomputacion.com/dvdDebian.php"
	contribution="yes"
	cd="no"
	dvd="yes"
	architectures="i386, amd64"
	ship="no"
	contacturl="mailto:administracion@swcomputacion.com" />
</country>

#<country code="at" name="<country-name AT>">
#</country>

#<country code="au" name="<country-name AU>">
#</country>

#<country code="bg" name="<country-name BG>">
#</country>

<country code="br" name="<country-name BR>">
<vendor name="Hudson Oliveira"
        url="https://www.freewebs.com/corsario/"
        deburl="https://www.freewebs.com/corsario/"
        contacturl="mailto:hudsonoliveira@outlook.com"
        country="br"
        contribution="yes"
        ship="yes"
        dvd="yes"
        architectures="multi-arch, amd64, i386" />
</country>

#<country code="by" name="<country-name BY>">
#</country>

<country code="ca" name="<country-name CA>">
<vendor name="Stormfront Ventures"
        url="https://www.jbox.ca"
        deburl="https://www.jbox.ca/product-category/computers-tablets/software/linux-cds-dvds/debian-cds-dvds/"
        contacturl="mailto:sales@jbox.ca"
        country="ca"
        contribution="yes"
        ship="yes"
        cd="yes"
        dvd="yes"
        architectures="amd64, i386" />
</country>

<country code="ch" name="<country-name CH>">
<vendor name="Dominique E. Blake-Hofer"
	url="https://blake-hofer.net"
	deburl="https://blake-hofer.net/?post_type=product"
	contacturl="mailto:info@blake-hofer.net"
	country="ch"
	contribution="yes"
	ship="yes"
	cd="yes"
	architectures="amd64, i386" />
</country>

#<country code="cl" name="<country-name CL>">
#</country>

#<country code="cn" name="<country-name CN>">
#</country>

#<country code="co" name="<country-name CO>">
#</country>

#<country code="cr" name="<country-name CR>">
#</country>

#<country code="cz" name="<country-name CZ>">
#</country>

<country code="de" name="<country-name DE>">
<vendor name="Debianland.de"
        country="de"
        url="https://www.debianland.de/"
        deburl="https://www.debianland.de/index.php?page=articles"
        contribution="yes"
        cd="yes"
        dvd="yes"
        architectures="multi-arch, i386, amd64, <source>"
        ship="europe"
        contacturl="mailto:busgefahren@gmx.de" />
<vendor name="Dr. Hinner EDV"
	url="http://www.hinner.de/linux"
	deburl="http://www.hinner.de/linux"
	contacturl="mailto:infot@hinner.de"
	country="de"
	contribution="yes"
	ship="yes"
	cd="yes"
	dvd="yes"
	architectures="ALL " />
<vendor name="Druckerzwerge.de"
        url="https://www.Druckerzwerge-Shop.de/"
        deburl="https://www.druckerzwerge-shop.de/advanced_search_result.php?keywords=debian"
        contacturl="mailto:info@druckerzwerge.de"
        country="de"
        contribution="no"
        ship="no"
        dvd="yes"
        architectures="amd64, i386" />
<vendor name="Joomla Webdesign Homburg"
        url="https://www.joomla-programmieren.de/"
        deburl="https://www.joomla-programmieren.de/debian-linux.html"
        contacturl="mailto:info@joomla-programmieren.de"
        country="de"
        contribution="yes"
        ship="no"
        dvd="yes"
        architectures="amd64, i386" />
<vendor name="Linux-Onlineshop"
        url="https://www.linux-onlineshop.de"
        deburl="https://www.linux-onlineshop.de/Software-Buecher/ISO-Distris/Debian.geek"
        contacturl="mailto:linux@linux-onlineshop.de"
        country="de"
        contribution="yes"
        ship="yes"
        cd="no"
        dvd="yes"
        bd="no"
        usb="yes"
        architectures="amd64, i386, powerpc" />
<vendor name="Tintenalarm.de"
        country="de"
        url="https://www.tintenalarm.de/"
        deburl="https://www.tintenalarm.de/advanced_search_result.php?keywords=debian"
        contribution="yes"
        cd="yes"
        dvd="yes"
        architectures="amd64, arm, i386, ia64, mips, mipsel, powerpc, s390, sparc, <source>"
        ship="europe"
        contacturl="mailto:info@tintenalarm.de" />
<vendor name="vianos webdesign"
        url="https://www.vianos.de"
        deburl="https://www.vianos.de/debian.html"
        contacturl="mailto:kontakt@vianos.de"
        country="de"
        contribution="yes"
        ship="europe"
        dvd="yes"
        architectures="multi-arch, amd64, i386" />
</country>

#<country code="dk" name="<country-name DK>">
#</country>

#<country code="es" name="<country-name ES>">
#</country>

#<country code="fi" name="<country-name FI>">
#</country>

<country code="fr" name="<country-name FR>">
<vendor name="GetLinux"
        url="http://www.getlinux.fr"
        deburl="https://www.getlinux.fr/index.php?route=product/manufacturer/info&amp;manufacturer_id=11"
        contacturl="mailto:contact@getlinux.fr"
        country="fr"
        contribution="no"
        ship="yes"
        dvd="yes"
        architectures="multi-arch, amd64, i386, powerpc, <source>" />
<vendor name="Hypra"
	url="https://www.hypra.fr"
	deburl="https://www.hypra.fr/Notre-catalogue-logiciel.html"
	contacturl="mailto:contact@hypra.fr"
	country="fr"
	contribution="no"
	ship="yes"
	cd="yes"
	dvd="yes"
	architectures="ALL" />
</country>

#<country code="gr" name="<country-name GR>">
#</country>

# <country code="hk" name="<country-name HK>">
# </country>

#<country code="hu" name="<country-name HU>">
#</country>

<country code="id" name="<country-name ID>">
<vendor name="Toko Baliwae - Tokonya Maniak Linux"
        country="id"
        url="https://toko.baliwae.com/"
        deburl="https://toko.baliwae.com/advanced_search_result.php?keywords=debian"
        contribution="no"
        cd="yes"
        dvd="yes"
        architectures="i386"
        ship="yes"
        contacturl="mailto:toko@baliwae.com" />
</country>

#<country code="il" name="<country-name IL>">
#</country>

#<country code="in" name="<country-name IN>">
#</country>


#<country code="ir" name="<country-name IR>">
#</country>

<country code="it" name="<country-name IT>">
<vendor name="Laser Office sas"
        country="it"
        url="https://www.laseroffice.it/"
        deburl="https://www.laseroffice.it/eshop/"
        contribution="yes"
        cd="yes"
        dvd="yes"
        architectures="amd64, i386, <source>"
        ship="europe"
        contacturl="mailto:alex@laseroffice.it" />
</country>

<country code="jo" name="<country-name JO>">
</country>

<country code="jp" name="<country-name JP>">
<vendor name="env-reform.com"
        url="http://env-reform.com"
        deburl="http://env-reform.com/cart/"
        contacturl="http://env-reform.com/cart/index.php?main_page=contact_us"
        #contacturl="mailto:akuroiwa@env-reform.com"
        country="jp"
        contribution="no"
        ship="no"
        cd="no"
		dvd="yes"
        architectures="i386, amd64"/>
</country>

#<country code="kz" name="<country-name KZ>">
#</country>

#<country code="mx" name="<country-name MX>">
#</country>

#<country code="my" name="<country-name MY>">
#</country>

<country code="nl" name="<country-name NL>">
<vendor name="De Linuxspecialist"
        country="nl"
        url="http://www.delinuxspecialist.nl/"
        deburl="http://www.delinuxspecialist.nl/index.php?page=debian"
        contribution="yes"
        cd="yes"
        dvd="yes"
        architectures="alpha, arm, i386, m68k, powerpc, sparc, <source>"
        ship="yes"
        contacturl="mailto:onbetwist@delinuxspecialist.nl" />
<vendor name="Dutch Debian Distribution Initiative"
        country="nl"
        url="http://www.dddi.nl/"
        deburl="http://www.dddi.nl/"
        contribution="yes"
        cd="yes"
        dvd="yes"
        bd="yes"
        usb="yes"
        architectures="ALL"
        ship="europe"
        contacturl="mailto:info@dddi.nl" />
<vendor name="Paul van der Vlis Linux systeembeheer"
		country="nl"
		url="https://vandervlis.nl/"
		deburl="https://vandervlis.nl/media.html"
		contacturl="mailto:paul@vandervlis.nl"
		contribution="yes"
		ship="yes"
		cd="yes"
		dvd="yes"
		bd="no"
		usb="yes"
		architectures="ALL" />
</country>

<country code="nz" name="<country-name NZ>">
</country>

#<country code="pe" name="<country-name PE>">
#</country>

#<country code="pk" name="<country-name PK>">
#</country>

#<country code="pl" name="<country-name PL>">
#</country>

#<country code="pt" name="<country-name PT>">
#</country>

#<country code="ro" name="<country-name RO>">
#</country>

<country code="ru" name="<country-name RU>">
<vendor name="GNU/Linuxcenter"
	url="https://www.linuxcenter.shop"
	deburl="https://linuxcenter.shop/collection/distributivy-na-usb-i-dvd"
	contacturl="mailto:info@linuxcenter.shop"
	country="ru"
	contribution="yes"
	ship="no"
	cd="no"
	dvd="yes"
	bd="no"
	usb="yes"
	architectures="amd64 i386" />
<vendor name="Linux2u "
	url="https://linux2u.ru"
	deburl="https://linux2u.ru/debian.html"
	contacturl="mailto:info@linux2u.ru"
	country="ru"
	contribution="no"
	ship="some"
	cd="yes"
	dvd="yes"
	architectures="amd64 i386" />
</country>

#<country code="se" name="<country-name SE>">
#</country>

#<country code="sk" name="<country-name SK>">
#</country>

#<country code="ua" name="<country-name UA>">
#</country>

<country code="gb" name="<country-name GB>">
<Vendor name="Onyx Dragon"
        url="https://onyxdragon.co.uk/"
        deburl="https://onyxdragon.co.uk/product-category/linux/"
        contacturl="mailto:sarah@reford.co.uk"
        country="gb"
        contribution="no"
        ship="yes"
        cd="yes"
        dvd="yes"
        architectures="amd64 i386 <source>" />
<vendor name="Steve McIntyre"
        country="gb"
        url="http://www.einval.com/~steve/DebianCD/"
        deburl="http://www.einval.com/~steve/DebianCD/"
        contribution="yes"
        cd="yes"
        dvd="yes"
        bd="yes"
        usb="yes"
        architectures="ALL"
        ship="yes"
        contacturl="mailto:steve@einval.com" />
<vendor name="thelinuxshop"
        country="gb"
        url="http://www.thelinuxshop.co.uk/"
        deburl="http://www.thelinuxshop.co.uk/catalog/advanced_search_result.php?keywords=debian"
        contribution="no"
        cd="yes"
        dvd="no"
        architectures="i386"
        ship="no"
        contacturl="mailto:sales@thelinuxshop.co.uk" />
</country>

<country code="us" name="<country-name US>">
<vendor name="LinuxCollections.com"
        url="http://www.linuxcollections.com"
        deburl="http://www.linuxcollections.com/products/debian/debian.htm"
        contacturl="mailto:info@linuxcollections.com"
        country="us"
        contribution="yes"
        ship="yes"
        cd="yes"
        dvd="yes"
        bd="no"
        usb="yes"
        architectures="amd64, i386, ia64, powerpc, <source>, kfreebsd-i386, kfreebsd-amd64"/>
<vendor name="ShopLinuxOnLine.com"
        url="https://www.shoplinuxonline.com"
        deburl="https://www.shoplinuxonline.com/linux-discs/debian-linux.html"
        contacturl="mailto:sales@shoplinuxonline.com"
        country="us"
        contribution="no"
        ship="yes"
        dvd="yes"
		bd="no"
		usb="yes"
        architectures="amd64, i386, ppc64el, <source>" />
</country>

<country code="uy" name="<country-name UY>">
<vendor name="PM Consultores Ltda"
        url="http://pmc.com.uy"
        deburl="http://pmc.com.uy/debian.html"
        contacturl="mailto:ventas@pmc.com.uy"
        country="uy"
        contribution="no"
        ship="yes"
        cd="yes"
        dvd="yes"
        architectures="amd64, i386" />
</country>

#<country code="ve" name="<country-name VE>">
#</country>

#<country code="za" name="<country-name ZA>">
#</country>

</protect>
