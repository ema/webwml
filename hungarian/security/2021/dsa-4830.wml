#use wml::debian::translation-check translation="cc173b8d34b89c7d43e8628759e88ae4a67b7db9" maintainer="Szabolcs Siebenhofer"
<define-tag description>biztonsági frissítés</define-tag>
<define-tag moreinfo>
<p>Simon McVittie felfedezett egy hibát a flatpak-portal szolgáltatásban,
ami lehetővé teszi a homokveremben (sandbox) futattott alkalmazásnak, 
hogy tetszőleges kódot futasson a gazda rendszeren (sandbox escape).</p>

<p>A Flatpak portál D-Bus szolgáltatás (flatpak-portal, vagy más néven
org.freedesktop.portal.Flatpak-ként ismerik a D-Bus szolgáltatását)
lehetővé teszi a Flatpak homokveremben futtatott alkalmazásoknak, hogy 
saját alfolyamatot indítsanak egy új homokveremben, ugyanolyan vagy 
szigorúbb biztonsági beállításokkal, mint a hívó. Például, ezt használják
a Flatpak csomagolású böngészőkben, mint például a Chromium, olyan
alfolyamatok elindítására, melyek nem megbítható webtartalmat dolgoznak
fel és ezeknek az alfolyamatoknak szigorúbb homokvermet adnak, mint maga
a böngésző.</p>

<p>A sebezhető verziókban a Flatpak portál szolgáltatás átadja a hívó-specifikus
környezeti változókat egy nem homokveremben futó folyamatnak a gazda
rendszeren, különösen a flatpak run parancsnak, ami új homokverem példányok
indítására használatos. Gyanús vagy kompromittált Flatpak alkalmazás olyan
környezeti változókat állíthat be, amelyben a flatpak run parancs megbízik,
és tetszőleges parancs futtatására használhatja a homokvermen kívül.</p>

<p>A stabil kiadásban (buster) ez a probléma javításra került a 1.2.5-0+deb10u2
verzióban.</p>

<p>Azt tanácsoljuk, hogy frissítsd a flatpak csomagjaidat.</p>

<p>A flatpak részletes biztonsági állapotáért keresd fel a biztonsági 
nyomkövető oldalát:
<a href="https://security-tracker.debian.org/tracker/flatpak">\
https://security-tracker.debian.org/tracker/flatpak</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4830.data"
# $Id: $
