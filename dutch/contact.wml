#use wml::debian::template title="Contact opnemen" NOCOMMENTS="yes" MAINPAGE="true"
#use wml::debian::translation-check translation="fb050641b19d0940223934350c51061fcb2439a9"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#generalinfo">Algemene informatie</a></li>
  <li><a href="#installuse">Debian installeren en gebruiken</a></li>
  <li><a href="#press">Publiciteit &amp; pers</a></li>
  <li><a href="#events">Evenementen &amp; conferenties</a></li>
  <li><a href="#helping">Debian helpen</a></li>
  <li><a href="#packageproblems">Problemen in Debian-pakketten melden</a></li>
  <li><a href="#development">Ontwikkeling van Debian</a></li>
  <li><a href="#infrastructure">Problemen met de infrastructuur van Debian</a></li>
  <li><a href="#harassment">Pesterijen</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Wij verzoeken u vriendelijk om een vraag aanvankelijk in het <strong>Engels</strong> te stellen, aangezien dit de taal is die de meesten van ons spreken. Indien dit niet mogelijk is, vraag dan om hulp op een van onze <a href="https://lists.debian.org/users.html#debian-user">mailinglijsten voor gebruikers</a>, welke in veel verschillende talen beschikbaar zijn.</p>
</aside>

<p>
Debian is een grote organisatie en er zijn verschillende manieren om in contact te komen met leden van het project en andere gebruikers van Debian. Deze pagina geeft een overzicht van contactmogelijkheden waar vaak naar gevraagd wordt; zij is geenszins volledig. Raadpleeg de rest van de webpagina's voor andere contactmethoden.
</p>

<p>
Houd er rekening mee dat de meeste van de onderstaande e-mailadressen adressen zijn van open mailinglijsten met openbare archieven. Lees de <a href="$(HOME)/MailingLists/disclaimer">niet-aansprakelijkheidsverklaring</a> voordat u een bericht verzendt.
</p>

<h2 id="generalinfo">Algemene informatie</h2>

<p>
De meeste informatie over Debian werd verzameld op onze webpagina,
<a href="$(HOME)">https://www.debian.org/</a>. Doorblader ze en <a href="$(SEARCH)">doorzoek</a> ze voordat u met ons contact opneemt.
</p>

<p>
U kunt vragen over het Debian-project sturen naar de mailinglijst <email debian-project@lists.debian.org>. Stel geen algemene vragen over Linux op deze lijst; lees verder voor meer informatie over andere mailinglijsten.
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="doc/user-manuals#faq">Lees onze FAQ</a></button></p>

<aside class="light">
  <span class="fa fa-envelope fa-5x"></span>
</aside>
<h2 id="installuse">Debian installeren en gebruiken</h2>

<p>
Als u zeker weet dat de documentatie op de installatiemedia en op onze website geen oplossing biedt voor uw probleem, is er een zeer actieve gebruikers-mailinglijst waar Debian-gebruikers en -ontwikkelaars uw vragen kunnen beantwoorden. Hier kunt u allerlei vragen stellen over de volgende onderwerpen:
</p>

<ul>
  <li>Installatie</li>
  <li>Configuratie</li>
  <li>Ondersteunde hardware</li>
  <li>Machinebeheer</li>
  <li>Debian gebruiken</li>
</ul>

<p>
<a href="https://lists.debian.org/debian-user/">Teken in</a> op de lijst en stuur uw vragen naar <email debian-user@lists.debian.org>.
</p>

<p>
Verder zijn er nog gebruikersmailinglijsten in verschillende talen. Raadpleeg de informatie over het intekenen op <a href="https://lists.debian.org/users.html#debian-user">internationale mailinglijsten</a>.
</p>

<p>
U kunt het <a href="https://lists.debian.org/">archief van onze mailinglijsten</a> doorbladeren of <a href="https://lists.debian.org/search.html">zoeken</a> in de archieven zonder dat u erop moet intekenen.
</p>

<p>
Bovendien kunt u onze mailinglijsten als nieuwsgroepen doorbladeren.
</p>

<p>
Als u denkt dat u een bug in ons installatiesysteem gevonden heeft, schrijf dan naar de mailinglijst <email debian-boot@lists.debian.org>. U kunt ook een <a href="$(HOME)/releases/stable/i386/ch05s04#submit-bug">bugrapport</a> indienen tegen het pseudo-pakket <a href="https://bugs.debian.org/debian-installer">debian-installer</a>.
</p>

<aside class="light">
  <span class="far fa-newspaper fa-5x"></span>
</aside>
<h2 id="press">Publiciteit &amp; pers</h2>

<p>
Het <a href="https://wiki.debian.org/Teams/Publicity">Publiciteitsteam van Debian</a> modereert nieuws en aankondigingen in alle officiële bronnen van Debian, bijvoorbeeld sommige mailinglijsten, de blog, de micronews-website, en de officiële kanalen in sociale media.
</p>

<p>
Als u over Debian schrijft en hulp of informatie nodig hebt, neem dan contact op met het <a href="mailto:press@debian.org">publiciteitsteam</a>. Ook als u nieuws wilt aanleveren voor onze eigen nieuwspagina bent u hier aan het juiste adres.
</p>

<aside class="light">
  <span class="fas fa-handshake fa-5x"></span>
</aside>
<h2 id="events">Evenementen &amp; conferenties</h2>

<p>
Stuur uitnodigingen voor <a href="$(HOME)/events/">conferenties</a>, tentoonstellingen of andere evenementen naar <email events@debian.org>.
</p>

<p>
Aanvragen voor folders, posters en deelnames in Europa moeten worden gestuurd naar de mailinglijst <email debian-events-eu@lists.debian.org>.
</p>

<aside class="light">
  <span class="fas fa-hands-helping fa-5x"></span>
</aside>
<h2 id="helping">Debian helpen</h2>

<p>
Er zijn tal van mogelijkheden om het Debian-project te ondersteunen en bij te dragen aan de distributie. Als u hulp wilt bieden, ga dan eerst naar onze pagina over <a href="devel/join/">Lid worden van Debian</a>.
</p>

<p>
Als u een spiegelserver voor Debian wilt onderhouden, raadpleeg dan de pagina's over <a href="mirror/">Debian spiegelen</a>. Nieuwe spiegelservers worden aangemeld via <a href="mirror/submit">dit formulier</a>. Problemen met bestaande spiegelserver kunnen worden gemeld op <email mirrors@debian.org>.
</p>

<p>
Als u Debian-cd's/dvd's wilt verkopen, raadpleeg dan de <a href="CD/vendors/info">informatie voor cd-verkopers</a>. Gebruik <a href="CD/vendors/adding-form">dit formulier</a> om vermeld te worden op de lijst met cd-leveranciers.
</p>

<aside class="light">
  <span class="fas fa-bug fa-5x"></span>
</aside>
<h2 id="packageproblems">Problemen in Debian-pakketten melden</h2>

<p>
Als u een bug wilt indienen tegen een Debian-pakket, dan hebben we een bugvolgsysteem ter beschikking waar u gemakkelijk uw probleem kunt melden. Raadpleeg de <a href="Bugs/Reporting">instructies voor het indienen van bugrapporten</a>.
</p>

<p>
Als u gewoon wilt communiceren met de beheerder van een Debian-pakket, dan kunt u de speciale e-mailaliassen gebruiken die voor elk pakket zijn ingesteld. Elke e-mail die wordt verzonden naar &lt;<var>pakketnaam</var>&gt;@packages.debian.org zal worden doorgestuurd naar de beheerder die verantwoordelijk is voor dat pakket.
</p>

<p>
Als u de ontwikkelaars op een discrete manier attent wilt maken op een beveiligingsprobleem in Debian, stuur dan een e-mail naar <email security@debian.org>.
</p>

<aside class="light">
  <span class="fas fa-code-branch fa-5x"></span>
</aside>
<h2 id="development">Ontwikkeling van Debian</h2>

<p>
Als u vragen heeft over de ontwikkeling van Debian, dan hebben we daar verschillende <a href="https://lists.debian.org/devel.html">mailinglijsten</a> voor. Hier kunt u in contact komen met onze ontwikkelaars.
</p>

<p>
Er is ook een algemene mailinglijst voor ontwikkelaars: <email debian-devel@lists.debian.org>.
Volg deze <a href="https://lists.debian.org/debian-devel/">link</a> om u erop te abonneren.
</p>

<aside class="light">
  <span class="fas fa-network-wired fa-5x"></span>
</aside>
<h2 id="infrastructure">Problemen met de infrastructuur van Debian</h2>

<p>
Om een probleem met een dienst van Debian te melden, kunt u meestal <a href="Bugs/Reporting">een bug rapporteren</a> tegen het betreffende <a href="Bugs/pseudo-packages">pseudo-pakket</a>.
</p>

<p>U kunt ook een e-mail sturen naar een van de volgende adressen:</p>

<define-tag btsurl>pakket: <a href="https://bugs.debian.org/%0">%0</a></define-tag>

<dl>
<dt>Redacteuren van de webpagina's</dt>
  <dd><btsurl www.debian.org><br />
      <email debian-www@lists.debian.org></dd>
#include "$(ENGLISHDIR)/devel/website/tc.data"
<ifneq "$(CUR_LANG)" "English" "
<dt>Vertalers van de webpagina's</dt>
  <dd><: &list_translators($CUR_LANG); :></dd>
">
<dt>Beheerders van de mailinglijsten en -archieven</dt>
  <dd><btsurl lists.debian.org><br />
      <email listmaster@lists.debian.org></dd>
<dt>Beheerders van het bugvolgsysteem</dt>
  <dd><btsurl bugs.debian.org><br />
      <email owner@bugs.debian.org></dd>
</dl>

<aside class="light">
  <span class="fas fa-umbrella fa-5x"></span>
</aside>
<h2 id="harassment">Pesterijen</h2>

<p>
Debian is een gemeenschap van mensen die veel waarde hechten aan respect. Als u het slachtoffer bent van ongepast gedrag, als iemand in het project u schade heeft berokkend, of als u zich lastiggevallen voelt, neem dan contact op met het Gemeenschapsteam: <email community@debian.org>
</p>
