#use wml::debian::template title="Wat betekent vrij?" NOHEADER="yes"
#use wml::debian::translation-check translation="2de158b0e259c30eefd06baf983e4930de3d7412"

# Last translation update by: $Author$
# Last translation update at: $Date$

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#freesoftware">Vrij zoals in...?</a></li>
    <li><a href="#licenses">Softwarelicenties</a></li>
    <li><a href="#choose">Hoe een licentie kiezen?</a></li>
  </ul>
</div>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> In februari 1998 kwam een groep mensen ervoor op om de term <a href="https://www.gnu.org/philosophy/free-sw">Vrije Software</a> te vervangen door <a href="https://opensource.org/docs/definition.html">Openbronsoftware</a>. Het debat over de terminologie weerspiegelt de onderliggende filosofische verschillen, maar de praktische vereisten en andere zaken die op deze site worden besproken, zijn in wezen dezelfde voor vrije software en openbronsoftware.</p>
</aside>


<h2><a id="freesoftware">Vrij zoals in...?</a></h2>

<p>
Veel mensen die niet vertrouwd zijn met dit onderwerp zijn verward door het
woord "vrij". Het wordt niet gebruikt zoals zij verwachten - "vrij" betekent
voor hen "zonder kosten". Als men in een Engels woordenboek kijkt, staan er
bijna twintig verschillende betekenissen voor "free" ("vrij"), en slechts één
daarvan is "kosteloos". De rest verwijst naar "vrijheid" en "ontbreken van
beperkingen". Dus, wanneer we spreken over vrije software, bedoelen we
vrijheid, niet vrij van betaling.
</p>

<p>
Software die als vrij omschreven wordt, maar die alleen vrij is in de betekenis
dat u er niet moet voor betalen, is nauwelijks vrij. Mogelijk is het u verboden
om ze verder te verspreiden en u mag ze vrijwel zeker niet zelf wijzigen.
Software die gratis in licentie wordt gegeven, is over het
algemeen een wapen in een marketingcampagne om een ander, gerelateerd
product aan te prijzen of om een kleinere concurrent uit de markt te
drukken. Er is geen enkele garantie dat de software altijd gratis zal
blijven.
</p>

<p>
Voor niet-ingewijden is software ofwel vrij of niet. In het echte leven is het
echter veel gecompliceerder dan dat. Om te begrijpen wat mensen bedoelen
wanneer ze het over vrije software hebben, moeten we een kleine uitstap maken
en de wereld van softwarelicenties binnentreden.
</p>

<h2><a id="licenses">Softwarelicenties</a></h2>

<p>
Copyright is een methode om de rechten van de maker van bepaalde
werken te beschermen. In de meeste landen is nieuwe software
automatisch auteursrechtelijk beschermd. Een
licentie is de manier waarop een auteur aan anderen toestemming geeft
om zijn/haar creatie (in dit geval software) te gebruiken op manieren
die voor hem/haar acceptabel zijn. Het is aan de auteur om een licentie toe te
voegen die verklaart hoe een stukje software mag worden gebruikt.
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="https://www.copyright.gov/">Meer lezen over copyright</a></button></p>

<p>
Verschillende omstandigheden vragen natuurlijk om verschillende licenties.
Softwarebedrijven zijn op zoek naar manieren om hun activa te beschermen, en
daarom geven zij vaak alleen gecompileerde code vrij die niet leesbaar is voor
mensen. Zij leggen ook veel beperkingen op aan het gebruik van de software.
Auteurs van vrije software daarentegen zijn meestal op zoek naar andere regels,
soms zelfs een combinatie van de volgende punten:
</p>

<ul>
  <li>Het is niet toegestaan hun code te gebruiken in gepatenteerde software. Aangezien zij hun software vrijgeven zodat iedereen ze kan gebruiken, willen zij niet dat anderen ze stelen. In dit geval wordt het gebruik van de code gezien als een vertrouwensband: men mag ze gebruiken, zolang men zich aan dezelfde regels houdt.</li>
  <li>De identiteit van de auteur moet worden beschermd. Mensen zijn erg trots op hun werk en willen niet dat anderen hun vermelding als auteur verwijderen of zelfs beweren dat zij het zelf hebben geschreven.</li>
  <li>Broncode moet worden verspreid. Een groot probleem met gepatenteerde software is dat u geen bugs kunt repareren of de software niet kunt aanpassen, aangezien de broncode niet beschikbaar is. Ook kan een leverancier besluiten te stoppen met het ondersteunen van de hardware van de gebruiker. Verspreiding van broncode, zoals de meeste vrije licenties voorschrijven, beschermt de gebruikers door hen in staat te stellen de software te wijzigen en aan te passen aan hun behoeften.</li>
  <li>Elk werk dat (een deel van) het werk van de auteur bevat (dit wordt in copyrightdiscussies ook "afgeleide werken" genoemd), moet dezelfde licentie gebruiken.</li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Drie van de meest gebruikte vrijesoftwarelicenties zijn de <a href="https://www.gnu.org/copyleft/gpl.html">GNU General Public License (GPL)</a> (GNU Algemene Publieke Licentie), de <a href="https://opensource.org/licenses/artistic-license.php">Artistic License</a> (Artistieke Licentie) en de <a href="https://opensource.org/licenses/BSD-3-Clause">BSD Style License</a> (BSD-achtige Licentie).
</aside>

<h2><a id="choose">Hoe een licentie kiezen?</a></h2>

<p>
Soms schrijven mensen hun eigen licentie, hetgeen problematisch kan zijn, en
dus wordt dit afgekeurd in de vrijesoftwaregemeenschap. Te vaak is de
formulering dubbelzinnig, of stelt men voorwaarden die met elkaar in strijd
zijn. Het schrijven van een licentie die stand houdt in de rechtbank is nog
moeilijker. Gelukkig zijn er een aantal openbronlicenties beschikbaar waaruit
men kan kiezen. Ze hebben de volgende dingen gemeen:
</p>

<ul>
  <li>Gebruikers mogen de software installeren op zoveel machines als ze willen.</li>
  <li>Om het even welk aantal mensen mag de software tegelijkertijd gebruiken.</li>
  <li>Gebruikers mogen zoveel kopieën van de software maken als ze willen of nodig hebben en deze kopieën ook aan andere gebruikers geven (vrije of open herverdeling).</li>
  <li>Er zijn geen beperkingen voor het wijzigen van de software (behalve voor het behouden van bepaalde auteursvermeldingen).</li>
  <li>Gebruikers mogen de software niet alleen verspreiden, maar zelfs verkopen.</li>
</ul>

<p>
Vooral het laatste punt, dat mensen toestaat de software te verkopen, lijkt in
te gaan tegen het hele idee van vrije software, maar het is juist één van de
sterke punten ervan. Omdat de licentie gratis herverdeling toestaat, kan iemand
die een kopie heeft verkregen deze ook verspreiden. Mensen kunnen zelfs
proberen ze te verkopen.
</p>

<p>
Hoewel vrije software niet helemaal vrij van beperkingen is, geeft het de
gebruiker de flexibiliteit om te doen wat nodig is om zijn werk gedaan te
krijgen. Tegelijkertijd worden de rechten van de auteur beschermd - dat is pas
vrijheid. Het Debian-project en zijn leden zijn grote voorstanders van vrije
software. We hebben de <a href="../social_contract#guidelines">Debian
Richtlijnen voor Vrije Software (Debian Free Software Guidelines - DFSG)</a>
opgesteld om met een redelijke definitie te komen van wat volgens ons vrije
software is. Alleen software die aan de DFSG voldoet, wordt
toegelaten tot de hoofddistributie (main) van Debian.
</p>

