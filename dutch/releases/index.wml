#use wml::debian::template title="Releases van Debian"
#use wml::debian::translation-check translation="2b2b2d98876137a0efdabdfc2abad6088d4c511f"
#include "$(ENGLISHDIR)/releases/info"

<p>Debian heeft altijd tenminste drie releases die actief worden onderhouden:
<q>stable</q>, <q>testing</q> en <q>unstable</q>.</p>

<dl>
<dt><a href="stable/">stable</a> (stabiel)</dt>
<dd>
<p>
  De distributie <q>stable</q> is de meest recente, officieel uitgebrachte
  distributie van Debian.
</p>
<p>
  Dit is de release voor productiviteit, de release die wij u vooral
  aanbevelen te gebruiken.
</p>
<p>
  De huidige <q>stable</q>-distributie van Debian is versie
  <:=substr '<current_initial_release>', 0, 2:>, met de codenaam
  <em><current_release_name></em>.
<ifeq "<current_initial_release>" "<current_release>"
  "Zij werd uitgebracht op <current_release_date>."
/>
<ifneq "<current_initial_release>" "<current_release>"
  "Zij werd voor het eerst uitgebracht als versie <current_initial_release>
  op <current_initial_release_date> en de meest recente versie,
  versie <current_release>, werd uitgebracht op <current_release_date>."
/>
</p>
</dd>

<dt><a href="testing/">testing</a> (in de testfase)</dt>
<dd>
<p>
  De <q>testing</q>-distributie bevat pakketten die nog
  niet in de <q>stable</q>-release zijn opgenomen, maar die wel
  daarvoor in de rij staan. Deze distributie heeft hoofdzakelijk het
  voordeel dat zij recentere versies van software bevat.
</p>
<p>
  Zie de <a href="$(DOC)/manuals/debian-faq/">Debian FAQ</a> voor meer informatie over
  <a href="$(DOC)/manuals/debian-faq/ftparchives#testing">wat <q>testing</q> is</a>
  en <a href="$(DOC)/manuals/debian-faq/ftparchives#frozen">hoe het
  <q>stable</q> wordt</a>.
</p>
<p>
  De huidige <q>testing</q>-distributie is <em><current_testing_name></em>.
</p>
</dd>

<dt><a href="unstable">unstable</a> (onstabiel)</dt>
<dd>
<p>
  De distributie <q>unstable</q> is het ontwikkelfront van Debian. Over
  het algemeen wordt deze distributie gebruikt door ontwikkelaars en door
  degenen die graag voortdurend de allernieuwste programmatuur willen
  gebruiken
  en de risico's die daaraan verbonden zijn, kunnen accepteren. Personen
  welke unstable gebruiken wordt aanbevolen in te tekenen op de mailinglijst
  debian-devel-announce om meldingen van belangrijke wijzigingen te ontvangen,
  bijvoorbeeld van opwaarderingen die tot defecten kunnen leiden.
</p>
<p>
  De distributie <q>unstable</q> heeft steeds <em>sid</em> als naam.
</p>
</dd>
</dl>

<h2>Levenscyclus van een release</h2>
<p>
  Debian kondigt regelmatig een nieuwe stabiele release aan. Gebruikers
  kunnen 3 jaar volledige ondersteuning voor elke stabiele release
  verwachten, en daarna nog eens 2 jaar LTS-ondersteuning.
</p>

<p>
  Raadpleeg voor gedetailleerde informatie de wikipagina's
  <a href="https://wiki.debian.org/DebianReleases">Debian Releases</a>
  en <a href="https://wiki.debian.org/LTS">Debian LTS</a>.
</p>

<h2>Lijst van releases</h2>
<ul>
  <li><a href="<current_testing_name>/">De volgende versie van Debian heeft de codenaam
      <q><current_testing_name></q></a>
      &mdash; <q>testing</q>, er werd nog geen releasedatum bepaald
  </li>
  <li><a href="bullseye/">Debian 11 (<q>bullseye</q>)</a>
      &mdash; huidige <q>stable</q> (stabiele) release
  </li>
  <li><a href="buster/">Debian 10 (<q>buster</q>)</a>
      &mdash; huidige <q>oldstable</q> (verouderde stabiele) release
  </li>
  <li><a href="stretch/">Debian 9 (<q>stretch</q>)</a>
      &mdash; <q>oldoldstable</q> (nog meer verouderde stabiele) release, met
      <a href="https://wiki.debian.org/LTS">LTS-ondersteuning</a>
      (langetermijnondersteuning)
  </li>
  <li><a href="jessie/">Debian 8 (<q>jessie</q>)</a>
      &mdash; gearchiveerde release, met
 <a href="https://wiki.debian.org/LTS/Extended">verlengde LTS-ondersteuning</a>
      (langetermijnondersteuning)
  </li>
  <li><a href="wheezy/">Debian 7 (<q>wheezy</q>)</a>
      &mdash; achterhaalde stabiele release
  </li>
  <li><a href="squeeze/">Debian 6.0 (<q>squeeze</q>)</a>
      &mdash; achterhaalde stabiele release
  </li>
  <li><a href="lenny/">Debian GNU/Linux 5.0 (<q>lenny</q>)</a>
      &mdash; achterhaalde stabiele release
  </li>
  <li><a href="etch/">Debian GNU/Linux 4.0 (<q>etch</q>)</a>
      &mdash; achterhaalde stabiele release
  </li>
  <li><a href="sarge/">Debian GNU/Linux 3.1 (<q>sarge</q>)</a>
      &mdash; achterhaalde stabiele release
  </li>
  <li><a href="woody/">Debian GNU/Linux 3.0 (<q>woody</q>)</a>
      &mdash; achterhaalde stabiele release
  </li>
  <li><a href="potato/">Debian GNU/Linux 2.2 (<q>potato</q>)</a>
      &mdash; achterhaalde stabiele release
  </li>
  <li><a href="slink/">Debian GNU/Linux 2.1 (<q>slink</q>)</a>
      &mdash; achterhaalde stabiele release
  </li>
  <li><a href="hamm/">Debian GNU/Linux 2.0 (<q>hamm</q>)</a>
      &mdash; achterhaalde stabiele release
  </li>
</ul>

<p>De internetpagina's voor de achterhaalde Debian-releases worden in stand
gehouden, maar de releases zelf kunnen alleen gevonden worden in een apart
<a href="$(HOME)/distrib/archive">archief</a>.</p>

<p>De <a href="$(HOME)/doc/manuals/debian-faq/">Debian FAQ</a> bevat een uitleg over
<a href="$(HOME)/doc/manuals/debian-faq/ftparchives#sourceforcodenames">waar
alle bovenstaande namen vandaan komen</a>.</p>

<h2>Integriteit van de data in de releases</h2>

<p>De integriteit van een release wordt gegarandeerd door een digitaal
ondertekend <code>Release</code>-bestand. Om er zeker van te zijn dat
alle bestanden in een release er echt bij horen, worden controlesommen
van alle <code>Packages</code>-bestanden opgenomen in het
<code>Release</code>-bestand.</p>

<p>De digitale handtekeningen voor het <code>Release</code>-bestand worden
opgeslagen in het bestand <code>Release.gpg</code>; hiervoor wordt de huidige
digitale sleutel voor de ondertekening van een archief gebruikt.
Voor <q>stable</q> en <q>oldstable</q>, de verouderde stabiele release, wordt
een aanvullende handtekening gegenereerd met behulp van een offline sleutel
die door een lid van het
<a href="$(HOME)/intro/organization#release-team">Stable Release Team</a>
specifiek voor een release wordt aangemaakt.</p>

