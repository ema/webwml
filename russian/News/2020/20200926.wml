#use wml::debian::translation-check translation="cc3aa11466129a6224ab33a305a554cb8d65f63c" maintainer="Lev Lamberov"
<define-tag pagetitle>Обновлённый Debian 10: выпуск 10.6</define-tag>
<define-tag release_date>2020-09-26</define-tag>
#use wml::debian::news

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.6</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Проект Debian с радостью сообщает о шестом обновлении своего
стабильного выпуска Debian <release> (кодовое имя <q><codename></q>).
Это обновление в основном содержит исправления проблем безопасности,
а также несколько корректировок серьёзных проблем. Рекомендации по безопасности
опубликованы отдельно и указываются при необходимости.</p>

<p>Заметьте, что это обновление не является новой версией Debian
<release>, а лишь обновлением некоторых включённых в выпуск пакетов. Нет
необходимости выбрасывать старые носители с выпуском <q><codename></q>. После установки
пакеты можно обновить до текущих версий, используя актуальное
зеркало Debian.</p>

<p>Тем, кто часто устанавливает обновления с security.debian.org, не придётся
обновлять много пакетов, большинство обновлений с security.debian.org
включены в данное обновление.</p>

<p>Новые установочные образы будут доступны позже в обычном месте.</p>

<p>Обновление существующих систем до этой редакции можно выполнить с помощью
системы управления пакетами, используя одно из множества HTTP-зеркал Debian.
Исчерпывающий список зеркал доступен на странице:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<h2>Исправления различных ошибок</h2>

<p>Данное стабильное обновление вносит несколько важных исправлений для следующих пакетов:</p>

<table border=0>
<tr><th>Пакет</th>               <th>Причина</th></tr>
<correction arch-test "Исправление обнаружения архитектуры s390x">
<correction asterisk "Исправление аварийной остановки при согласовании T.38 с отклонённым потоком [CVE-2019-15297], <q>SIP-запрос может изменить адрес SIP-узла</q> [CVE-2019-18790], <q>пользователь AMI может выполнить системные команды</q> [CVE-2019-18610], ошибка сегментирования в показе истории pjsip с одноранговыми узлами IPv6">
<correction bacula "Исправление ошибки <q>слишком большие строки в резюме сообщений позволяют вредоносному клиенту вызывать переполнение динамической памяти у контроллера</q> [CVE-2020-11061]">
<correction base-files "Обновление /etc/debian_version для текущей редакции">
<correction calamares-settings-debian "Отключение модуля displaymanager">
<correction cargo "Новый выпуск основной ветки разработки для поддержки готовящихся версий Firefox ESR">
<correction chocolate-doom "Исправление отсутствующей проверки [CVE-2020-14983]">
<correction chrony "Предотвращение гонки символьных ссылок при записи в PID-файл [CVE-2020-14367]; исправление чтения температуры">
<correction debian-installer "Обновление ABI Linux до версии 4.19.0-11">
<correction debian-installer-netboot-images "Повторная сборка с учётом proposed-updates">
<correction diaspora-installer "Использование опции --frozen для привязки установки к использованию Gemfile.lock, поставляемого из основной ветки разработки; отмена исключения Gemfile.lock во время обновлений; не перезаписывать config/oidc_key.pem во время обновлений; разрешение записи в config/schedule.yml">
<correction dojo "Исправление загрязнения прототипа в методе deepCopy [CVE-2020-5258] в методе jqMix [CVE-2020-5259]">
<correction dovecot "Исправление регрессии отсеивающего фильтра dsync; исправление обработки результата getpwent в userdb-passwd">
<correction facter "Изменение конечной точки Google GCE Metadata с <q>v1beta1</q> на <q>v1</q>">
<correction gnome-maps "Исправление проблемы с отрисовкой неправильно выравненного слоя фигуры">
<correction gnome-shell "LoginDialog: сброс приглашения аутентификации при переключении VT до постепенного появления изображения [CVE-2020-17489]">
<correction gnome-weather "Предотвращение аварийной остановки при ошибочной настройке набора локаций">
<correction grunt "Использование safeLoad при загрузке YAML-файлов [CVE-2020-7729]">
<correction gssdp "Новый стабильный выпуск основной ветки разработки">
<correction gupnp "Новый стабильный выпуск основной ветки разработки; предотвращение атаки <q>CallStranger</q> [CVE-2020-12695]; требование GSSDP 1.0.5">
<correction haproxy "logrotate.conf: использование вспомогательного сценария вместо сценария инициализации SysV; отклонение сообщений с отсутствующим параметром <q>chunked</q> в Transfer-Encoding [CVE-2019-18277]">
<correction icinga2 "Исправление атаки через символьные ссылки [CVE-2020-14004]">
<correction incron "Исправление очистки бездействующих процессов">
<correction inetutils "Исправление проблемы с удалённым выполнением кода [CVE-2020-10188]">
<correction libcommons-compress-java "Исправление отказа в обслуживании [CVE-2019-12402]">
<correction libdbi-perl "Исправление повреждения содержимого памяти в функциях XS при перераспределении стека Perl [CVE-2020-14392]; исправление переполнения буфера при использовании слишком длинного имени класса DBD [CVE-2020-14393]; исправление разыменования NULL-профиля в dbi_profile() [CVE-2019-20919]">
<correction libvncserver "libvncclient: выполнение очистки, если имя UNIX-сокета может привести к переполнению [CVE-2019-20839]; исправление проблемы с выравниванием или присвоением псевдонима указателю [CVE-2020-14399]; ограничение максимального размера textchat [CVE-2020-14405]; libvncserver: добавление отсутствующих проверок NULL-указателя [CVE-2020-14397]; исправление проблемы с выравниванием или присвоением псевдонима указателю [CVE-2020-14400]; scale: изменение типа на 64-битный до выполнения сдвига [CVE-2020-14401]; предотвращение доступа к OOB [CVE-2020-14402 CVE-2020-14403 CVE-2020-14404]">
<correction libx11 "Исправление переполнения целых чисел [CVE-2020-14344 CVE-2020-14363]">
<correction lighttpd "Обратный перенос нескольких исправлений безопасности и улучшений удобства использования">
<correction linux "Новый стабильный выпуск основной ветки разработки; увеличение номера версии ABI до 11">
<correction linux-latest "Обновление для ABI ядра Linux версии 11">
<correction linux-signed-amd64 "Новый стабильный выпуск основной ветки разработки">
<correction linux-signed-arm64 "Новый стабильный выпуск основной ветки разработки">
<correction linux-signed-i386 "Новый стабильный выпуск основной ветки разработки">
<correction llvm-toolchain-7 "Новый выпуск основной ветки разработки для поддержки готовящихся версий Firefox ESR; исправление ошибок сборки rustc">
<correction lucene-solr "Исправление проблемы безопасности в коде обработки настройки DataImportHandler [CVE-2019-0193]">
<correction milkytracker "Исправление переполнения динамической памяти [CVE-2019-14464], переполнения стека [CVE-2019-14496], переполнения динамической памяти [CVE-2019-14497], исправление указателей после освобождения памяти [CVE-2020-15569]">
<correction node-bl "Исправление чтения за пределами выделенного буфера памяти [CVE-2020-8244]">
<correction node-elliptic "Предотвращение возможности деформирования подписей и переполнений [CVE-2020-13822]">
<correction node-mysql "Добавление опции localInfile для управления LOAD DATA LOCAL INFILE [CVE-2019-14939]">
<correction node-url-parse "Исправление недостаточной проверки и очистки пользовательских входных данных [CVE-2020-8124]">
<correction npm "Не показывать пароли в журналах [CVE-2020-15095]">
<correction orocos-kdl "Удаление явного добавления включённого пути по умолчанию, исправление ошибок с cmake &lt; 3.16">
<correction postgresql-11 "Новый стабильный выпуск основной ветки разработки; установка безопасного значения search_path в логической репликации walsender и apply worker [CVE-2020-14349]; использование более безопасных сценариев установки модулей contrib [CVE-2020-14350]">
<correction postgresql-common "Отмена сброса plpgsql до выполнения тестирования расширений">
<correction pyzmq "Asyncio: ожидание POLLOUT для отправителя в can_connect">
<correction qt4-x11 "Исправление переполнения буфера в коде для грамматического разбора XBM [CVE-2020-17507]">
<correction qtbase-opensource-src "Исправление переполнения буфера в коде для грамматического разбора XBM [CVE-2020-17507]; исправление поломки буфера обмена при сбросе таймера, достигшего значения в 50 дней">
<correction ros-actionlib "Безопасная загрузка YAML [CVE-2020-10289]">
<correction rustc "Новый выпуск основной ветки разработки для поддержки готовящихся версий Firefox ESR">
<correction rust-cbindgen "Новый выпуск основной ветки разработки для поддержки готовящихся версий Firefox ESR">
<correction ruby-ronn "Исправление обработки содержимого в кодировке UTF-8 в страницах руководства">
<correction s390-tools "Исправление жёсткой зависимости от perl вместо подстановки в ${perl:Depends} для исправления установки при использовании debootstrap">
</table>


<h2>Обновления безопасности</h2>


<p>В данный выпуск внесены следующие обновления безопасности. Команда
безопасности уже выпустила рекомендации для каждого
из этих обновлений:</p>

<table border=0>
<tr><th>Идентификационный номер рекомендации</th>  <th>Пакет</th></tr>
<dsa 2020 4662 openjdk-11>
<dsa 2020 4734 openjdk-11>
<dsa 2020 4736 firefox-esr>
<dsa 2020 4737 xrdp>
<dsa 2020 4738 ark>
<dsa 2020 4739 webkit2gtk>
<dsa 2020 4740 thunderbird>
<dsa 2020 4741 json-c>
<dsa 2020 4742 firejail>
<dsa 2020 4743 ruby-kramdown>
<dsa 2020 4744 roundcube>
<dsa 2020 4745 dovecot>
<dsa 2020 4746 net-snmp>
<dsa 2020 4747 icingaweb2>
<dsa 2020 4748 ghostscript>
<dsa 2020 4749 firefox-esr>
<dsa 2020 4750 nginx>
<dsa 2020 4751 squid>
<dsa 2020 4752 bind9>
<dsa 2020 4753 mupdf>
<dsa 2020 4754 thunderbird>
<dsa 2020 4755 openexr>
<dsa 2020 4756 lilypond>
<dsa 2020 4757 apache2>
<dsa 2020 4758 xorg-server>
<dsa 2020 4759 ark>
<dsa 2020 4760 qemu>
<dsa 2020 4761 zeromq3>
<dsa 2020 4762 lemonldap-ng>
<dsa 2020 4763 teeworlds>
<dsa 2020 4764 inspircd>
<dsa 2020 4765 modsecurity>
</table>



<h2>Программа установки Debian</h2>

Программа установки была обновлена с целью включения исправлений, добавленных в
данную редакцию стабильного выпуска.

<h2>URL</h2>

<p>Полный список пакетов, которые были изменены в данной редакции:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Текущий стабильный выпуск:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Предлагаемые обновления для стабильного выпуска:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Информация о стабильном выпуске (информация о выпуске, известные ошибки и т. д.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Анонсы безопасности и информация:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>О Debian</h2>

<p>Проект Debian &mdash; объединение разработчиков свободного программного обеспечения,
которые жертвуют своё время и знания для создания абсолютно свободной
операционной системы Debian.</p>

<h2>Контактная информация</h2>

<p>Более подробную информацию вы можете получить на сайте Debian
<a href="$(HOME)/">https://www.debian.org/</a>, либо отправив письмо по адресу
&lt;press@debian.org&gt;, либо связавшись с командой стабильного выпуска по адресу
&lt;debian-release@lists.debian.org&gt;.</p>
