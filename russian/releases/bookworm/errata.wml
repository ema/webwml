#use wml::debian::template title="Debian 12 &mdash; Известные ошибки" BARETITLE=true
#use wml::debian::toc
#use wml::debian::translation-check translation="571f37332bde9e6cd25f3ea49eea7661b8cf7d70" maintainer="Lev Lamberov"

#include "$(ENGLISHDIR)/releases/info"

<toc-display/>


# <toc-add-entry name="known_probs">Известные проблемы</toc-add-entry>
<toc-add-entry name="security">Проблемы безопасности</toc-add-entry>

<p>Команда безопасности Debian выпускает обновления пакетов в стабильном выпуске,
в которых они обнаружили проблемы, относящиеся к безопасности. Информацию о всех
проблемах безопасности, найденных в <q>bookworm</q>, смотрите на
<a href="$(HOME)/security/">страницах безопасности</a>.</p>

<p>Если вы используете APT, добавьте следующую строку в <tt>/etc/apt/sources.list</tt>,
чтобы получить доступ к последним обновлениям безопасности:</p>

<pre>
  deb http://security.debian.org/ bookworm-security main contrib non-free
</pre>

<p>После этого запустите <kbd>apt update</kbd> и затем
<kbd>apt upgrade</kbd>.</p>


<toc-add-entry name="pointrelease">Редакции выпусков</toc-add-entry>

<p>Иногда, в случаях множества критических проблем или обновлений безопасности, выпущенный
дистрибутив обновляется. Обычно эти выпуски обозначаются как
редакции выпусков.</p>

<!-- <ul>
  <li>Первая редакция, 12.1, была выпущена
      <a href="$(HOME)/News/2017/FIXME">FIXME</a>.</li>
</ul> -->

<ifeq <current_release_bookworm> 12.0 "

<p>Для выпуска Debian 12 пока нет редакций.</p>" "

<p>Подробную информацию об изменениях между версиями 12 и <current_release_bookworm/> смотрите в <a
href=http://http.us.debian.org/debian/dists/bookworm/ChangeLog>\
журнале
изменений</a>.</p>"/>


<p>Исправления выпущенного стабильного дистрибутива часто должны пройти
усиленное тестирование, прежде чем они будут помещены в архив. Тем не менее,
эти исправления уже доступны в каталоге
<a href="http://ftp.debian.org/debian/dists/bookworm-proposed-updates/">\
dists/bookworm-proposed-updates</a> на всех зеркалах
Debian.</p>

<p>Если для обновления пакетов вы используете APT, то можете
установить предлагаемые обновления, добавив следующую строку в файл
<tt>/etc/apt/sources.list</tt>:</p>

<pre>
  \# предлагаемые дополнения для редакции 12-ого выпуска
  deb http://deb.debian.org/debian bookworm-proposed-updates main contrib non-free
</pre>

<p>После этого запустите <kbd>apt update</kbd> и затем
<kbd>apt upgrade</kbd>.</p>


<toc-add-entry name="installer">Система установки</toc-add-entry>

<p>
Информацию об известных ошибках и обновлениях в системе установки смотрите
на страницах <a href="debian-installer/">системы установки</a>.
</p>
