#use wml::debian::translation-check translation="c1cda89411cb7d3a03d275607055406f3c9d3db5" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découverts dans le noyau Linux qui pouvaient
conduire à une élévation des privilèges, un déni de service ou une fuite
d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2873">CVE-2022-2873</a>

<p>Zheyu Ma a découvert qu’un défaut d’accès en mémoire hors limites dans le
pilote du contrôleur d’hôte iSMT SMBus 2.0 d’Intel pouvait aboutir dans un
déni de service (plantage du sytème).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3545">CVE-2022-3545</a>

<p>Il a été découvert que le pilote NFD (Netronome Flow Processor) contenait un
défaut d’utilisation de mémoire après libération dans area_cache_get(), qui
pouvait aboutir à un déni de service ou à l’exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3623">CVE-2022-3623</a>

<p>Une situation de compétition lors de la recherche d’une page hugetlb de taille
CONT-PTE/PMD pouvait aboutir à un déni de service ou à une fuite d'informations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-4696">CVE-2022-4696</a>

<p>Une vulnérabilité d’utilisation de mémoire après libération a été découverte
le sous-système io_uring.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-36280">CVE-2022-36280</a>

<p>Une vulnérabilité d’écriture en mémoire hors limites a été découverte dans
le pilote vmwgfx, qui pouvait permettre à un utilisateur local non privilégié de
provoquer un déni de service (plantage du système).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-41218">CVE-2022-41218</a>

<p>Hyunwoo Kim a signalé un défaut d’utilisation de mémoire après libération
dans le sous-système central Media DVB causé par une compétition de refcount,
qui pouvait permettre à un utilisateur local de provoquer un déni de service ou
d’élever ses privileges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-45934">CVE-2022-45934</a>

<p>Un dépassement d'entier dans l2cap_config_req() dans le sous-système
Bluetooth a été découvert, qui pouvait permettre à un attaquant physiquement
proche de provoquer un déni de service (plantage du système).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-47929">CVE-2022-47929</a>

<p>Frederick Lawler a signalé qu’un déréférencement de pointeur NULL dans le
sous-système de contrôle de trafic permettait à un utilisateur non privilégié de
provoquer un déni de service en définissant une configuration de contrôle de
trafic contrefait pour l'occasion.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0179">CVE-2023-0179</a>

<p>Davide Ornaghi a découvert une arithmétique incorrecte lors de la recherche
des bits d’en-tête de VLAN dans le sous-système netfilter sous-système,
permettant à un utilisateur local de divulguer des adresses de pile et de tas
ou éventuellement d’élever les privilèges locaux à ceux du superutilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0240">CVE-2023-0240</a>

<p>Un défaut a été découvert dans le sous-système io_uring qui pouvait conduire
à une utilisation de mémoire après libération. Un utilisateur local pouvait
exploiter cela pour provoquer un déni de service (plantage ou corruption de
mémoire) ou éventuellement pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0266">CVE-2023-0266</a>

<p>Un défaut d’utilisation de mémoire après libération dans le sous-système
audio dû à un blocage absent pouvait aboutir à un déni de service ou à une
élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0394">CVE-2023-0394</a>

<p>Kyle Zeng a découvert un défaut de déréférencement de pointeur NULL dans
rawv6_push_pending_frames() dans le sous-système réseau permettant à un
utilisateur local de provoquer un déni de service (plantage système).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-23454">CVE-2023-23454</a>

<p>Kyle Zeng a signalé que l’ordonnanceur de réseau CBQ (Class Based Queueing)
était prédisposé a un déni de service dû à l’interprétation des résultats de
classification avant la vérification du code de retour de la classification.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-23455">CVE-2023-23455</a>

<p>Kyle Zeng a signalé que l’ordonnanceur de réseau de circuits virtuels ATM
était prédisposé à un déni de service dû à l’interprétation des résultats de
classification avant la vérification du code de retour de la classification.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-23586">CVE-2023-23586</a>

<p>Un défaut a été découvert dans le sous-système io_uring qui pouvait conduire
à une fuite d'informations. Un utilisateur local pouvait exploiter cela pour
obtenir des informations sensibles du noyau ou d’autres utilisateurs.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 5.10.162-1~deb10u1.</p>

<p>Cette mise à jour also corrige Debian bugs #825141, #1008501, #1027430, et #1027483, et includes many more bug corrige from stable updates
5.10.159-5.10.162 inclusive.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux-5.10.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de linux-5.10,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/linux-5.10">\
https://security-tracker.debian.org/tracker/linux-5.10</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

<p>--pgIrMxmM/8co4Tq2
Content-Type: application/pgp-signature; name="signature.asc"</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3349.data"
# $Id: $
