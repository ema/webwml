#use wml::debian::translation-check translation="04f7bb43cf8653ced94a84d0870dda905538297d" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans Wireshark, un
analyseur de trafic réseau. Un attaquant pouvait causer un déni de service
(boucle infinie ou plantage d'application) à l’aide d’une injection de paquet ou
d’un fichier de capture contrefait.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-4345">CVE-2022-4345</a>

<p>Une boucle infinie dans les dissecteurs de protocole BPv6, OpenFlow et Kafka
dans Wireshark, versions 4.0.0 à 4.0.1 et 3.6.0 à 3.6.9, permettait un déni de
service à l’aide d’une injection de paquet ou d’un fichier de capture contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0411">CVE-2023-0411</a>

<p>Des boucles excessives dans plusieurs dissecteurs dans Wireshark,
versions 4.0.0 à 4.0.2 et 3.6.0 à 3.6.10, permettaient un déni de service à
l’aide d’une injection de paquet ou d’un fichier de capture contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0412">CVE-2023-0412</a>

<p>Le dissecteur TIPC plantait dans Wireshark, versions 4.0.0 à 4.0.2 et 3.6.0
à 3.6.10, et permettait un déni de service à l’aide d’une injection de paquet ou
d’un fichier de capture contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0413">CVE-2023-0413</a>

<p>Un bogue de dissection dans Wireshark, versions 4.0.0 à 4.0.2 et 3.6.0 à
3.6.10, permettait un déni de service à l’aide d’une injection de paquet ou
d’un fichier de capture contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0415">CVE-2023-0415</a>

<p>Le plantage du dissecteur iSCSI dans Wireshark, versions 4.0.0 à 4.0.2 et
3.6.0 à 3.6.10, permettait un déni de service à l’aide d’une injection de paquet
ou d’un fichier de capture contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0417">CVE-2023-0417</a>

<p>Une fuite de mémoire dans le dissecteur NFS dans Wireshark, versions 4.0.0 à
4.0.2 et 3.6.0 à 3.6.10, permettait un déni de service à l’aide d’une injection
de paquet ou d’un fichier de capture contrefait.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 2.6.20-0+deb10u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets wireshark.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de wireshark,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/wireshark">\
https://security-tracker.debian.org/tracker/wireshark</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3313.data"
# $Id: $
