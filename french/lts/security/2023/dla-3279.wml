#use wml::debian::translation-check translation="233458ba39f1375892a3a58ab68e642f6f8efeca" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été trouvées dans trafficserver, un serveur cache
mandataire.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-37150">CVE-2021-37150</a>

<p>Une vulnérabilité de validation impropre d’entrée dans l’analyse de l’en-tête
d’Apache Traffic Server permettait à un attaquant de solliciter des ressources
non sûres.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-25763">CVE-2022-25763</a>

<p>Une vulnérabilité de validation impropre d’entrée dans la validation de
requête HTTP/2 d’Apache Traffic Server permettait à un attaquant de créer des
attaques par dissimulation ou empoisonnement de cache.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-28129">CVE-2022-28129</a>

<p>Une vulnérabilité de validation impropre d’entrée dans l’analyse d’en-tête
HTTP/1.1 d’Apache Traffic Server permettait à un attaquant d’envoyer des
en-têtes non valables.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-31780">CVE-2022-31780</a>

<p>Une vulnérabilité de validation impropre d’entrée dans le traitement de
trame HTTP/2 d’Apache Traffic Server permettait à un attaquant de dissimuler
des requêtes.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 8.0.2+ds-1+deb10u7.</p>

<p>Nous vous recommandons de mettre à jour vos paquets trafficserver.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de trafficserver,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/trafficserver">\
https://security-tracker.debian.org/tracker/trafficserver</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3279.data"
# $Id: $
