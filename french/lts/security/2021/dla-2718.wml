#use wml::debian::translation-check translation="9a33145cdeca4ea8218e54190881e397e42f7d20" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Cette mise à jour fournit le microcode mis à jour pour certains types
de processeurs d’Intel et fournit une mitigation pour des vulnérabilités de
sécurité qui pourraient aboutir à une élévation des privilèges dans la
combinaison de VT-d et de diverses attaques par canal auxiliaire.</p>

<p>Pour Debian 9 « Stretch », ces problèmes ont été corrigés dans
la version 3.20210608.2~deb9u2.</p>

<p>Veuillez noter qu’un des processeurs ne reçoit pas de mise à jour, aussi
les utilisateurs des processeurs 0x906ea qui n’ont pas de carte sans fil
Intel peuvent utiliser le paquet de buster-security à la place.</p>

<p>Nous vous recommandons de mettre à jour vos paquets intel-microcode.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de intel-microcode,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/intel-microcode">\
https://security-tracker.debian.org/tracker/intel-microcode</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2718.data"
# $Id: $
