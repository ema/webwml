#use wml::debian::translation-check translation="43250814d9c41ecd2f89d0c50a31a13b16818873" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été corrigées dans curl, une bibliothèque de
transfert d’URL coté client.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-22898">CVE-2021-22898</a>

<p>Divulgation d'informations lors de la connexion à des serveurs telnet.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-22924">CVE-2021-22924</a>

<p>Mauvaise réutilisation de connexion due à des vérifications défectueuses
de noms de chemin.</p></li>

</ul>

<p>Pour Debian 9 « Stretch », ces problèmes ont été corrigés dans
la version 7.52.1-5+deb9u15.</p>

<p>Nous vous recommandons de mettre à jour vos paquets curl.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de curl,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/curl">\
https://security-tracker.debian.org/tracker/curl</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2734.data"
# $Id: $
