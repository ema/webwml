#use wml::debian::translation-check translation="e8a53409d2db97e5b80677bfd214ef53f639b689" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans Salt, un
puissant gestionnaire d'exécution à distance, qui permet une élévation
locale de privilèges sur un client (« minion »), des attaques par injection
de patron côté serveur, des vérifications insuffisantes pour des
accréditations eauth, des injections d'interpréteur de commandes et de
commandes ou une validation incorrecte de certificats SSL.</p>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 2016.11.2+ds-1+deb9u7.</p>

<p>Nous vous recommandons de mettre à jour vos paquets salt.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de salt, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/salt">\
https://security-tracker.debian.org/tracker/salt</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2815.data"
# $Id: $
