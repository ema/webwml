#use wml::debian::translation-check translation="4633aacd3c2b66b04edd0587650980deeb1700f0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une régression a été introduite dans la DLA-2768-1, où le module mandataire
uwsgi pour Apache2 (mod_uwsgi) interprète des configurations d’Apache
d’une façon moins stricte, provoquant l’échec de configurations existantes
après une mise à niveau.</p>

<p>Pour Debian 9 « Stretch », ce problème a été corrigé dans
la version 2.0.14+20161117-3+deb9u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets uwsgi.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de uwsgi,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/uwsgi">\
https://security-tracker.debian.org/tracker/uwsgi</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2768-2.data"
# $Id: $
