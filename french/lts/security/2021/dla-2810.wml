#use wml::debian::translation-check translation="edec9b68ebe4b851f32586a2757ada3ded41e04d" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il y avait un certain nombre de problèmes dans redis, une base de
données clé-valeur populaire :</p>

<ul>
<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-41099">CVE-2021-41099</a>

<p>Dépassement d'entier sur le tampon de tas lors de la gestion de
certaines commandes de chaîne et charges utiles de réseau, quand
<q>proto-max-bulk-len</q> est configuré manuellement à une valeur très
grande différente de celle par défaut.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-32762">CVE-2021-32762</a>:

<p>Problème de dépassement d'entier sur le tampon de tas dans redis-cli et
redis-sentinel lors de l'analyse de grandes réponses « multi-bulk » sur
certaines plateformes plus anciennes et moins répandues.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-32687">CVE-2021-32687</a>

<p>Dépassement d'entier sur le tampon de tas avec les IntSet, quand
<q>set-max-intset-entries</q> est configuré manuellement à une valeur très
grande différente de celle par défaut.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-32675">CVE-2021-32675</a>

<p>Déni de service lors du traitement de la charge utile de requêtes RESP
avec un grand nombre d'éléments sur de nombreuses connexions.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-32672">CVE-2021-32672</a>

<p>Problème de lecture aléatoire de tas avec le débogueur Lua.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-32626">CVE-2021-32626</a>

<p>Des scripts Lua contrefaits pour l'occasion peuvent avoir pour
conséquence un dépassement de tampon de tas.</p></li>
</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 3:3.2.6-3+deb9u8.</p>

<p>Nous vous recommandons de mettre à jour vos paquets redis.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2810.data"
# $Id: $
