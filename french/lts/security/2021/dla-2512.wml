#use wml::debian::translation-check translation="019d03b3baaee431755b0558412b8ca20da18276" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Un défaut a été découvert dans hibernate-core. Une injection SQL dans
l’implémentation de l’API JPA Criteria peut permettre des littéraux non
nettoyés quand un littéral est utilisé dans les commentaires SQL de la requête. Ce
défaut pourrait permettre à un attaquant d’obtenir des informations réservées
ou éventuellement de conduire d’autres attaques.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 3.6.10.Final-6+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libhibernate3-java.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libhibernate3-java, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/libhibernate3-java">https://security-tracker.debian.org/tracker/libhibernate3-java</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2512.data"
# $Id: $
