#use wml::debian::translation-check translation="ab3be4ee01879fd4484c795bbaa824377c218575" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Divers problèmes ont été découverts dans exempi, une bibliothèque pour
analyser des métadonnées XMP qui pourraient causer un déni de service ou avoir
un autre impact non spécifié à l’aide de fichiers contrefaits.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-18233">CVE-2017-18233</a>

<p>Un dépassement d’entier dans la classe Chunk dans RIFF.cpp permet à des
attaquants distants de provoquer un déni de service (boucle infinie) à l’aide de
données XMP contrefaites dans un fichier .avi.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-18234">CVE-2017-18234</a>

<p>Un problème a été découvert qui permet à des attaquants distants de
provoquer un déni de service (memcpy non valable résultant dans une utilisation
de mémoire après libération) ou éventuellement avoir un impact non précisé
à l'aide d'un fichier .pdf contenant des données JPEG.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-18236">CVE-2017-18236</a>

<p>La fonction ASF_Support::ReadHeaderObject dans ASF_Support.cpp permet à des
attaquants distants de provoquer un déni de service (boucle infinie) à l'aide
d'un fichier .asf contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-18238">CVE-2017-18238</a>

<p>La fonction TradQT_Manager::ParseCachedBoxes dans QuickTime_Support.cpp
permet à des attaquants distants de provoquer un déni de service (boucle
infinie) à l’aide de données XMP contrefaites dans un fichier .qt.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7728">CVE-2018-7728</a>

<p>TIFF_Handler.cpp gère incorrectement un cas de longueur nulle, conduisant
à une lecture hors limites de tampon basé sur le tas dans la fonction MD5Update()
dans MD5.cpp.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7730">CVE-2018-7730</a>

<p>Un certain cas de longueur 0xffffffff est mal géré dans PSIR_FileWriter.cpp,
conduisant à une lecture hors limites de tampon basé sur le tas dans la fonction
 PSD_MetaHandler::CacheFileData().</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 2.2.0-1+deb7u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets exempi.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1310.data"
# $Id: $
