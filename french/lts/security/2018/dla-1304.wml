#use wml::debian::translation-check translation="ec63203ae7c88b70ad002f6a6d89bd22297d4fb8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>plusieurs vulnérabilités ont été découvertes dans l’interpréteur de commandes
<q>zsh</q>.</p>

<ul>
<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-10070">CVE-2014-10070</a>
<p>Correction d’un problème d’élévation des privilèges si l’environnement
n’avait pas été correctement nettoyé.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-10071">CVE-2014-10071</a>
<p>Prévention d’un dépassement de tampon pour de très longs descripteurs de
fichier dans la syntaxe <q>&gt;&amp; fd</q>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-10072">CVE-2014-10072</a>
<p>Correction d’un dépassement de tampon lors de l’analyse de très longs chemins
de répertoire pour les liens symboliques.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10714">CVE-2016-10714</a>
<p>Correction d’une erreur due à un décalage d'entier aboutissant à des tampons
sous-dimensionnés prévus pour prendre en charge PATH_MAX.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-18206">CVE-2017-18206</a>
<p>Correction d'un dépassement de tampon dans l’expansion de liens symboliques.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ce problème a été corrigé dans la
version 4.3.17-1+deb7u1 de zsh.</p>

<p>Nous vous recommandons de mettre à jour vos paquets zsh.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1304.data"
# $Id: $
