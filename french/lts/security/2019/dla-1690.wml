#use wml::debian::translation-check translation="6ae537437a38874792473335ec1a39668f85f810" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans liblivemedia, la
bibliothèque LIVE555 du serveur RTSP :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6256">CVE-2019-6256</a>

<p>Les serveurs liblivemedia avec la tunnellisation RTSP-sur-HTTP activée
sont vulnérables à un déréférencement de pointeur de fonction non valable. Ce
problème peut se produire pendant la gestion d’erreur lors du traitement de deux
requêtes GET et POST envoyées avec des x-sessioncookie identiques dans la même
session TCP, et peut être exploité par des attaquants distants pour provoquer un
déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-7314">CVE-2019-7314</a>

<p>Les serveurs liblivemedia avec la tunnellisation RTSP-sur-HTTP activée sont
affectés par une vulnérabilité d’utilisation de mémoire après libération . Cette
vulnérabilité peut être déclenchée par des attaquants distants pour provoquer un
déni de service (plantage du serveur) ou éventuellement un autre impact non
précisé.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans la version 2014.01.13-1+deb8u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets liblivemedia.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1690.data"
# $Id: $
