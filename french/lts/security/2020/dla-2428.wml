#use wml::debian::translation-check translation="106090aebc4507575641da0e99fe431379ecc2a9" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de dépassement de tampon ont été trouvées dans le
processus de décodage d’image QUIC du système d’affichage à distance SPICE avant
spice-0.14.2-1.</p>

<p>À la fois le client (spice-gtk) et le serveur de SPICE sont touchés par ces
défectuosités. Celles-ci permettent à un client ou à un serveur malveillant
d’envoyer des messages contrefaits pour l'occasion qui, lorsque traités
par l’algorithme de compression d’image de QUIC, aboutissaient à un plantage du
processus ou à une exécution potentielle de code.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 0.33-3.3+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets spice-gtk.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de spice-gtk, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/spice-gtk">https://security-tracker.debian.org/tracker/spice-gtk</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2428.data"
# $Id: $
