#use wml::debian::translation-check translation="c0b4c1ce5ac4f69c6f9eb2b070d235ad6f09dd09" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>La classe Commandline dans maven-shared-utils, une collection de diverses
classes d'utilitaire pour le système de construction Maven, peut émettre
des chaînes à double guillemets sans les protéger correctement, permettant
des attaques d'injection de commande d'interpréteur.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
3.0.0-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets maven-shared-utils.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de maven-shared-utils,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/maven-shared-utils">\
https://security-tracker.debian.org/tracker/maven-shared-utils</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3059.data"
# $Id: $
