#use wml::debian::translation-check translation="a62c48bbb1b6f5d1c3eddb6e04c45c8273dd9911" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Le greffon SQL dans cyrus-sasl2, une bibliothèque implémentant « SASL »,
la couche simple d'authentification et de sécurité, est prédisposé à une
attaque d'injection SQL. Un attaquant distant authentifié peut tirer
avantage de ce défaut pour exécuter des commandes SQL arbitraires et pour
une élévation de privilèges.</p>


<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
2.1.27~101-g0780600+dfsg-3+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets cyrus-sasl2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de cyrus-sasl2, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/cyrus-sasl2">\
https://security-tracker.debian.org/tracker/cyrus-sasl2</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2931.data"
# $Id: $
