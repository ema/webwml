#use wml::debian::translation-check translation="f7b7f728043a9eb6dfb2e7e2cb2f898da3da2636" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Cette mise à jour corrige deux vulnérabilités de format dans giflib.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-11490">CVE-2018-11490</a>

<p>La fonction <q>DGifDecomqssLine</q> dans <q>dgif_lib.c</q>, comme fournie
précédemment dans <q>cgif.c</q> dans sam2p version 0.49.4, avait un dépassement
de tampon de tas à cause d’un certain indice de tableau
<q>Private-&gt;RunningCode - 2</q> non vérifié. Cela pouvait aboutir à un déni
de service ou éventuellement avoir un autre impact non précisé.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15133">CVE-2019-15133</a>

<p>Un fichier GIF mal formé déclenche une exception de division par zéro dans
la fonction de décodeur <q>DGifSlurp</q> dans <q>dgif_lib.c</q> si le champ
<q>height</q> de la structure de données <q>ImageSize</q> est égal à zéro.</p></li>

</ul>

<p>Pour Debian 10 « Buster », ces problèmes ont été corrigés dans
la version 5.1.4-3+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets giflib.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de giflib,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/giflib">\
https://security-tracker.debian.org/tracker/giflib</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3223.data"
# $Id: $
