#use wml::debian::translation-check translation="23e71859bd02588bbbf20f898101ae4844c0328a" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>jQuery-UI, la bibliothèque officielle de l’interface utilisateur de jQuery,
est un ensemble soigneusement sélectionné d’interactions, d’effets de composants
graphiques et de thèmes d’interface utilisateur construits au-dessus de jQuery ;
elle a été signalée comme comportant les vulnérabilités suivantes.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-41182">CVE-2021-41182</a>

<p>jQuery-UI acceptait la valeur de l’option « altField » du composant graphique
Datepicker de sources non fiables et pouvait exécuter du code non fiable. Cela a
été corrigé et maintenant toute valeur de chaîne passée à l’option
« altField » est traitée comme un sélecteur CSS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-41183">CVE-2021-41183</a>

<p>jQuery-UI acceptait la valeur de diverses options « *Text » du composant
graphique Datepicker de sources non fiables et pouvait exécuter du code non
fiable. Cela a été corrigé et maintenant les valeurs passées à toute option
« *Text » sont traitées comme du texte pur et non du HTML.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-41184">CVE-2021-41184</a>

<p>jQuery-UI acceptait la valeur de l’option « of » de l’outil
« .position() » de sources non fiables et pouvait exécuter du code non fiable.
Cela a été corrigé et maintenant toute valeur de chaîne passée à l’option
« of » est traitée comme un sélecteur CSS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-31160">CVE-2022-31160</a>

<p>jQuery-UI était éventuellement vulnérable à un script intersite.
L’initialisation d’un composant graphique checkboxradio d’une entrée incorporée
dans une étiquette faisait que le contenu de l’étiquette parent était considéré
comme l’étiquette d’entrée. Un appel à « .checkboxradio( <q>refresh</q> ) » sur
un tel composant graphique et le code HTML contenant des entités HTML encodées
faisaient qu’elles étaient décodées de manière erronée. Cela pouvait conduire
éventuellement à une exécution de code JavaScript.</p></li>

</ul>

<p>Pour Debian 10 « Buster », ces problèmes ont été corrigés dans
la version 1.12.1+dfsg-5+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets jqueryui.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de jqueryui,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/jqueryui">\
https://security-tracker.debian.org/tracker/jqueryui</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3230.data"
# $Id: $
