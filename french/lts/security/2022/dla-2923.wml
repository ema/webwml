#use wml::debian::translation-check translation="f7157af009ccc03e4c76793a38fae39488d26e4f" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Des chercheurs en sécurité de JFrog Security et Ismail Aydemir ont
découvert deux vulnérabilités d'exécution de code à distance dans le moteur
de base de données Java SQL H2 qui peuvent être exploitées au moyen de
divers vecteurs d'attaque, plus particulièrement à travers la console de H2
et en chargeant des classes personnalisées à partir de serveurs distants en
utilisant JNDI. La console de H2 est un outil de développement et n'est
requise par aucune dépendance inverse dans Debian. Elle a été désactivée
dans les versions oldstable et stable. Il est recommandé aux développeurs
de bases de données d'utiliser au moins la version 2.1.210-1, actuellement
disponible dans Debian unstable.</p>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 1.4.193-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets h2database.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de h2database, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/h2database">\
https://security-tracker.debian.org/tracker/h2database</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2923.data"
# $Id: $
