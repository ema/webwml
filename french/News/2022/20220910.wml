#use wml::debian::translation-check translation="4924e09e5cb1b4163d7ec7161488354e4167b24c" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Publication de la mise à jour de Debian 10.13</define-tag>
<define-tag release_date>2022-09-10</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>10</define-tag>
<define-tag codename>Buster</define-tag>
<define-tag revision>10.13</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Le projet Debian a l'honneur d'annoncer la treizième (et dernière) mise à jour
de sa distribution oldstable Debian <release> (nom de code <q><codename></q>).
Tout en réglant quelques problèmes importants, cette mise à jour corrige
principalement des problèmes de sécurité de la version oldstable. Les annonces
de sécurité ont déjà été publiées séparément et sont simplement référencées dans
ce document.
</p>

<p>
Après cette version intermédiaire, les équipes de sécurité et de publication
de Debian ne produiront plus de mises à jour pour Debian 10. Les utilisateurs
qui souhaitent continuer à bénéficier du suivi de sécurité devraient mettre à
niveau vers Debian 11, ou consulter <url "https://wiki.debian.org/LTS"> pour
avoir des détails sur le sous-ensemble d'architectures et de paquets couverts
par le projet « Long Term Support ».
</p>

<p>
Veuillez noter que cette mise à jour ne constitue pas une nouvelle version de
Debian <release> mais seulement une mise à jour de certains des paquets qu'elle
contient. Il n'est pas nécessaire de jeter les anciens médias de la
version <codename>. Après installation, les paquets peuvent être mis à niveau
vers les versions courantes en utilisant un miroir Debian à jour.
</p>

<p>
Ceux qui installent fréquemment les mises à jour à partir de security.debian.org
n'auront pas beaucoup de paquets à mettre à jour et la plupart des mises à jour
de security.debian.org sont comprises dans cette mise à jour.
</p>

<p>
De nouvelles images d'installation seront prochainement disponibles à leurs
emplacements habituels.
</p>

<p>
Mettre à jour une installation vers cette révision peut se faire en faisant
pointer le système de gestion de paquets sur l'un des nombreux miroirs HTTP de
Debian. Une liste complète des miroirs est disponible à l'adresse :
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Corrections de bogues divers</h2>

<p>
Cette mise à jour de la version stable apporte quelques corrections importantes
aux paquets suivants :
</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction adminer "Correction d'un problème de redirection ouverte, de problèmes de script intersite [CVE-2020-35572 CVE-2021-29625] ; elasticsearch : pas d'affichage de réponse si le code HTTP n'est pas 200 [CVE-2021-21311] ; version compilée et fichiers de configuration fournis">
<correction apache2 "Correction de problèmes de déni de service [CVE-2022-22719], de dissimulation de requête HTTP [CVE-2022-22720], de dépassement d'entier [CVE-2022-22721], d'écriture hors limites [CVE-2022-23943], de dissimulation de requête HTTP [CVE-2022-26377], de lecture hors limites [CVE-2022-28614 CVE-2022-28615], de déni de service [CVE-2022-29404], de lecture hors limites [CVE-2022-30556] et d'un possible problème de contournement d'authentification basée sur l'IP [CVE-2022-31813]">
<correction base-files "Mise à jour pour la version 10.13">
<correction clamav "Nouvelle version amont stable ; corrections de sécurité [CVE-2022-20770 CVE-2022-20771 CVE-2022-20785 CVE-2022-20792 CVE-2022-20796]">
<correction commons-daemon "Correction de détection de JVM">
<correction composer "Correction d'une vulnérabilité d'injection de code [CVE-2022-24828] ; mise à jour du modèle de jeton de GitHub ; utilisation de l'en-tête Authorization à la place du paramètre de requête obsolète access_token">
<correction debian-installer "Reconstruction avec buster-proposed-updates ; passage de l'ABI de Linux à la version 4.19.0-21">
<correction debian-installer-netboot-images "Reconstruction avec buster-proposed-updates ; passage de l'ABI de Linux à la version 4.19.0-21">
<correction debian-security-support "Mise à jour de l'état de sécurité de divers paquets">
<correction debootstrap "Assurance que les chroots non merged-usr peuvent continuer à être créés pour les chroots de versions plus anciennes et de buildd">
<correction distro-info-data "Ajout d'Ubuntu 22.04 LTS, Jammy Jellyfish et Ubuntu 22.10, Kinetic Kudu">
<correction dropbear "Correction d'un problème potentiel d'énumération de noms d'utilisateur [CVE-2019-12953]">
<correction eboard "Correction d'une erreur de segmentation lors de la sélection du moteur">
<correction esorex "Correction d'échecs de suite de tests sur armhf et ppc64el provoqués par une utilisation incorrecte de libffi">
<correction evemu "Correction d'échec de construction avec les versions récentes du noyau">
<correction feature-check "Correction de certaines comparaisons de version">
<correction flac "Correction d'un problème d'écriture hors limites [CVE-2021-0561]">
<correction foxtrotgps "Correction d'échec de construction avec les versions récentes d'imagemagick">
<correction freeradius "Correction d'une fuite par canal auxiliaire quand une négociation de connexion sur 2048 échoue [CVE-2019-13456], d'un problème de déni de service dû à un accès multithreadé à BN_CTX [CVE-2019-17185], d'un plantage dû à une allocation de mémoire non sécurisée vis-à-vis des fils d'exécution">
<correction freetype "Correction d'un problème de dépassement de tampon [CVE-2022-27404] ; correction de plantages [CVE-2022-27405 CVE-2022-27406]">
<correction fribidi "Correction de problèmes de dépassement de tampon [CVE-2022-25308 CVE-2022-25309] ; correction de plantage [CVE-2022-25310]">
<correction ftgl "Plus d'essai de conversion de PNG vers EPS pour latex, parce la version Debian d'imagemagick a désactivé EPS pour des raisons de sécurité">
<correction gif2apng "Correction de dépassements de tampon de tas [CVE-2021-45909 CVE-2021-45910 CVE-2021-45911]">
<correction gnucash "Correction d'échec de construction avec la version récente de tzdata">
<correction gnutls28 "Correction de la suite de tests en combinaison avec OpenSSL 1.1.1e ou plus récent">
<correction golang-github-docker-go-connections "Tests qui utilisent des certificats expirés évités">
<correction golang-github-pkg-term "Correction de construction avec les nouveaux noyaux 4.19">
<correction golang-github-russellhaering-goxmldsig "Correction d'un problème de déréférencement de pointeur NULL [CVE-2020-7711]">
<correction grub-efi-amd64-signed "Nouvelle version amont">
<correction grub-efi-arm64-signed "Nouvelle version amont">
<correction grub-efi-ia32-signed "Nouvelle version amont">
<correction grub2 "Nouvelle version amont">
<correction htmldoc "Correction d'une boucle infinie [CVE-2022-24191], de problèmes de dépassement d'entier [CVE-2022-27114] et d'un problème de dépassement de tampon de tas [CVE-2022-28085]">
<correction iptables-netflow "Correction d'une régression due à un échec de construction DKMS provoquée par des modifications amont de Linux dans le noyau 4.19.191">
<correction isync "Correction de problèmes de dépassement de tampon [CVE-2021-3657]">
<correction kannel "Correction d'échec de construction en désactivant la génération de la documentation Postscript">
<correction krb5 "Utilisation de SHA256 comme empreinte CMS de Pkinit">
<correction libapache2-mod-auth-openidc "Amélioration de la validation du paramètre d'URL post-déconnexion à la déconnexion [CVE-2019-14857]">
<correction libdatetime-timezone-perl "Mise à jour des données incluses">
<correction libhttp-cookiejar-perl "Correction d'échec de construction en prolongeant la date d'expiration d'un cookie de test">
<correction libnet-freedb-perl "Changement de l'hôte par défaut de freedb.freedb.org abandonné pour gnudb.gnudb.org">
<correction libnet-ssleay-perl "Correction d'échec de tests avec OpenSSL 1.1.1n">
<correction librose-db-object-perl "Correction d'échec de test après le 6 juin 2020">
<correction libvirt-php "Correction d'erreur de segmentation dans libvirt_node_get_cpu_stats">
<correction llvm-toolchain-13 "Nouveau paquet source pour prendre en charge la construction des nouvelles versions de firefox-esr et de thunderbird">
<correction minidlna "Validation des requêtes HTTP pour protéger contre les attaques par rattachement DNS [CVE-2022-26505]">
<correction mokutil "Nouvelle version amont pour permettre la gestion de SBAT">
<correction mutt "Correction d'un dépassement de tampon uudecode [CVE-2022-1328]">
<correction node-ejs "Nettoyage des options et des nouveaux objets [CVE-2022-29078]">
<correction node-end-of-stream "Contournement d'un bogue de test">
<correction node-minimist "Correction d'un problème de pollution de prototype [CVE-2021-44906]">
<correction node-node-forge "Correction de problèmes de vérification de signature [CVE-2022-24771 CVE-2022-24772 CVE-2022-24773]">
<correction node-require-from-string "Correction d'un test survenant avec nodejs &gt;= 10.16">
<correction nvidia-graphics-drivers "Nouvelle version amont">
<correction nvidia-graphics-drivers-legacy-390xx "Nouvelle version amont ; correction de problèmes d'écriture hors limites [CVE-2022-28181 CVE-2022-28185] ; corrections de sécurité [CVE-2022-31607 CVE-2022-31608 CVE-2022-31615]">
<correction octavia "Correction de vérifications de certificat de client [CVE-2019-17134] ; détection correcte de l'exécution de l'agent sur Debian ; correction du modèle qui génère le script de vérification du protocole vrrp ; ajout de dépendances d'exécution supplémentaires ; fourniture d'une configuration supplémentaire directement dans le paquet agent">
<correction orca "Correction de l'utilisation avec WebKitGTK 2.36">
<correction pacemaker "Mise à jour des versions de relation pour corriger les mises à niveau à partir de Stretch LTS">
<correction pglogical "Correction d'échec de construction">
<correction php-guzzlehttp-psr7 "Correction d'une analyse incorrecte d'en-tête [CVE-2022-24775]">
<correction postfix "Nouvelle version amont stable ; pas d'écrasement du default_transport défini par l'utilisateur ; if-up.d : pas d'émission d'erreur si postfix ne peut pas encore envoyer de message ; correction d'entrées dupliquées de bounce_notice_recipient dans la sortie de postconf">
<correction postgresql-common "pg_virtualenv : écriture du fichier de mot passe temporaire avant de changer le propriétaire du fichier">
<correction postsrsd "Correction d'un problème de possible déni de service quand Postfix envoie certains champs de données longs tels que plusieurs adresses de courriel concaténées [CVE-2021-35525]">
<correction procmail "Correction d'un déréférencement de pointeur NULL">
<correction publicsuffix "Mise à jour des données incluses">
<correction python-keystoneauth1 "Mise à jour des tests pour corriger un échec de construction">
<correction python-scrapy "Plus d'envoi de données d'authentification avec toutes les requêtes [CVE-2021-41125] ; pas d'exposition de cookies inter-domaines lors des redirections [CVE-2022-0577]">
<correction python-udatetime "Liaison correcte avec la bibliothèque libm">
<correction qtbase-opensource-src "Correction de setTabOrder pour les <q>widgets</q> composés ; ajout d'une limite d'expansion pour les entités XML [CVE-2015-9541]">
<correction ruby-activeldap "Ajout d'une dépendance manquante à ruby-builder">
<correction ruby-hiredis "Certains tests non fiables évités afin de corriger un échec de construction">
<correction ruby-http-parser.rb "Correction d'échec de construction lors de l'utilisation de http-parser contenant la correction pour le CVE-2019-15605">
<correction ruby-riddle "Utilisation de <q>LOAD DATA LOCAL INFILE</q> permise">
<correction sctk "Utilisation de <q>pdftoppm</q> à la place de <q>convert</q> pour convertir de PDF à JPEG parce que ce dernier échoue avec le changement de politique de sécurité d'ImageMagick">
<correction twisted "Correction d'un problème de validation incorrecte des méthodes URI et HTTP [CVE-2019-12387], de validation incorrecte de certificats dans la prise en charge de XMPP [CVE-2019-12855], de problèmes de déni de service de HTTP/2, de problèmes de dissimulation de requête HTTP [CVE-2020-10108 CVE-2020-10109 CVE-2022-24801], d'un problème de divulgation d'informations quand des redirections inter-domaines sont suivies [CVE-2022-21712], d'un problème de déni de service durant une négociation de connexion SSH [CVE-2022-21716]">
<correction tzdata "Mise à jour des données de fuseau horaire pour l'Iran, le Chili et la Palestine ; mise à jour de la liste de secondes intercalaires">
<correction ublock-origin "Nouvelle version amont stable">
<correction unrar-nonfree "Correction d'un problème de traversée de répertoires [CVE-2022-30333]">
<correction wireshark "Correction d'un problème d'exécution de code à distance [CVE-2021-22191] et de problèmes de déni de service [CVE-2021-4181 CVE-2021-4184 CVE-2021-4185 CVE-2022-0581 CVE-2022-0582 CVE-2022-0583 CVE-2022-0585 CVE-2022-0586]">
</table>


<h2>Mises à jour de sécurité</h2>


<p>
Cette révision ajoute les mises à jour de sécurité suivantes à la version
oldstable. L'équipe de sécurité a déjà publié une annonce pour chacune de ces
mises à jour :
</p>

<table border=0>
<tr><th>Identifiant</th>  <th>Paquet</th></tr>
<dsa 2021 4836 openvswitch>
<dsa 2021 4852 openvswitch>
<dsa 2021 4906 chromium>
<dsa 2021 4911 chromium>
<dsa 2021 4917 chromium>
<dsa 2021 4981 firefox-esr>
<dsa 2022 5034 thunderbird>
<dsa 2022 5044 firefox-esr>
<dsa 2022 5045 thunderbird>
<dsa 2022 5069 firefox-esr>
<dsa 2022 5074 thunderbird>
<dsa 2022 5077 librecad>
<dsa 2022 5080 snapd>
<dsa 2022 5086 thunderbird>
<dsa 2022 5090 firefox-esr>
<dsa 2022 5094 thunderbird>
<dsa 2022 5097 firefox-esr>
<dsa 2022 5106 thunderbird>
<dsa 2022 5108 tiff>
<dsa 2022 5109 faad2>
<dsa 2022 5111 zlib>
<dsa 2022 5113 firefox-esr>
<dsa 2022 5115 webkit2gtk>
<dsa 2022 5118 thunderbird>
<dsa 2022 5119 subversion>
<dsa 2022 5122 gzip>
<dsa 2022 5123 xz-utils>
<dsa 2022 5126 ffmpeg>
<dsa 2022 5129 firefox-esr>
<dsa 2022 5131 openjdk-11>
<dsa 2022 5132 ecdsautils>
<dsa 2022 5135 postgresql-11>
<dsa 2022 5137 needrestart>
<dsa 2022 5138 waitress>
<dsa 2022 5139 openssl>
<dsa 2022 5140 openldap>
<dsa 2022 5141 thunderbird>
<dsa 2022 5142 libxml2>
<dsa 2022 5143 firefox-esr>
<dsa 2022 5144 condor>
<dsa 2022 5145 lrzip>
<dsa 2022 5147 dpkg>
<dsa 2022 5149 cups>
<dsa 2022 5150 rsyslog>
<dsa 2022 5151 smarty3>
<dsa 2022 5152 spip>
<dsa 2022 5153 trafficserver>
<dsa 2022 5154 webkit2gtk>
<dsa 2022 5156 firefox-esr>
<dsa 2022 5157 cifs-utils>
<dsa 2022 5158 thunderbird>
<dsa 2022 5159 python-bottle>
<dsa 2022 5160 ntfs-3g>
<dsa 2022 5164 exo>
<dsa 2022 5165 vlc>
<dsa 2022 5167 firejail>
<dsa 2022 5169 openssl>
<dsa 2022 5171 squid>
<dsa 2022 5172 firefox-esr>
<dsa 2022 5173 linux-latest>
<dsa 2022 5173 linux-signed-amd64>
<dsa 2022 5173 linux-signed-arm64>
<dsa 2022 5173 linux-signed-i386>
<dsa 2022 5173 linux>
<dsa 2022 5174 gnupg2>
<dsa 2022 5175 thunderbird>
<dsa 2022 5176 blender>
<dsa 2022 5178 intel-microcode>
<dsa 2022 5181 request-tracker4>
<dsa 2022 5182 webkit2gtk>
<dsa 2022 5185 mat2>
<dsa 2022 5186 djangorestframework>
<dsa 2022 5188 openjdk-11>
<dsa 2022 5189 gsasl>
<dsa 2022 5190 spip>
<dsa 2022 5193 firefox-esr>
<dsa 2022 5194 booth>
<dsa 2022 5195 thunderbird>
<dsa 2022 5196 libpgjava>
</table>


<h2>Paquets supprimés</h2>

<p>Les paquets suivants ont été supprimés à cause de circonstances hors de
notre contrôle :</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction elog "non maintenu ; problèmes de sécurité">
<correction libnet-amazon-perl "dépend d'une API supprimée">

</table>

<h2>Installateur Debian</h2>
<p>
L'installateur a été mis à jour pour inclure les correctifs incorporés dans
cette version de oldstable.
</p>

<h2>URL</h2>

<p>
Liste complète des paquets qui ont été modifiés dans cette version :
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>
Adresse de l'actuelle distribution oldstable :
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable/">
</div>

<p>
Mises à jour proposées à la distribution oldstable :
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable-proposed-updates">
</div>

<p>
Informations sur la distribution oldstable (notes de publication, <i>errata</i>, etc.) :
</p>

<div class="center">
  <a
  href="$(HOME)/releases/oldstable/">https://www.debian.org/releases/oldstable/</a>
</div>

<p>
Annonces et informations de sécurité :
</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>À propos de Debian</h2>
<p>
Le projet Debian est une association de développeurs de logiciels libres qui
offrent volontairement leur temps et leurs efforts pour produire le système
d'exploitation complètement libre Debian.
</p>

<h2>Contacts</h2>

<p>
Pour de plus amples informations, veuillez consulter le site Internet de
Debian <a href="$(HOME)/">https://www.debian.org/</a> ou envoyez un courrier
électronique à &lt;press@debian.org&gt; ou contactez l'équipe de publication de
la version stable à &lt;debian-release@lists.debian.org&gt;.

