#use wml::debian::translation-check translation="cb61e48c0b590145d526b25534c156232550c9c0" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pourraient conduire à une élévation de privilèges, un déni de service ou à
des fuites d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3702">CVE-2020-3702</a>

<p>Un défaut a été découvert dans le pilote pour la famille de puces
Atheros IEEE 802.11n (ath9k) permettant une divulgation d'informations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-16119">CVE-2020-16119</a>

<p>Hadar Manor a signalé une utilisation de mémoire après libération dans
l'implémentation du protocole DCCP dans le noyau Linux. Un attaquant local
peut tirer avantage de ce défaut pour provoquer un déni de service ou
éventuellement pour exécuter du code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3653">CVE-2021-3653</a>

<p>Maxim Levitsky a découvert une vulnérabilité dans l'implémentation de
l'hyperviseur KVM pour les processeurs AMD dans le noyau Linux : l'absence
de validation du champ « int_ctl » de VMCB pourrait permettre à un client
L1 malveillant d'activer la prise en charge d'AVIC (Advanced Virtual
Interrupt Controller) pour le client L2. Le client L2 peut tirer avantage
de ce défaut pour écrire dans un sous-ensemble limité mais néanmoins
relativement grand de la mémoire physique de l'hôte.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3656">CVE-2021-3656</a>

<p>Maxim Levitsky et Paolo Bonzini ont découvert un défaut dans
l'implémentation de l'hyperviseur KVM pour les processeurs AMD dans le
noyau Linux. L'absence de validation du champ « virt_ext » de VMCB pourrait
permettre à un client L1 malveillant de désactiver à la fois les
interceptions des instructions VMLOAD/VMSAVE et VLS (VMLOAD/VMSAVE
virtuels) pour le client L2. Dans ces circonstances, le client L2 peut
exécuter des instructions VMLOAD/VMSAVE non interceptées et donc lire et
écrire des portions de la mémoire physique de l'hôte.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3679">CVE-2021-3679</a>

<p>Un défaut dans la fonctionnalité du module de traçage du noyau Linux
pourrait permettre à un utilisateur local privilégié (doté de la capacité
CAP_SYS_ADMIN) de provoquer un déni de service (saturation des ressources).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3732">CVE-2021-3732</a>

<p>Alois Wohlschlager a signalé un défaut dans l'implémentation du
sous-système overlayfs, permettant à un attaquant local, doté des
privilèges pour monter un système de fichiers, de révéler des fichiers
cachés dans le montage d'origine.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3739">CVE-2021-3739</a>

<p>Un défaut de déréférencement de pointeur NULL a été découvert dans le
système de fichier btrfs, permettant à un attaquant local doté de la
capacité CAP_SYS_ADMIN de provoquer un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3743">CVE-2021-3743</a>

<p>Une lecture de mémoire hors limites a été découverte dans
l'implémentation du protocole du routeur IPC de Qualcomm, permettant de
provoquer un déni de service ou une fuite d'informations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3753">CVE-2021-3753</a>

<p>Minh Yuan a signalé une situation de compétition dans vt_k_ioctl dans
rivers/tty/vt/vt_ioctl.c, qui peut provoquer une lecture hors limites dans
le terminal virtuel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-37576">CVE-2021-37576</a>

<p>Alexey Kardashevskiy a signalé un dépassement de tampon dans le
sous-système KVM sur les plateformes powerpc. Cela permet aux utilisateurs
du système d'exploitation client de KVM de provoquer une corruption de
mémoire sur l'hôte.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-38160">CVE-2021-38160</a>

<p>Un défaut a été découvert dans virtio_console permettant la corruption
ou la perte de données par un périphérique non fiable.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-38166">CVE-2021-38166</a>

<p>Un défaut de dépassement d'entier dans le sous-système BPF pourrait
permettre à un attaquant local de provoquer un déni de service ou
éventuellement l'exécution de code arbitraire. Ce défaut est atténué par
défaut dans Debian parce que les appels à bpf() sans privilège sont
désactivés.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-38199">CVE-2021-38199</a>

<p>Michael Wakabayashi a signalé un défaut dans l'implémentation du client
NFSv4, où l'ordre incorrect des réglages de connexion permet aux opérations
d'un serveur NFSv4 distant de provoquer un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-40490">CVE-2021-40490</a>

<p>Une situation de compétition a été découverte dans le sous-système ext4
lors de l'écriture dans un fichier inline_data alors que ses attributs
étendus sont modifiés. Cela pourrait avoir pour conséquence un déni de
service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-41073">CVE-2021-41073</a>

<p>Valentina Palmiotti a découvert un défaut dans io_uring permettant à un
attaquant local d'augmenter ses privilèges.</p></li>

</ul>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 5.10.46-5. Cette mise à jour inclut des correctifs pour les
bogues nº993948 et nº993978.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de linux, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4978.data"
# $Id: $
