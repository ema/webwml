#use wml::debian::translation-check translation="d65a53d18834a941829aaf85abcf1935333ebf52" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pouvaient conduire à une élévation de privilèges, un déni de service ou des
fuites d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-4197">CVE-2021-4197</a>

<p>Eric Biederman a signalé que des vérifications de permission incorrectes
dans l'implémentation de la migration de processus de groupe de contrôle
peuvent permettre à un attaquant local d'élever ses privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0168">CVE-2022-0168</a>

<p>Un défaut de déréférencement de pointeur NULL a été découvert dans
l'implémentation du client CIFS qui peut permettre à un attaquant local
doté des privilèges CAP_SYS_ADMIN de planter le système. L'impact de
sécurité est négligeable dans la mesure où CAP_SYS_ADMIN donne par essence
la possibilité de refuser un service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1016">CVE-2022-1016</a>

<p>David Bouman a découvert un défaut dans le sous-système netfilter où la
fonction nft_do_chain n'initialisait pas les données de registre à partir
desquelles ou vers lesquelles les expressions nf_tables peuvent lire ou
écrire. Un attaquant local peut tirer avantage de cela pour lire des
informations sensibles.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1048">CVE-2022-1048</a>

<p>Hu Jiahui a découvert une situation de compétition dans le sous-système
son qui peut avoir pour conséquence une utilisation de mémoire après
libération. Un utilisateur local autorisé à accéder au périphérique son PCM
peut tirer avantage de ce défaut pour planter le système ou éventuellement
pour une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1158">CVE-2022-1158</a>

<p>Qiuhao Li, Gaoning Pan et Yongkang Jia ont découvert un bogue dans
l'implémentation de KVM pour les processeurs x86. Un utilisateur local doté
d'un accès à /dev/kvm pouvait faire que l'émulateur MMU mette à jour les
attributs d'entrée de la table de page à une mauvaise adresse. Ils
pouvaient exploiter cela pour provoquer un déni de service (corruption de
mémoire ou plantage) ou éventuellement une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1195">CVE-2022-1195</a>

<p>Lin Ma a découvert des situations de compétition dans les pilotes radio
amateur 6pack et mkiss, qui pouvaient conduire à une utilisation de mémoire
après libération. Un utilisateur local pouvait exploiter cela pour
provoquer un déni de service (corruption de mémoire ou plantage) ou
éventuellement pour une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1198">CVE-2022-1198</a>

<p>Duoming Zhou a découvert une situation de compétition dans le pilote
radio amateur 6pack qui pouvait conduire à une utilisation de mémoire après
libération. Un utilisateur local pouvait exploiter cela pour provoquer un
déni de service (corruption de mémoire ou plantage) ou éventuellement une
élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1199">CVE-2022-1199</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2022-1204">CVE-2022-1204</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2022-1205">CVE-2022-1205</a>

<p>Duoming Zhou a découvert des situations de compétition dans le protocole
radio amateur AX.25, qui pouvaient conduire à une utilisation de mémoire
après libération ou à un déréférencement de pointeur NULL. Un utilisateur
local pouvait exploiter cela pour provoquer un déni de service (corruption
de mémoire ou plantage) ou éventuellement une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1353">CVE-2022-1353</a>

<p>L'outil TCS Robot a découvert une fuite d'informations dans le
sous-système PF_KEY. Un utilisateur local peut recevoir un message netlink
quand un démon IPsec s'inscrit sur le noyau, et cela pourrait comprendre
des informations sensibles.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1516">CVE-2022-1516</a>

<p>Un défaut de déréférencement de pointeur NULL dans l'implémentation de
l'ensemble X.25 des protocoles réseau standardisés qui peut avoir pour
conséquence un déni de service.</p>

<p>Ce pilote est désactivé dans les configurations officielles du noyau de
Debian.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-26490">CVE-2022-26490</a>

<p>Des dépassements de tampon dans le pilote du cœur de ST21NFCA de
STMicroelectronics peuvent avoir pour conséquence un déni de service ou une
élévation de privilèges.</p>

<p>Ce pilote est désactivé dans les configurations officielles du noyau de
Debian.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-27666">CVE-2022-27666</a>

<p><q>valis</q> a signalé un possible dépassement de tampon dans le code de
transformation IPsec ESP. Un utilisateur local peut tirer avantage de ce
défaut pour provoquer un déni de service ou une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-28356">CVE-2022-28356</a>

<p>Beraphin a découvert que le pilote ANSI/IEEE 802.2 LLC type 2 ne
réalisait pas correctement le compte de références sur certains chemins
d'erreur. Un attaquant local peut tirer avantage de ce défaut pour
provoquer un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-28388">CVE-2022-28388</a>

<p>Une vulnérabilité de double libération de zone de mémoire a été
découverte dans le pilote d'interface USB2CAN de 8devices.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-28389">CVE-2022-28389</a>

<p>Une vulnérabilité de double libération de zone de mémoire a été
découverte dans le pilote de l'interface CAN BUS Analyzer de Microchip.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-28390">CVE-2022-28390</a>

<p>Une vulnérabilité de double libération de zone de mémoire a été
découverte dans le pilote d'interface CPC-USB/ARM7 CAN/USB d'EMS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-29582">CVE-2022-29582</a>

<p>Jayden Rivers et David Bouman ont découvert une vulnérabilité
d'utilisation de mémoire après libération dans le sous-système io_uring à
cause d'une situation de compétition dans les « timeouts » d'io_uring. Un
utilisateur local non privilégié peut tirer avantage de ce défaut pour une
élévation de privilèges.</p></li>

</ul>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 5.10.113-1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de linux, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5127.data"
# $Id: $
