#use wml::debian::template title="Debian GNU/NetBSD" BARETITLE="yes" NOHEADER="yes"
#use wml::fmt::verbatim
#use wml::debian::translation-check translation="c9a7e0f78250fe2fea728e669907c9ee47374e1c" maintainer="galaxico"

#############################################################################
<div class="important">
<p><strong>
This porting effort has long been abandoned. It has had no updates since October
2002. The information in this page is only for historical purposes.
</strong></p>
</div>

<h1>
Debian GNU/NetBSD
</h1>

<p>
Debian GNU/NetBSD (i386) was a port of the Debian Operating System to the
NetBSD kernel and libc (not to be confused with the other Debian BSD ports
based on glibc). At the time it was abandoned (around October 2002), it was
in an early stage of development - however, it was installable from scratch.
</p>

<p>
There was also an attempt to start a Debian GNU/NetBSD (alpha) port, which
could be run from a chroot in a native NetBSD (alpha) system, but was not
able to boot of its own, and was using most of the native NetBSD libraries.
A <a
href="https://lists.debian.org/debian-bsd/2002/debian-bsd-200201/msg00203.html">status
message</a> was sent to the list.
</p>

<h2>Historical News</h2>

<dl class="gloss">
  <dt class="new">2002-10-06:</dt>
  <dd>
      Experimental install floppies are now available for installing
      a Debian GNU/NetBSD system.
  </dd>
  <dt>2002-03-06:</dt>
  <dd>
      Matthew hacked <a href="https://packages.debian.org/ifupdown">ifupdown</a>
      in a workable state.
  </dd>
  <dt>2002-02-25:</dt>
  <dd>
      Matthew has reported that shadow support and PAM works on NetBSD
      now.  <a href="https://packages.debian.org/fakeroot">fakeroot</a>
      seems to work on FreeBSD, but still has issues on NetBSD.
  </dd>
  <dt>2002-02-07:</dt>
  <dd>
      Nathan has just <a
      href="https://lists.debian.org/debian-bsd/2002/debian-bsd-200202/msg00091.html">reported</a>
      that he got Debian GNU/FreeBSD to boot multiuser.  Also, he's
      working on a packages-only install (using a hacked debootstrap)
      featuring a considerably smaller tarball.
  </dd>
  <dt>2002-02-06:</dt>
  <dd>
      According to Joel gcc-2.95.4 passed most of its test-suite and
      is packaged.
  </dd>
  <dt>2002-02-06:</dt>
  <dd>X11 works on NetBSD!  Again, kudos to Joel Baker
  </dd>
  <dt>2002-02-04:</dt>
  <dd>First step towards a Debian/*BSD archive: <br />
      <a href="mailto:lucifer@lightbearer.com">Joel Baker</a>
      <a href="https://lists.debian.org/debian-bsd/2002/debian-bsd-200202/msg00067.html">
      announced</a> a <kbd>dupload</kbd>able archive for FreeBSD and
      NetBSD Debian packages.
  </dd>
  <dt>2002-02-03:</dt>
  <dd>Debian GNU/NetBSD now
      <a href="https://lists.debian.org/debian-bsd/2002/debian-bsd-200202/msg00043.html">
      self-hosting</a>!  Note that it still needs a working NetBSD for
      installation.
  </dd>
  <dt>2002-01-30:</dt>
  <dd>The Debian GNU/*BSD port now has a webpage!</dd>
</dl>

<h2>Why Debian GNU/NetBSD?</h2>

<ul>
<li>NetBSD runs on hardware unsupported by Linux. Porting Debian to
the NetBSD kernel increases the number of platforms that can run a
Debian-based operating system.</li>

<li>The Debian GNU/Hurd project demonstrates that Debian is not tied
to one specific kernel. However, the Hurd kernel was still relatively
immature - a Debian GNU/NetBSD system would be usable at a production
level.</li>

<li>Lessons learned from the porting of Debian to NetBSD can be used
in porting Debian to other kernels (such as those of <a
href="https://www.freebsd.org/">FreeBSD</a> and <a
href="https://www.openbsd.org/">OpenBSD</a>).</li>

<li>In contrast to projects like <a href="https://www.finkproject.org/">Fink</a>,
Debian GNU/NetBSD did not exist in order to provide extra software or a
Unix-style environment to an existing OS (the *BSD ports trees are already
comprehensive, and they unarguably provide a Unix-style environment).
Instead, a user or administrator used to a more traditional Debian system
would feel comfortable with a Debian GNU/NetBSD system immediately and
competent in a relatively short period of time.</li>

<li>Not everybody likes the *BSD ports tree or the *BSD userland (this
is a personal preference thing, rather than any sort of comment on
quality). Linux distributions have been produced which provide *BSD
style ports or a *BSD style userland for those who like the BSD user
environment but also wish to use the Linux kernel - Debian GNU/NetBSD
is the logical reverse of this, allowing people who like the GNU
userland or a Linux-style packaging system to use the NetBSD
kernel.</li>

<li>Because we can.</li>
</ul>

<h2>
Resources
</h2>

<p>
There is a Debian GNU/*BSD mailing list. Most of the historic discussions
about this port happened there, which are accessible from the web archives at
<url "https://lists.debian.org/debian-bsd/" />.
</p>

## Local variables:
## sgml-default-doctype-name: "HTML"
## End:
