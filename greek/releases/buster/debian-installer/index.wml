#use wml::debian::template title="Debian &ldquo;buster&rdquo; Πληροφορίες Εγκατάστασης" NOHEADER="true"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/buster/release.data"
#use wml::debian::translation-check translation="0065ae6967765345045a036d7bc028f5252036f7" maintainer="galaxico"

<h1>Εγκαθιστώντας το Debian <current_release_buster></h1>

<if-stable-release release="bullseye">
<p><strong>Το Debian 10 έχει αντικατασταθεί από το
<a href="../../bullseye/">Debian 11 (<q>bullseye</q>)</a>. Μερικές από αυτές 
τις εικόνες εγκατάστασης ενδέχεται να μην είναι διαθέσιμες ή να μην είναι πλέον 
λειτουργικές οπότε σας συνιστούμε αντί της έκδοσης buster να εγκαταστήσετε την 
έκδοση bullseye.
</strong></p>
</if-stable-release>

<if-stable-release release="buster">
<p>
Για εικόνες εγκατάστασης και τεκμηρίωση σχετικά με το πώς να εγκαταστήσετε την 
έκδοση <q>bullseye</q>
(που είναι αυτή τη στιγμή η δοκιμαστική έκδοση), δείτε τη σελίδα
<a href="$(HOME)/devel/debian-installer/">ο Εγκαταστάτης του Debian</a>.
</if-stable-release>

<if-stable-release release="buster">
<p>
<strong>Για να εγκαταστήσετε το Debian</strong> <current_release_buster>
(<em>buster</em>), μεταφορτώστε οποιαδήποτε από τις παρακάτω εικόνες (όλες οι 
εικόνες CD/DVD για τις αρχιτεκτονικές i386 και amd64 μπορούν επίσης να 
χρησιμοποιηθούν και σε κλειδί USB):
</p>

<div class="line">
<div class="item col50">
	<p><strong>εικόνες CD netinst (γενικά 170-470 MB)</strong></p>
		<netinst-images />
</div>


</div>

<div class="line">
<div class="item col50">
	<p><strong>πλήρη σετ CD</strong></p>
		<full-cd-images />
</div>

<div class="item col50 lastcol">
	<p><strong>πλήρη σετ DVD</strong></p>
		<full-dvd-images />
</div>

</div>

<div class="line">
<div class="item col50">
<p><strong>CD (μέσω <a href="$(HOME)/CD/torrent-cd">BitTorrent</a>)</strong></p>
<full-cd-torrent />
</div>

<div class="item col50 lastcol">
<p><strong>DVD (μέσω <a 
href="$(HOME)/CD/torrent-cd">BitTorrent</a>)</strong></p>
<full-dvd-torrent />
</div>

</div>

<div class="line">
<div class="item col50">
<p><strong>CD (μέσω <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<p><strong>DVD (μεω <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-dvd-jigdo />
</div>


</div>

<div class="line">
<div class="item col50">
<p><strong>Blu-ray  (μέσω <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-bluray-jigdo />
</div>

<div class="item col50 lastcol">
<p><strong>άλλες εικόνες (netboot, flexible usb stick, κλπ.)</strong></p>
<other-images />
</div>
</div>

<div id="firmware_nonfree" class="warning">
<p>
Αν οποιοδήποτε από το υλικό στο σύστημά σας <strong>απαιτεί τη 
φόρτωση μη-ελεύθερου λογισμικού</strong> με τον οδηγό της συσκευής, μπορείτε να 
χρησιμοποιήσετε ένα από τα
<a 
href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/buster/
current/">συμπιεσμένα αρχεία διαδεδομένων πακέτων υλισμικού (firmware) 
</a> ή να μεταφορτώσετε μια <strong>ανεπίσημη</strong> εικόνα που περιλαμβάνει 
αυτό το <strong>μη-ελεύθερο</strong> υλισμικό. Οδηγίες για το πώς να 
χρησιμοποιήσετε αυτά τα συμπιεσμένα αρχεία και γενικές πληροφορίες σχετικά με 
τη φόρτωση υλισμικού στη διάρκεια μιας εγκατάστασης μπορούν να βρεθούν στον 
Οδηγό Εγκατάστασης (δείτε την τεμκηρίωση παρακάτω).
</p>
<div class="line">
<div class="item col50">
<p><strong>netinst (γενικά 240-290 MB) <strong>μη-ελεύθερες</strong>
εικόνες CD <strong>με υλισμικό</strong></strong></p>
<small-non-free-cd-images />
</div>
</div>
</div>



<p>
<strong>Σημειώσεις</strong>
</p>
<ul>
    <li>
	Για τη μεταφόρτωση πλήρους σετ CD και DVD συνιστάται η χρήση BitTorrent ή
jigdo.
    </li><li>
	Για τις λιγότερο διαδεδομένες αρχιτεκτονικές μόνο ένας περιορισμένος 
αριθμός εικόνων από τα σετ των CD και DVD είναι διαθέσιμα ως αρχεία ISO ή μέσω 
BitTorrent.
	Τα πλήρη σετ είναι διαθέσιμα μόνο μέσω jigdo.
    </li><li>
	Οι εικόνες <em>CD</em> πολλαπλών αρχιτεκτονικών υποστηριζουν τις 
i386/amd64· η εγκατάσταση είναι ανάλογη της εγκατάστασης με από μια εικόνα 
netinst για μια μοναδική αρχιτεκτονική.
    </li><li>
	Η εικόνα <em>DVD</em> πολλαπλών αρχιτεκτονικών υποστηρίζει τις 
αρχιτεκτονικές i386/amd64· η εγκατάσταση είναι ανάλογη της εγκατάστασης από μια
πλήρη εικόνα CD για μια αρχιτεκτονική· το DVD περιλαμβάνει επίσης τον πηγαίο 
κώδικα για όλα τα πακέτα που περιέχονται σ' αυτό.
    </li><li>
	Για τις εικόνες εγκατάστασης, διατίθενται αρχεία επαλήθευσης 
(<tt>SHA256SUMS</tt>,
	<tt>SHA512SUMS</tt> και άλλα) από τον ίδιο κατάλογο στον οποίο βρίσκονται 
και οι εικόνες.
    </li>
</ul>


<h1>Τεκμηρίωση</h1>

<p>
<strong>Αν πρόκειται να διαβάστε ένα και μόνο κείμενο</strong> πριν την 
εγκατάσταση, τότε διαβάστε το
<a href="../i386/apa">Installation Howto</a>, μια γρήγορη περιήγηση στη 
διαδικασία εγκατάστασης. Άλλη χρήσιμη τεκμηρίωση περιλαμβάνει:
</p>

<ul>
<li><a href="../installmanual">Οδηγός εγκατάστασης του Buster</a><br />
λεπτομερείς οδηγίες εγκατάστασης</li>
<li><a 
href="https://wiki.debian.org/DebianInstaller/FAQ">Συχνές 
ερωτήσεις του Εγκαταστάτη του (Debian-Installer FAQ)</a>
και <a href="$(HOME)/CD/faq/">Συχνές ερωτήσεις των Debian-CD</a><br />
κοινές ερωτήσεις και απαντήσεις</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Wiki του 
Εγκαταστάτη του Debian</a><br />
τεκμηρίωση που συντηρείται από την κοινότητα</li>
</ul>

<h1 id="errata">Παροράματα</h1>

<p>
Αυτή είναι μια λίστα με γνωστά προβλήματα του εγκαταστάτη που 
περιλαμβάνεται με την έκδοση του Debian <current_release_buster>. Αν είχατε 
κάποιο πρόβλημα με την εγκατάσταση του Debian και δεν βλέπετε το πρόβλημα αυτό 
να περιέχεται στην παρούσα λίστα, παρακαλούμε στείλτε μας μια 
<a href="$(HOME)/Bugs/Reporting">αναφορά εγκατάστασης</a>
περιγράφοντας το πρόβλημα ή
<a href="https://wiki.debian.org/DebianInstaller/BrokenThings">ελέγξτε το 
wiki</a> για άλλα γνωστά προβλήματα.
</p>

## Translators: copy/paste from devel/debian-installer/errata
<h3 id="errata-r0">Παροράματα για την έκδοση 10.0</h3>

<dl class="gloss">

<!--
     <dt>Desktop installations may not work using CD#1 alone</dt>

     <dd>Due to space constraints on the first CD, not all of the
     expected GNOME desktop packages fit on CD#1. For a successful
     installation, use extra package sources (e.g. a second CD or a
     network mirror) or use a DVD instead.

     <br /> <b>Status:</b> It is unlikely more efforts can be made to
     fit more packages on CD#1. </dd>
-->
</dl>

<p>
Βελτιωμένες εκδόσεις του συστήματος εγκατάστασης είναι υπό ανάπτυξη για την 
επόμενη κυκλοφορία του Debian, και μπορούν επίσης να χρησιμοποιηθούν για να 
εγκαταστήσετε το Debian buster. Για λεπτομέρειες, δείτε τη σελίδα του
<a href="$(HOME)/devel/debian-installer/">To Σχέδιο του Εγκαταστάτη του 
Debian</a>.
</p>
</if-stable-release>
