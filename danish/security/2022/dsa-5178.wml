#use wml::debian::translation-check translation="006fd719ddd7701428df2e4ab411c878c33ce4ba" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Denne opdatering indeholder opdateret CPU-mikrokode til nogle af Intel 
CPU-typer, med afhjælpelse af sikkerhedssårbarheder.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-21123">CVE-2022-21123</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2022-21125">CVE-2022-21125</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2022-21127">CVE-2022-21127</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2022-21166">CVE-2022-21166</a>

    <p>Forskellige efterforskere opdagede fejl i Intel-processorer, der under ét 
    refereres til som MMIO Stale Data-sårbarheder, hvilke kunne medføre 
    informationslækager til lokale brugere.</p>

    <p>For flere oplysninger, se: 
    <a href="https://www.intel.com/content/www/us/en/developer/articles/technical/software-security-guidance/technical-documentation/processor-mmio-stale-data-vulnerabilities.html">\
    https://www.intel.com/content/www/us/en/developer/articles/technical/software-security-guidance/technical-documentation/processor-mmio-stale-data-vulnerabilities.html</a></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-21151">CVE-2022-21151</a>

    <p>Alysa Milburn, Jason Brandt, Avishai Redelman og Nir Lavi opdagede at 
    ved nogle Intel-processorer, kunne optimeringsfjernelse eller -ændring 
    af sikkerhedskritisk kode, medføre informationsafsløring to lokale 
    brugere.</p></li>

</ul>

<p>I den gamle stabile distribution (buster), er disse problemer rettet
i version 3.20220510.1~deb10u1.</p>

<p>I den stabile distribution (bullseye), er disse problemer rettet i
version 3.20220510.1~deb11u1.</p>

<p>Vi anbefaler at du opgraderer dine intel-microcode-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende intel-microcode, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/intel-microcode">\
https://security-tracker.debian.org/tracker/intel-microcode</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5178.data"
