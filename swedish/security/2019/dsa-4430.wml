#use wml::debian::translation-check translation="907a0371eb05342911768c66ad56f028349d3301" mindelta="1"
<define-tag description>säkerhetsuppdatering</define-tag>
<define-tag moreinfo>
<p>Mathy Vanhoef (NYUAD) och Eyal Ronen (Tel Avivs Universitet &amp; KU Leuven)
upptäckte flera sårbarheter i WPA-implementation som finns wpa_supplication
(station) och hostapd (accesspunkt). Dessa såbarheter är även kollektivt kända
som <q>Dragonblood</q>.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9495">CVE-2019-9495</a>

	<p>Cache-baserat sidokanalsangrepp mot EAP-pwd-implementationen: en
	angripare med möjlighet att köra icke priviligierad kod kod på målmaskinen
	(inklusive exempelvis javaskriptkod i en webbläsare på en mobiltelefon)
	under en handskakning kunde få tag på tillräckligt med information för att
	upptäcka lösenordet i ett ordboksangrepp.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9497">CVE-2019-9497</a>

	<p>Reflektionsangrepp mot EAP-pwd-serverimplementationen: en brist på
	validering av mottagna skalärer samt elementvärden i
	EAP-pwd-Commit-meddelanden kunde resultera i attacker som kunde ha möjlighet
	till fullständig EAP-pwd auktoriseringsutbyten utan att angriparen behöver
	veta lösenordet. Detta resulterar inte i att angriparen har möjlighet att
	härleda sessionnyckeln, fullfölja den efterförljande nyckelutbytet och få
	åtkomst till nätverket.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9498">CVE-2019-9498</a>

	<p>EAP-pwd-server saknar commitvalidering för scalärer/element: hostapd
	validerar inte värden som mottagits i EAP-pwd-Commit-meddelandet, så en
	angripare kunde använda ett speciellt skapat commitmeddelande för att
	manipulera utbytet för att få hostapd att härleda en sessionsnyckel från
	en begränsad uppsättning möjliga värden. Detta kunde resultera i att en
	angripare får möjlighet att slutföra auktorisering och få åtkomst till
	nätverket.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9499">CVE-2019-9499</a>

	<p>EAP-pwd peer saknar commitvalidering för skalärer/element: wpa_supplicant
	validerar inte värden som mottagits i EAP-pwd-Commitmeddelandet, så en
	angripare kunde använda ett speciellt skapat commit-meddelande för att
	manipulera utbytet för att få wpa_supplicant att härleda en sessionsnyckel
	från en begränsad uppsättning av möjliga värden. Detta kunde resultera i att en
	angripare får möjlighet att slutföra auktorisering och uppträder som en
	illasinnad accesspunkt.</p></li>

</ul>

<p>Notera att Dragonblood-monikern även applicerar på 
<a href="https://security-tracker.debian.org/tracker/CVE-2019-9494">\
CVE-2019-9494</a> och <a href="https://security-tracker.debian.org/tracker/CVE-2014-9496">\
CVE-2014-9496</a> som är sårbarheter i SAE-protokollet i WPA3. SAE är inte
aktiverat i Debian Stetchs byggen av wpa, som därmed inte är sårbar som standard.</p>

<p>På grund av komplexiteten i bakåtanpassningsprocessen, är rättningen för
dessa sårbarheter partiell. Användare rekommenderas att använda starka
lösenord för att förhindra ordboksangrepp eller använda en 2.7-baserad
version från stretch-backports (versioner över 2:2.7+git20190128+0c1e29f-4).</p>

<p>För den stabila utgåvan (Stretch) har dessa problem rättats i
version 2:2.4-1+deb9u3.</p>

<p>Vi rekommenderar att ni uppgraderar era wpa-paket.</p>

<p>För detaljerad säkerhetsstatus om wpa vänligen se
dess säkerhetsspårare på
<a href="https://security-tracker.debian.org/tracker/wpa">\
https://security-tracker.debian.org/tracker/wpa</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4430.data"
