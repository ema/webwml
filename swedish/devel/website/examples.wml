#use wml::debian::template title="Exempel"
#use wml::debian::translation-check translation="d1af73e44d054ea9925b972ae2cb196c0365a2bb"

<h3>Exempel på att påbörja en översättning</h3>

<p>Vi använder här franska som exempel

<pre>
   git pull
   cd webwml
   mkdir french
   cd french
   cp ../english/.wmlrc ../english/Make.* .
   echo '<protect>include $(subst webwml/french,webwml/english,$(CURDIR))/Makefile</protect>' &gt; Makefile
   mkdir po
   git add Make*.wmlrc
   cp Makefile po
   make -C po init-po
   git add po/Makefile po/*.fr.po
</pre>

<p>Redigera filen <tt>.wmlrc</tt> och ändra:

<ul>
  <li>"-D CUR_LANG=English" till "-D CUR_LANG=French"
  <li>"-D CUR_ISO_LANG=en" till "-D CUR_ISO_LANG=fr"
  <li>"-D CUR_LOCALE=en_US" till "-D CUR_LOCALE=fr_FR"
  <li>"-D CHARSET=iso-8859-1" till vad som är lämpligt.<br>
      Franska råkar bara använda samma teckenkodning som engelska, så inga
      ändringar behövs, men det är dock troligt att nya språk kommer att
      behöva justera denna inställning.
</ul>

<p>Redigera Make.lang och ändra "LANGUAGE := en" till "LANGUAGE := fr".
Om du översätter till ett språk som använder en 
multibyteteckenuppsättning kan du behöva ändra andra variabler i filen.
För ytterligare information, läsa ../Makefile.common och kanske andra fungerande
exempel (översättningar såsom kinesiska).

<p>
Gå till french/po och översätt texterna i PO-filerna.
Detta bör vara rätt så självförklarande.
</p>

<p>Se alltid till att du kopierar Makefilen för varje katalog du översätter.
Detta krävs eftersom programmet <code>make</code> används för att konvertera
.wml-filerna till HTML, och <code>make</code> använder Makefiler.

<p>När du är klar med att redigera sidorna skriver du
<pre>
   git commit -m"Lägg till en beskrivning av dina ändringar här på engelska"
   git push
</pre>
från webwml-katalogen.
Du kan nu börja översätta sidorna.

<h3>Exempel på att översätta en sida</h3>

<p>En fransk översättning av det sociala kontraktet används som exempel:

<pre>
   cd webwml
   ./copypage.pl english/social_contract.wml
   cd french
</pre>

<p>Detta kommer automatiskt lägga till huvudet translation-check, som pekar ut den
version av originalfilen som kopierades. Det skapar också målkatalogen
och makefilen, om den saknas.</p>

<p>Redigera social_contract.wml och översätt texten.
Försök inte att översätta länkar eller att ändra dem på något sätt - om du vill
ändra något, begär ändringen på debian-www-listan.
När du är klar, skriv

<pre>
   cvs commit -m "kort beskrivning av ändringarna" social_contract.wml
</pre>

<h3>Exempel på att lägga till en ny katalog</h3>

<p>Detta exempel visar hur den franska översättningen lägger till katalogen
intro/:

<pre>
   cd webwml/french
   mkdir intro
   cd intro
   cp ../Makefile .
   git add Makefile
   git commit -m"Added the intro dir to git"
   git push
</pre>

<p>
Se till att den nya katalogen har en Makefile, och att den läggs till Git.
Om så inte är fallet, kommer make ge ett felmeddelande för alla som kör det.
</p>

<h3>Exempel på en konflikt</h3>

<p>Detta exempel visar en commit som inte kommer att fungera eftersom
kopian i förrådet har modifierats sedan din senaste <kbd>git pull</kbd>.</p>

<p>Du gjorde några förändringar på filen foo.wml, för att sedan:</p>

 <pre>
    git add foo.wml
    git commit -m "fixed a broken link"
    git push
 </pre>

vilket kommer att ge utdatan:

 <pre>
    To salsa.debian.org:webmaster-team/webwml.git
     ! [rejected]                master -> master (fetch first)
    error: failed to push some refs to 'git@salsa.debian.org:webmaster-team/webwml.git'
</pre>

<p>
eller något liknande :)
<br />
<br />
Detta betyder att förändringarna <strong>inte</strong> har skickats
till gitförrådet, till följd av konflikter.
<br />
Du kommer att behöva undersöka vad som gick fel, lösa konflikterna och
försöka att göra en commit/push igen.</p>

