#use wml::debian::template title="Var utvecklarna finns"
#use wml::debian::translation-check translation="bd2e76d96db915ccd0dee367a373417db7422565"


<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Var finns Debianutvecklarna (DD)? Om en DD har specificerat sina hemkoordinater i utvecklardatabasen så finns det synligt på vår världskarta.</p>
</aside>

<p>Kartan nedan genererades från en
<a href="developers.coords">lista över utvecklarkoordinater</a>
(med namnen borttagna) med hjälp av programmet
<a href="https://packages.debian.org/stable/graphics/xplanet">
xplanet</a>.
</p>

<img src="developers.map.jpeg" alt="Världskarta">

<h2>Hur du lägger till dina koordinater</h2>

<p>Om du är utvecklare och önskar lägga till dina koordinater till
din post i databasen, logga in på
<a href="https://db.debian.org">Debians utvecklardatabas</a>
och modifiera din listning.
Om du inte vet koordinaterna för din hemstad så kan du använda
<a href="https://www.openstreetmap.org">OpenStreetMap</a> för att kolla upp
dom. Sök efter din hemstad och välj riktningspilarna bredvid sökfältet.
Dra sedan den gröna markören
på OSM-kartan. Koordinaterna kommer sedan att dyka upp i <em>från</em>-fältet.
</p>


<p>Formatet på koordinaterna är ett av följande:</p>
<dl>
<dt>Decimala grader
<dd>Formatet är +-GGG.GGGGGGGGGGGGGGG.
    Detta är formatet program som xearth använder, och formatet många
    positionswebbplatser använder.
    Vanligtvis är dock precisionen begränsad till fyra eller fem decimaler.

<dt>Grader och minuter (DGM)
<dd>Formatet är +-GGGMM.MMMMMMMMMMMMM.
    Det är inte en aritmetisk typ, utan en packad representation av
    två separata enheter, grader och minuter.
    Denna utdata är vanlig från några typer av handhållna GPS-enheter och
    från GPS-meddelanden på NMEA-format.

<dt>Grader, minuter och sekunder (DGMS)
<dd>Formatet är +-GGGMMSS.SSSSSSSSSSS.
    Precis som DGM är detta inte en aritmetisk typ, utan en packad
    representation av tre separata enheter: grader, minuter och
    sekunder.
    Denna utdata kommer vanligtvis från webbplatser som ger tre värden
    för varje position.
    Om positionen t.ex ges som 34:50:12.24523 nord skulle den i
    DGMS-formatet bli +0345012.24523.
</dl>

<p><strong>Vänligen notera:</strong> För latitud är <code>+</code> nord, för longitud är <code>+</code> öst.
Det är viktigt att ge tillräckligt många inledande nollor för att skilja
ut formatet som används om din position är mindre än 2 grader från en
nollpunkt.
</p>
