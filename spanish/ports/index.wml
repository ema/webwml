#use wml::debian::template title="Adaptaciones a otras arquitecturas"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="25e271219c968b8eb71d77250152133a3cc10f57"
#use wml::debian::toc

<toc-display/>

<toc-add-entry name="intro">Introducción</toc-add-entry>
<p>
Como la mayoría de ustedes sabe,
<a href="https://www.kernel.org/">Linux</a> solo es un núcleo.
Durante mucho tiempo el núcleo Linux solo funcionaba en la serie de
máquinas x86 de Intel, desde el 386 en adelante.
</p>
<p>
Sin embargo, hoy día esto ya no es cierto. El núcleo Linux se ha 
adaptado a una larga y creciente lista de arquitecturas. Siguiendo
esos pasos, hemos adaptado la distribución Debian a estas plataformas.
En general este proceso tiene un comienzo difícil (hay que conseguir
que la libc y el enlazador dinámico funcionen sin trabas), luego sigue un trabajo
relativamente largo y rutinario, de conseguir recompilar todos los
paquetes bajo las nuevas arquitecturas.
</p>
<p>
Debian es un sistema operativo, no un núcleo (en realidad es más que un SO, ya
que incluye miles de aplicaciones). Para probar esta afirmación, aun cuando la 
mayor parte de adaptaciones se hacen sobre núcleos Linux, también existen
adaptaciones basadas en los núcleos FreeBSD, NetBSD y Hurd.
</p>

<div class="important">
<p>
Esta página está en desarrollo. No todas las
adaptaciones tienen su página todavía, y la mayoría de ellas están en
servidores externos. Estamos trabajando para que la información
reunida sobre todas las arquitecturas se refleje y actualice junto con
el resto del sitio web de Debian.
Se pueden <a href="https://wiki.debian.org/CategoryPorts">encontrar</a> más
adaptaciones en la wiki.
</p>
</div>

<toc-add-entry name="portlist-released">Listado de adaptaciones oficiales</toc-add-entry>
<br />

<table class="tabular" summary="">
<tbody>
<tr>
<th>Adaptación</th>
<th>Arquitectura</th>
<th>Descripción</th>
<th>Estado</th>
</tr>
<tr>
<td><a href="amd64/">amd64</a></td>
<td>PC de 64 bits (amd64)</td>
<td>Publicada oficialmente por primera vez en Debian 4.0.
 Adaptación a los procesadores de 64 bits x86. El objetivo es
 soportar espacios de usuario tanto de 32 como de 64 bits en esta arquitectura.
 Esta adaptación permite usar los Opteron de 64 bits de AMD, los procesadores 
 Athlon y Sempron, y los procesadores de Intel con soporte Intel 64, incluyendo Pentium D
 y varias series de Xeon y Core.</td>
<td><a href="$(HOME)/releases/stable/amd64/release-notes/">publicada</a></td>
</tr>
<tr>
<td><a href="arm/">arm64</a></td>
<td>ARM de 64 bits (AArch64)</td>
<td>La versión 8 de la arquitectura ARM incluye AArch64, un nuevo juego 
de instrucciones de 64 bits. Desde Debian 8.0, se ha incluido la adaptación arm64
en Debian, para dar soporte a este nuevo juego de instrucciones en
procesadores como el Applied Micro X-Gene, AMD Seattle y Cavium ThunderX.</td>
<td><a href="$(HOME)/releases/stable/arm64/release-notes/">publicada</a></td>
</tr>
<tr>
<td><a href="arm/">armel</a></td>
<td>EABI ARM</td>
<td>La más antigua de las adaptaciones actuales de Debian a ARM, tiene
soporte para las CPU ARM little-endian compatibles con el juego de instrucciones
v5te.</td>
<td><a href="$(HOME)/releases/stable/armel/release-notes/">publicada</a></td>
</tr>
<tr>
<td><a href="arm/">armhf</a></td>
<td>ABI ARM de punto flotante</td>
<td>Muchas de las modernas placas y dispositivos ARM de 32 bits se lanzan con una unidad
de punto flotante (FPU), pero la adaptación armel de Debian no les saca
provecho. La adaptación armhf fue iniciada para mejorar esta situación y también
para aprovechar otras características de los nuevos procesadores ARM. Esta
adaptación requiere al menos un procesador ARMv7 con soporte de punto flotante Thumb-2 y
VFPv3-D16.</td>
<td><a href="$(HOME)/releases/stable/armhf/release-notes/">publicada</a></td>
</tr>
<tr>
<td><a href="i386/">i386</a></td>
<td> PC de 32 bits (i386)</td>
<td>Es la primera arquitectura y, estrictamente hablando, no es una
adaptación. Linux se desarrolló originalmente en procesadores Intel 386, y
de ahí el nombre. Debian permite usar todos los procesadores IA-32, hechos por
Intel (incluyendo todas las series de Pentium y las recientes máquinas 
Core Duo en modo de 32 bits), AMD (K6, todas las series de Athlon
 y las series Athlon64 en modo de 32 bits), Cyrix y otros fabricantes.</td>
<td><a href="$(HOME)/releases/stable/i386/release-notes/">publicada</a></td>
</tr>
<tr>
<td><a href="mipsel/">mipsel</a></td>
<td>MIPS (modo little-endian)</td>
<td>Publicada oficialmente por primera vez en Debian 3.0.
Se está adaptando Debian a la arquitectura MIPS, usada en
máquinas SGI (debian-mips — big-endian) y DECstations de Digital
(debian-mipsel — little-endian).</td>
<td><a href="$(HOME)/releases/buster/mipsel/release-notes/">publicada</a></td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/mips64el">mips64el</a></td>
<td>MIPS (64 bits modo little-endian)</td>
<td>
Está adaptación es little-endian, usa la ABI N64, la ISA MIPS64r1 y hardware de 
punto flotante.
Parte de la publicación oficial desde la versión Debian 9.
</td>
<td><a href="$(HOME)/releases/stable/mips64el/release-notes/">publicada</a></td> 
</tr>
<tr>
<td><a href="powerpc/">ppc64el</a></td>
<td>POWER7+, POWER8</td>
<td>Publicada oficialmente por primera vez en Debian 8.0. Adaptación little-endian de ppc64,
 usando la nueva Open Power ELFv2 ABI.</td>
<td><a href="$(HOME)/releases/stable/ppc64el/release-notes/">publicada</a></td>
</tr>
<tr>
<td><a href="s390x/">s390x</a></td>
<td>System z</td>
<td>Publicada oficialmente por primera vez en Debian 7.0. Entorno de 64 bits para servidores IBM System z.</td>
<td><a href="$(HOME)/releases/stable/s390x/release-notes/">publicada</a></td>
</tr>
</tbody>
</table>

<toc-add-entry name="portlist-other">Lista de otras adaptaciones</toc-add-entry>

<div class="tip">
<p>
Existen imágenes no oficiales de instalación para algunas de las siguientes adaptaciones, disponibles
en <a href="https://cdimage.debian.org/cdimage/ports">https://cdimage.debian.org/cdimage/ports</a>.
Los correspondientes equipos de adaptación de Debian mantienen dichas imágenes.
</p>
</div>

<table class="tabular" summary="">
<tbody>
<tr>
<th>Adaptación</th>
<th>Arquitectura</th>
<th>Descripción</th>
<th>Estado</th>
</tr>
<tr>
<td><a href="alpha/">alpha</a></td>
<td>Alpha</td>
<td>Publicada de manera oficial en Debian 2.1. No cumplió con los
criterios de publicación en el lanzamiento de Debian 6.0 <q>squeeze</q>, y por
consiguiente, fue eliminada del archivo.
</td>
<td>discontinuada</td>
</tr>
<tr>
<td><a href="arm/">arm</a></td>
<td>OABI ARM</td>
<td>
Esta adaptación funciona en diverso hardware embebido, tal como routers o
dispositivos NAS. La adaptación arm fue lanzada por primera vez en Debian 2.2 y
mantenida hasta Debian Debian 5.0, cuando fue reemplazada por armel.
</td>
<td>reemplazada por armel</td>
</tr>
<tr>
<td><a href="https://web.archive.org/web/20130326061253/http://avr32.debian.net/">AVR32</a></td>
<td>Atmel de 32 bits RISC</td>
<td>Adaptación a la aquitectura RISC 32 de bits de Atmel, AVR32. </td>
<td>discontinuada</td>
</tr>
<tr>
<td><a href="hppa/">hppa</a></td>
<td>HP PA-RISC</td>
<td>Publicada de manera oficial con Debian 3.0 <q>woody</q>, esta es
una apdaptación a la arquitectura PA-RISC de Hewlett-Packard.
No cumplió con los criterios de publicación en el lanzamiento de Debian 6.0 
<q>squeeze</q>, y por consiguiente, fue eliminada del archivo.
</td>
<td>discontinuada</td>
</tr>
<tr>
<td><a href="hurd/">hurd-i386</a></td>
<td>PC de 32 bits (i386)</td>
<td>GNU Hurd es un sistema operativo totalmente nuevo puesto en marcha
por el grupo de GNU. De hecho, el HURD de GNU es el componente final que
hace posible construir un S.O. completo GNU, y Debian GNU/Hurd va a
ser uno de tales (posiblemente el primero) sistemas operativos GNU. El
proyecto actual está basado en la arquitectura i386.
</td>
<td>en desarollo</td>
</tr>
<tr>
<td><a href="ia64/">ia64</a></td>
<td>Intel Itanium IA-64</td>
<td>Publicada oficialmente por primera vez en Debian 3.0.
Es la adaptación a la primera arquitectura de 64 bits de Intel.
Nota: esto <em>no</em> se debería confundir con las últimas extensiones de
Intel de 64 bits para procesadores Pentium 4 y Celeron, denominadas Intel 64;
para estas mire la adaptación <a href="amd64/">AMD64</a>. En Debian 8 la
adaptación ia64 fue removida de la publicación debido a que no había
soporte por parte de los desarrolladores.</td>
<td>discontinuada</td>
</tr>
<tr>
<td><a href="kfreebsd-gnu/">kfreebsd-amd64</a></td>
<td>PC de 64 bits (amd64)</td>
<td>Publicada oficialmente por primera vez en Debian 6.0 como una 
tecnología preliminar y la primera adaptación no Linux de Debian. Adaptación
del núcleo FreeBSD al sistema GNU Debian. Esta adaptación ya no forma parte
de la publicación oficial desde Debian 8.</td>
<td>en desarrollo</td>
</tr>
<tr>
<td><a href="kfreebsd-gnu/">kfreebsd-i386</a></td>
<td>PC 32 de bits (i386)</td>
<td>Publicada oficialmente por primera vez en Debian 6.0 como una 
tecnología preliminar y la primera adaptación no Linux de Debian. Adaptación
del núcleo FreeBSD al sistema GNU Debian. Esta adaptación ya no forma parte
de la publicación oficial desde Debian 8.</td>
<td>en desarrollo</td>
</tr>
<tr>
<td><a href="http://www.linux-m32r.org/">m32</a></td>
<td>M32R</td>
<td>Adaptación al microprocesador RISC de 32 bits de Renesas Technology.</td>
<td>abandonada</td>
</tr>
<tr>
<td><a href="m68k/">m68k</a></td>
<td>Motorola 68k</td>
<td>Oficialmente salió por primera vez con Debian 2.0. La adaptación no pasó
los criterios de publicación para Debian 4.0 y, por tanto, no se incluyó en la
publicación de Etch y publicaciones posteriores y se ha movido a
<a href="http://www.debian-ports.org">debian-ports</a>.
La versión Debian para m68k funciona en
una amplia variedad de computadoras basadas en la serie de
procesadores 68k de Motorola, en particular, la gama de estaciones de
trabajo Sun3 y las computadoras personales Macintosh de Apple, Atari y
Amiga.</td>
<td>en desarrollo</td>
</tr>
<tr>
<td><a href="mips/">mips</a></td>
<td>MIPS (modo big-endian)</td>
<td>Publicada oficialmente por primera vez en Debian 3.0.
Se está adaptando Debian a la arquitectura MIPS, usada en
máquinas SGI (debian-mips — big-endian) y DECstations de Digital
(debian-mipsel — little-endian).
La adaptación de Debian a MIPS se discontinuó después de <a href="$(HOME)/releases/stable/mips/release-notes/">Debian 10 (Buster)</a></td>
<td><a href="https://lists.debian.org/debian-release/2019/08/msg00582.html">discontinuada</a></td>
</tr>
<tr>
<td><a href="netbsd/">netbsd-i386</a></td>
<td>PC de 32 bits (i386)</td>
<td>Una adaptación al núcleo y libc NetBSD del sistema operativo
Debian completo, con apt, dpkg y programas de GNU.
La adaptación, que nunca ha sido publicada, se ha abandonado.</td>
<td>abandonada</td>
</tr>
<tr>
<td>netbsd-alpha</td>
<td>Alpha</td>
<td>Una adaptación al núcleo y libc NetBSD del sistema operativo
Debian completo, con apt, dpkg y programas de GNU.
La adaptación, que nunca ha sido publicada, se ha abandonado.</td>
<td>abandonada</td>
</tr>
<tr>
<td><a href="https://web.archive.org/web/20150905061423/http://or1k.debian.net/">or1k</a></td>
<td>OpenRISC 1200</td>
<td>Una adaptación a la CPU de código abierto <a href="https://openrisc.io/">OpenRISC</a> 1200.</td>
<td>abandonada</td>
</tr>
<tr>
<td><a href="powerpc/">powerpc</a></td>
<td>Motorola/IBM PowerPC</td>
<td>Hecha pública oficialmente con Debian 2.2.
La adaptación se ejecuta de forma estable en muchos modelos de Apple
Macintosh PowerMac, y en las máquinas de arquitecturas abiertas CHRP y PReP.
No es parte de la publicación oficial desde Debian 9.</td>
<td>discontinuada</td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/PowerPCSPEPort">powerpcspe</a></td>
<td>PowerPC Signal Processing Engine</td>
<td>
Una adaptación para el hardware «Signal Processing Engine» presente en
 dispositivos FreeScale de bajo consumo de 32 bits y en la CPU "e500" de IBM.
</td>
<td>en desarrollo</td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/RISC-V">riscv64</a></td>
<td>RISC-V (64 bits little-endian)</td>
<td>Adaptación para <a href="https://riscv.org/">RISC-V</a>, una ISA libre/abierta, en particular para la variante little-endian de 64 bits.</td>
<td>en desarrollo</td>
</tr>
<tr>
<td><a href="s390/">s390</a></td>
<td>S/390 y zSeries</td>
<td>Publicada de manera oficial en Debian 3.0. Adaptación para 
los servidores S/390 de IBM. Esta adaptación fue reemplazada
por la adaptación S390x en Debian 8.</td>
<td>reemplazada por s390x</td>
</tr>
<tr>
<td><a href="sparc/">sparc</a></td>
<td>Sun SPARC</td>
<td>Oficialmente salió por primera vez con Debian 2.1.
Esta adaptación funciona sobre la gama de estaciones de trabajo Sun
UltraSPARC, así como sobre alguna de sus sucesoras en la
arquitectura sun4. Desde la publicación Debian 8 Sparc no es una
arquitectura a publicar, esto debido a que no hay suficiente soporte por
parte de los desarrolladores. Esta adaptación ha sido reemplazada
por la sparc64.</td>
<td>reemplazada por sparc64</td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/Sparc64">sparc64</a></td>
<td>SPARC de 64 bits</td>
<td>
Adaptación para los procesadores SPARC de 64 bits.
</td>
<td>en desarrollo</td>
</tr>
 
<tr>
<td><a href="https://wiki.debian.org/SH4">sh4</a></td>
<td>SuperH</td>
<td>Adaptación a los procesadores Hitachi SuperH. También soporta
los procesadores <a href="https://j-core.org/">J-Core</a>.</td>
<td>en desarrollo</td>
</tr>
<tr>
<td><a href="https://wiki.debian.org/X32Port">x32</a></td>
<td>PC de 64 bits con punteros de 32 bits</td>
<td>
X32 es una ABI para CPUs amd64/x86_64 que usa punteros de 32 bits.
La idea es reunir el amplio conjunto de instrucciones de x86_64 con 
el bajo consumo de memoria y cache que conlleva usar punteros de 32 bits.
</td>
<td>en desarrollo</td>
</tr>
</tbody>
</table>



<div class="note">
<p>Muchos de los nombres de computadoras y procesadores citados
anteriormente son marcas comerciales y marcas registradas de sus
respectivos fabricantes.</p>
</div>
