#use wml::debian::translation-check translation="a3ce03f0ff8939281b7a4da3bb955c91e6857f6f" maintainer="Erik Pfannenstein"
<define-tag pagetitle>Ampere spendet Arm64-Hardware an Debian, um das Arm-Ökosystem zu stärken</define-tag>
<define-tag release_date>2020-06-16</define-tag>
#use wml::debian::news

# Status: [content-frozen]

##
## Translators: 
## - if translating while the file is in publicity-team/announcements repo,
##   please ignore the translation-check header. Publicity team will add it
##   including the correct translation-check header when moving the file 
##   to the web repo
##
## - if translating while the file is in webmaster-team/webwml repo,
##   please use the copypage.pl script to create the page in your language
##   subtree including the correct translation-check header.
##

<p>
<a href="https://amperecomputing.com/">Ampere®</a> ist eine Partnerschaft 
mit Debian eingegangen und unterstützt unsere Hardware-Infrastruktur mit 
einer Spende dreier seiner hochleistungsfähigen Arm64-Server. Die Geräte 
der Baureihe Lenovo ThinkSystem HR330A enthalten Amperes eMAG-CPU mit einem 
Arm®v8 64-Bit-Prozessor, der speziell für Cloud-Server entwickelt worden ist. 
Ihm stehen 256 GB RAM, zwei 960GB-SSDs und ein 25GbE-Dual-Port-Netzwerkadapter 
zur Verfügung.
</p>


<p>
Die gespendeten Server sind in der Universität von British Columbia, unserem 
Hosting-Partner in Vancouver, Canada, in Betrieb genommen worden. Die 
Debian-Systemadministratoren (DSA) haben auf ihnen Build-Daemons für arm64, 
armhf und armel eingerichtet, welche die Build-Daemons auf den bisherigen 
weniger leistungsfähigen Entwickler-Boards ersetzen. Den virtuellen Maschinen 
wurden nur halb so viele virtuelle CPUs zugewiesen, aber sie benötigten auf 
Amperes eMAG-System trotzdem lediglich halb so viel Zeit, um Arm*-Pakete zu 
erzeugen. Abgesehen davon erlaubt es dieses großzügige Geschenk den DSA einige 
allgemeine Debian-Dienste von der derzeitigen Infrastruktur abzuziehen und den 
anderen Debian-Teams (z. B. Continuous Integration, Qualitätssicherung usw.) 
virtuelle Maschinen bereitzustellen, wenn diese einen Zugang zur 
Arm64-Architektur benötigen.
</p>

<p>
<q>Unsere Partnerschaft mit Debian gehört zu unserer Entwicklerstrategie, die 
Open-Source-Gemeinschaften, die bereits Ampere-Server verwenden, zu stärken, 
um so das Arm64-Ökosystem weiter auszubauen und neue Anwendungen zu kreieren</q>,
so Mauri Whalen, Vizepräsident der Softwareentwicklung bei Ampere:
<q>Debian ist eine gut geführte und respektierte Gemeinschaft und wir sind 
stolz darauf, mit ihnen zusammenzuarbeiten zu dürfen.</q>
</p>

<p>
<q>Die Debian-Systemadministratoren sind Ampere für die Spende der 
carrier-grade Arm64-Server dankbar. Solche Server mit integrierten 
Standard-Verwaltungsschnittstellen wie dem Intelligent Platform Management 
Interface (IPMI), Lenovos Hardware-Garantie und der 
Unterstützerorganisation dahinter ist genau das, was sich die DSA für die 
Arm64-Architektur gewünscht haben. Diese Server sind sehr leistungsfähig und 
sehr gut ausgestattet und wir können uns vorstellen, sie neben den 
Build-Daemons auch für weitere allgemeine Dienste einzusetzen. Ich denke, dass 
sie sehr attraktiv für Cloudbetreiber sind und bin begeistert, dass sich 
Ampere Computing mit Debian zusammengetan hat.</q> - Luca Filipozzi, Debian 
System Administrator.
</p>

<p>
Nur durch die Stiftung von freiwilligem Engagement, Sach- und 
Dienstleistungen sowie Finanzmitteln ist Debian in der Lage, seiner 
Verpflichtung ein freies Betriebssystem zu schaffen, nachzukommen. Wir 
wissen die Großzügigkeit von Ampere sehr zu schätzen.
</p>

<h2>Über Ampere Computing</h2>
<p>
Ampere gestaltet die Zukunft der Hyperscale-Cloud und des Edge-Computing mit 
dem ersten Cloud-nativen Prozessor. Mit der Auslegung für die Cloud und einer  
modernen 64-Bit-Arm-Architektur verschafft Ampere seinen Kundinnen und Kunden 
den Freiraum, die Bereitstellung all ihrer Cloud-Computing-Anwendungen zu 
beschleunigen. Mit ihrer industrieführenden Cloud-Rechenleistung, 
Energieeffizienz und Skalierbarkeit sind Ampere-Prozessoren auf das weitere 
Wachstum von Cloud- und Edge-Computing zugeschnitten.
</p>


<h2>Über Debian</h2>

<p>Das Debian-Projekt wurde 1993 von Ian Murdock als wirklich freies 
Gemeinschaftsprojekt gegründet. Seitdem ist das Projekt zu einem der größten 
und einflussreichsten Open-Source-Projekte angewachsen. Tausende von 
Freiwilligen aus aller Welt arbeiten zusammen, um Debian-Software herzustellen 
und zu betreuen. Verfügbar in über 70 Sprachen und eine große Bandbreite an 
Rechnertypen unterstützend bezeichnet sich Debian als das <q>universelle 
Betriebssystem</q>.</p>

<h2>Kontaktinformationen</h2>

<p>Für weitere Informationen besuchen Sie bitte die Debian-Webseiten unter 
<a href="$(HOME)/">https://www.debian.org/</a> oder schicken eine E-Mail (in 
Englisch) an 
&lt;press@debian.org&gt;.</p>
