#use wml::debian::template title="SPARC-Portierung" NOHEADER="yes"
#include "$(ENGLISHDIR)/ports/sparc/menu.inc"
#use wml::debian::translation-check translation="f9d5abd797e762089776545824869e3e44bd2c42"
# $Id$

<h1>Debian SPARC-Portierung</h1>

<ul>
 <li><a href="#intro">Überblick</a></li>
 <li><a href="#status">Aktueller Status</a></li>
 <li><a href="#sparc64bit">Über 64-bit SPARC Unterstützung</a>
 <ul>
   <li><a href="#kernelsun4u">Kernel für sun4u übersetzen</a></li>
 </ul></li>
 <li><a href="#errata">Ergänzungen</a></li>
 <li><a href="#who">Wer sind wir? Wie kann ich helfen?</a></li>
 <li><a href="#links">Wo kann ich weitere Informationen finden?</a></li>
</ul>

<h2 id="intro">Überblick</h2>
  <p>
Diese Seiten sollen Anwendern und Debian-Entwicklern helfen, Debian
GNU/Linux auf der SPARC-Architektur laufen zu lassen. Auf diesen
Seiten werden Sie Informationen über den aktuellen Status finden,
bekannte Probleme, Informationen für und über Debian-Portierer sowie
Verweise zu weiteren Informationen.</p>

<h2 id="status">Aktueller Status</h2>

  <p>
Mit dem Ende des Supports für Debian Etch wurde auch der Support für 
pre-UltraSPARC-Maschinen eingestellt (siehe <a href="https://wiki.debian.org/Sparc32">
https://wiki.debian.org/Sparc32</a>). Seitdem wurde für den 32-Bit-Port
eine UltraSPARC CPU vorausgesetzt und ein 64-Bit-Kernel verwendet.
Dieser 32-Bit-Port wurde schließlich mit dem Ende des Supports für Debian Wheezy eingestellt.
 </p>

  <p>
Zurzeit gibt es keine offizielle Debian-Portierung für SPARC, allerdings wird eine 
komplette 64-Bit-Portierung namens sparc64 vom Debian Ports Team zur Verfügung gestellt.
  </p>

<h2 id="sparc64bit">Über die Unterstützung von 64-Bit SPARC</h2>

<p>Die Debian-SPARC-Portierung unterstützt, wie oben
erwähnt, die sun4u-(<q>Ultra</q>) und die sun4v- (Niagara CPU) Architektur. Sie
verwendet einen 64-Bit-Kernel (kompiliert mit gcc 3.3 oder neuer), doch
die meisten der Anwendungen laufen im 32-Bit-Modus. Dies wird auch <q>32-Bit
Userland</q> genannt.</p>

<p>Die Anstrengungen, Debian auf SPARC 64 (auch als <q>UltraLinux</q> bekannt) zu
portieren, ist nicht vollständig und eigenständig wie andere Portierungen.
Stattdessen ist sie als <em>Add-On</em> zur Debian SPARC-Portierung gedacht.</p>

<p>In der Tat gibt es wirklich keinen Grund, alle Anwendungen im
64-Bit-Modus laufen zu lassen. Ein vollständiger 64-Bit-Modus
erfordert einen signifikanten Verwaltungsaufwand (in Speicher- und
Plattengröße),
oftmals ohne Vorteile. Einige Anwendungen können tatsächlich davon
profitieren, im 64-Bit zu laufen, und das ist der Grund dieser Portierung.</p>


<h3 id="kernelsun4u">Kernel für sun4u übersetzen</h3>

<p>Um einen Linux-Kernel für sun4u zu übersetzen, müssen Sie den Quellcode von
Linux 2.2 oder neuer verwenden.</p>

<p>Wir empfehlen Ihnen dringend, ebenfalls das <tt>kernel-package</tt> zu
verwenden, das Ihnen bei der Installation und der Verwaltung der Kernel hilft.
Sie können einen konfigurierten Kernel mit einem Befehl (als root) übersetzen:</p>
<pre>
  make-kpkg --subarch=sun4u --arch_in_name --revision=custom.1 kernel_image
</pre>


<h2 id="errata">Ergänzungen</h2>
  <p>
Einige der allgemeinen Probleme mit deren Behebung oder Umgehung können auf
unserer <a href="problems">Ergänzungs-Seite</a> gefunden werden.</p>


<h2 id="who">Wer sind wir? Wie kann ich helfen?</h2>
  <p>
Die Debian-SPARC-Portierung ist eine verteilte Anstrengung, genau wie es
Debian auch ist. Zahllose Personen haben bei der Portierung und
Dokumentation mitgeholfen, obwohl nur eine kleine Liste von <a
href="credits">Anerkennungen</a> verfügbar ist.</p>
  <p>
Wenn Sie helfen möchten, abonnieren Sie bitte die Mailingliste
 &lt;debian-sparc@lists.debian.org&gt; wie
<a href="#links">unten beschrieben</a> und ergreifen Sie das Wort.</p>
  <p>
Registrierte Entwickler, die aktiv portieren und portierte Pakete
hochladen möchten, sollten die Portierungs-Richtlinien in der <a
href="$(DOC)/developers-reference/">Entwicklerreferenz</a> lesen
sowie die <a href="porting">SPARC-Portierungs-Seite</a>.</p>

<h2 id="links">Wo kann ich weitere Informationen finden?</h2>
  
<p>
Eine Seite im Debian Wiki enthält Informationen zur <a href="https://wiki.debian.org/Sparc64">
Debian Sparc64-Portierung</a>.
</p>

<p>
Der beste Ort, um Debian-spezifische Fragen zur SPARC-Portierung zu stellen,
ist auf der Mailingliste <a href="https://lists.debian.org/debian-sparc/">\
&lt;debian-sparc@lists.debian.org&gt;</a>.
<a
href="https://lists.debian.org/debian-sparc/">Archive</a> der Liste
sind im Web vorhanden.</p>
  <p>
Um die Liste zu abonnieren, schreiben Sie eine E-Mail an
<a href="mailto:debian-sparc-request@lists.debian.org">\
debian-sparc-request@lists.debian.org</a> mit dem Wort <q>subscribe</q>
als Betreff ohne E-Mail-Text. Alternativ tragen Sie sich im Web ein
über die
<a href="https://lists.debian.org/debian-sparc/">Mailinglisten-Abonnement-Seite</a>.</p>
  <p>
Kernel-Fragen sollten auf der Liste
&lt;sparclinux@vger.rutgers.edu&gt; gestellt werden. Abonnieren Sie sie
mit einer Mail an die Adresse <a
href="mailto:majordomo@vger.rutgers.edu">majordomo@vger.rutgers.edu</a>
und dem Mail-Text <q>subscribe sparclinux</q>.
Es gibt natürlich auch eine Red-Hat-Liste.</p>
