#use wml::debian::template title="Как да се снабдим с Дебиан"
#use wml::debian::translation-check translation="0691e0b35ed0aa5df10a4b47799051f77e519d25"
#include "$(ENGLISHDIR)/releases/images.data"

<p>Дебиан се разпространява напълно <a
href="../intro/free">свободно</a> в Интернет. Цялата дистрибуция е достъпна за
изтегляне от <a href="ftplist">огледалните сървъри</a>. Подробни указания за
инсталиране има в <a href="../releases/stable/installmanual">Ръководството за
инсталиране</a>, а бележките по изданията са достъпни <a
href="../releases/stable/releasenotes">тук</a>.</p>

<p>Тази страница съдържа варианти за инсталиране на стабилното издание. За тестовото
или нестабилното издание вижте <a href="../releases/">страницата за всички
издания</a>.</p>

<div class="line">
  <div class="item col50">
    <h2><a href="netinst">Изтегляне на инсталационен носител</a></h2>
    <p>В зависимост от използваната връзка към Интернет се препоръчва изтеглянето
       на един от следните варианти:</p>
    <ul>
      <li><a href="netinst"><strong>Малък инсталационен носител</strong></a>:
          изтегля се бързо и се записва на преносим носител.
          По време на инсталирането ще имате нужда от връзка с Интернет.
         <ul class="quicklist downlist">
           <li><a title="Изтегляне на инсталатор за 64-битови компютри, базирани на Intel или AMD"
               href="<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso">образ
               за 64-битови ПК</a></li>
           <li><a title="Изтегляне на инсталатор за 32-битови компютри, базирани на Intel или AMD"
               href="<stable-images-url/>/i386/iso-cd/debian-<current-tiny-cd-release-filename/>-i386-netinst.iso">образ
               за 32-битови ПК</a></li>
         </ul>
      </li>

      <li><a href="../CD/">По-голям инсталационен носител</a>: съдържа пълен набор пакети
          и е подходящ за инсталиране на компютри без връзка с Интернет.
         <ul class="quicklist downlist">
           <li><a title="Изтегляне на файлове .torrent на DVD за 64-битови компютри, базирани на Intel или AMD"
               href="<stable-images-url/>/amd64/bt-dvd/">торенти за 64-битови ПК (DVD)</a></li>
           <li><a title="Изтегляне на файлове .torrent на DVD за 32-битови компютри, базирани на Intel или AMD"
               href="<stable-images-url/>/i386/bt-dvd/">торенти за 32-битови ПК (DVD)</a></li>
           <li><a title="Изтегляне на файлове .torrent на компактдиск за 64-битови компютри, базирани на Intel или AMD"
               href="<stable-images-url/>/amd64/bt-cd/">торенти за 64-битови ПК (компактдиск)</a></li>
           <li><a title="Изтегляне на файлове .torrent на компактдиск за 32-битови компютри, базирани на Intel или AMD"
               href="<stable-images-url/>/i386/bt-cd/">торенти за 32-битови ПК (компактдиск)</a></li>
         </ul>
      </li>
    </ul>
  </div>

  <div class="item col50 lastcol">
    <h2><a href="https://cloud.debian.org/images/cloud/">Образ на Debian за облак</a></h2>
    <p>Официални <a href="https://cloud.debian.org/images/cloud/"><strong>образи за облак</strong></a>, създадени от облачния екип, подходящи за:</p>
    <ul>
      <li>OpenStack, формат qcow2 или raw.
      <ul class="quicklist downlist">
	   <li>64-битови AMD/Intel (<a title="Образ за OpenStack за 64-битови AMD/Intel qcow2" href="https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-generic-amd64.qcow2">формат qcow2</a>, <a title="OpenStack image for 64-bit AMD/Intel raw" href="https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-generic-amd64.raw">формат raw</a>)</li>
       <li>64-битови ARM (<a title="Образ за OpenStack за for 64-битови ARM qcow2" href="https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-generic-arm64.qcow2">формат qcow2</a>, <a title="OpenStack image for 64-bit ARM raw" href="https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-generic-arm64.raw">формат raw</a>)</li>
	   <li>64-битови Little Endian PowerPC (<a title="Образ за OpenStack за 64-битови Little Endian PowerPC qcow2" href="https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-generic-ppc64el.qcow2">формат qcow2</a>, <a title="OpenStack image for 64-bit Little Endian PowerPC raw" href="https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-generic-ppc64el.raw">формат raw</a>)</li>
      </ul>
      </li>
      <li>Amazon EC2, като машинен образ или чрез AWS Marketplace.
	   <ul class="quicklist downlist">
	    <li><a title="Машинни образи" href="https://wiki.debian.org/Cloud/AmazonEC2Image/">Машинни образи</a></li>
	    <li><a title="AWS Marketplace" href="https://aws.amazon.com/marketplace/seller-profile?id=4d4d4e5f-c474-49f2-8b18-94de9d43e2c0">AWS Marketplace</a></li>
	   </ul>
      </li>
      <li>Microsoft Azure, в Azure Marketplace.
	   <ul class="quicklist downlist">
	    <li><a title="Debian 11 в Azure Marketplace" href="https://azuremarketplace.microsoft.com/en-us/marketplace/apps/debian.debian-11?tab=PlansAndPrice">Debian 11 ("Bullseye")</a></li>
        <li><a title="Debian 10 в Azure Marketplace" href="https://azuremarketplace.microsoft.com/en-us/marketplace/apps/debian.debian-10?tab=PlansAndPrice">Debian 10 ("Buster")</a></li>
	   </ul>
      </li>
    </ul>
 </div>
</div>

<div class="line">
  <div class="item col50">
    <h2><a href="../CD/vendors/">Купуване на комплект компактдискове или DVD с Дебиан</a></h2>

   <p>
     Цената при много от разпространителите е по-малко от $5 плюс цената на
     доставката (проверете уеб-страницата на разпространителя за информация
     относно международни пратки).
     <br />
     Някои от <A HREF="../doc/books">книгите за Дебиан</A> включват компактдиск
     с Дебиан.
   </p>

   <p>Ето и основните предимства на използването на комплект с дискове:</p>
devel/website/uptodate
   <ul>
     <li>Инсталацията от дискове е по-лесна.</li>
     <li>Дебиан може да се инсталира на компютри без връзка с Интернет.</li>
     <li>Дебиан може да се инсталира на повече от един компютър без да е
         необходимо изтегляне на индивидуалните пакети.</li>
     <li>Дискът може да се използва за възстановяване или поправка на повредена система Дебиан.</li>
   </ul>

  <h2><a href="pre-installed">Купуване на компютър с инсталирана
  Дебиан</a></h2>

   <p>Този вариант има редица предимства:</p>

   <ul>
     <li>Не се занимавате с инсталиране.</li>
     <li>Операционната система е настроена за правилна работа с хардуерните
         компоненти.</li>
     <li>Доставчикът вероятно предлага и техническа поддръжка.
   </ul>
  </div>

  <div class="item col50 lastcol">
     <h2><a href="../CD/live/">Изпробване на Дебиан преди инсталиране</a></h2>
     <p>
      Можете да изпробвате Дебиан като заредите от специален „жив“ носител без да
      инсталирате нищо на компютъра. В удобен за вас момент можете да пристъпите
      към инсталиране от живия носител (от версия 10/Buster инсталирането от живата
      система използва лесния <a href="https://calamares.io">инсталатор
      Calamares</a>). Прочетете <a href="../CD/live#choose_live">още подробности</a>
      за да проверите дали този начин е подходящ за вас.
     </p>
     <ul class="quicklist downlist">
        <li><a title="Изтегляне на .torrent за 64-битови компютри, базирани на Intel или AMD" href="<live-images-url/>/amd64/bt-hybrid/">торенти за 64-битови ПК</a></li>
        <li><a title="Изтегляне на файл .torrent за 32-битови компютри, базирани на Intel или AMD" href="<live-images-url/>/i386/bt-hybrid/">торенти за 32-битови ПК</a></li>
     </ul>
  </div>

</div>

<div id="firmware_nonfree" class="important">
<p>
Ако някои от хардуерните компоненти на вашия компютър <strong>изискват зареждането
на затворен фърмуер</strong>, може да използвате
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/stable/current/">\
комплектите с популярни пакети с фърмуер</a> или да изтеглите <strong>неофициален</strong> образ, включващ <strong>затворен</strong> фърмуер. Инструкциите за използването
на комплектите с фърмуер и обща информация относно зареждането на фърмуер по време на
инсталацията има в <a href="../releases/stable/amd64/ch06s04">Ръководството за инсталиране</a>.
</p>
<p>
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current/">неофициални образи за инсталиране на
<q>стабилното</q> издание с включен фърмуер</a>
</p>
</div>
