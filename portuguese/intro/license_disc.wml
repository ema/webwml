#use wml::debian::template title="Comparação de licenças de software"
#use wml::debian::translation-check translation="a2d057aa44562ddcc643379de20b7fc2c0c7f9e4"

<P><STRONG>******Este documento está em desenvolvimento*******</STRONG>

<P>As pessoas que acompanham o Software Livre tendem a desenvolver opiniões
muito fortes sobre licenças. Os iniciantes não se preocupam com elas,
já que estão mais preocupados(as) em acabar seu trabalho e não entendem
as implicações de longo prazo de se escolher um software com uma licença
em vez de outra (seria de se estranhar pessoas que entendessem as
nuances do licenciamento e que não tivessem opiniões fortes sobre
o assunto).

<P>Através dos anos, muitas licenças ganharam destaque ao oferecer aos(às)
autores(as) de software um tipo de controle sobre sua criação que, como
desenvolvedores(as), sempre desejaram. Mas é ainda comum encontrar softwares
que não explicitam os direitos de autor, ou que contenham uma licença incomum,
elaborada pelo(a) próprio(a) autor(a). Esta última situação pode ser muito
incômoda para os distribuidores de software (tanto on-line quanto pessoas que
criam CDs), pois muitas dessas licenças contêm
<a href="#mistakes">erros comuns</a> que tornam o software difícil de
distribuir.

<P>O que segue é uma lista de licenças de software, livres (abertas) e comuns,
com alguns de seus pontos bons e ruins.
Apenas os pontos relevantes à discussão são mostrados. Além disso, muitos
pontos são listados sob o título "BOM/MAU". Note que esses pontos podem
ser bons ou maus a depender do seu ponto de vista.

<UL>
<LI>A <A HREF="https://www.gnu.org/">Licença Pública Geral GNU (GPL)</A>.
	<BR>
	<B>RESUMO:</B> código-fonte deve ser disponibilizado; software pode ser
	vendido; trabalhos derivados devem usar a mesma licença
	<BR>
	<B>BOM:</B> Há uma boa razão para essa ser a licença mais usada para
	software livre (aberto). Ela faz um bom trabalho de proteção dos
	direitos dos(as) desenvolvedores(as) de software, e a disponibilidade de
	código-fonte garante que os(as) usuários(as) não terão preocupações em
        perder suporte no futuro.
	<BR>
	<B>BOM/MAU:</B> Software que é lançado sob a GPL não pode ser
	incorporado em software comercial. Isto só será realmente uma coisa ruim
	dependendo do seu ponto de vista. As pessoas que desenvolvem software
	comercial geralmente se sentem frustradas quando há uma solução
	disponível, mas que não pode ser usada por causa de conflitos no
	licenciamento. Claro, nada os impede de contatar o(a) autor(a) e ver se
	podem comprar uma versão usando uma licença diferente. A maioria das
	pessoas que lançam seu software usando a GPL não considera essas
	restrições ruins, porque a licença permite que se use e se melhore o
        software, e ao mesmo tempo previne (para todos os propósitos práticos)
	que outras pessoas façam dinheiro a partir do seu trabalho duro sem uma
	permissão.

<LI>Licença Artística
	<A HREF="http://language.perl.com/misc/Artistic.html">http://language.perl.com/misc/Artistic.html</A>.
	<BR>
	<B>RESUMO:</B>
	<BR>
	<B>BOM:</B>
	<BR>
	<B>MAU:</B>

<LI><A HREF="https://opensource.org/licenses/BSD-3-Clause">Licença estilo BSD</A>.
	<BR>
	<B>RESUMO:</B> Binários e código-fonte devem conter a licença; anúncios
	devem reconhecer os(as) desenvolvedores(as) listados(as) na licença
	<BR>
	<B>BOM/MAU:</B> Empresas que geralmente gostam dessa licença querem um
	executável disponível (possivelmente de graça) sem lançar o
	código-fonte. Um bom exemplo é uma empresa que quer lançar um driver
	para uma placa de vídeo. Os(as) defensores do Código Aberto
	preferem que a empresa libere as especificações de hardware de qualquer
	maneira. Um exemplo é o desenvolvimento dos drivers para o XFree86;
	os melhores drivers são aqueles escritos com o código-fonte disponível.
	As empresas fazem má publicidade quando seus produtos com drivers
        proprietários são lentos e bugados. As empresas também podem
	economizar nos custos de desenvolvimento ao deixar que outros
        desenvolvam o driver para eles.
	<BR>
	<B>BOM/MAU:</B> Qualquer um pode pegar o código-fonte, modificá-lo e
	disponibilizar o resultado, sem publicar as mudanças feitas. É uma
        questão de preferência pessoal achar que isso é bom ou ruim.
</UL>

<HR>

<P><A NAME="mistakes">Alguns erros comuns em licenças personalizadas</A>:
<UL>
<LI>Não permitir ou restringir a venda com fins lucrativos do software.
	Isso torna difícil distribuir o software em CD. As pessoas normalmente
	cometem o erro de usar termos que não são bem definidos, como 'custo
	razoável'. É melhor simplesmente usar uma das licenças mencionadas
	acima, pois elas conseguem o mesmo objetivo sem fazer uso de tais
	termos. Por exemplo, ao permitir que qualquer um distribua o software, a
	GPL mantém os custos baixos devido a dinâmicas de mercado. Se alguém
	está vendendo um CD com uma margem de lucro alta, não durará muito tempo
	até que competidores entrem no mercado e vendam por um preço mais baixo.
	<BR>Nota: a Licença Artística usa a expressão 'taxa de cópia razoável',
	mas a qualifica o termo na tentativa de torná-lo menos vago.
<LI>Não permitir a distribuição de versões modificadas do software.
	Isso atrapalha a distribuição de software por certos grupos. Por
	exemplo, o Debian distribui software compilado, mas é comum a
	necessidade de modificar o código-fonte para conseguir compilá-lo para
        outros propósitos, ou para fazer com que ele seja compatível com os
	<A HREF="ftp://tsx-11.mit.edu/pub/linux/docs/linux-standards/fsstnd/">FSSTND</A>
        ("Linux File System Standard" ou Padrão do Sistema de Arquivos Linux).
	Contudo, ao fazer isso, não estamos mais permitidos a distribuí-lo.
<LI>Requerer que todas as mudanças feitas no software sejam reportadas ao(à)
        autor(a).
	Apesar de ser uma boa ideia relatar mudanças/melhorias para o(a)
	autor(a), de modo que as alterações possam ser melhor distribuídas,
	fazer disso uma condição pode causar problemas. Quantas pessoas sabem
	onde estarão daqui há 5 anos? Simplesmente mude a condição para
        'quaisquer mudanças no software podem ser relatadas ao(a) autor(a)'.
        A maioria das pessoas o farão.
	<BR>Esta cláusula é normalmente incluída para prevenir o desenvolvimento
	de projetos derivados. A história mostra que o desenvolvimento no código
        original não pode parar, pois se isso acontecer, as derivações só
	prosperarão se alguma outra força apoiar a derivação.
<LI>Dizer que o software é de domínio público, mas incluir limitações (como não
	permitir a venda para fins lucrativos). Ou algo está em domínio público
	ou não está - não há meio termo. Tais licenças não têm sentido e é
	possível que essas condições extras não sejam consideradas em juízo.
</UL>
