#use wml::debian::template title="As pessoas: quem somos e o que fazemos" MAINPAGE="true"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="5eef343480ec39f5bb8db5e4e69bbb9e8b926c9b"

# tradutores(as): parte do texto foi retirado de /intro/about.wml

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#history">Como tudo começou</a>
    <li><a href="#devcont">Desenvolvedores(as) e contribuidores(as)</a>
    <li><a href="#supporters">Pessoas e organizações que apoiam o Debian</a>
    <li><a href="#users">Usuários(as) Debian</a>
  </ul>
</div>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span>Já que muitas pessoas
perguntaram: o Debian é pronunciado
<span style="white-space: nowrap;">/&#712;de.bi.&#601;n/.</span>
Ele tem o nome do criador do Debian, Ian Murdock, e sua esposa, Debra.</p>
</aside>

<h2><a id="history">Como tudo começou</a></h2>

<p>Foi em agosto de 1993, quando Ian Murdock começou a trabalhar em um novo
sistema operacional que seria feito abertamente, no espírito do Linux e GNU.
Ele enviou um convite aberto a outros(as) desenvolvedores(as) de software,
pedindo-lhes que contribuíssem com uma distribuição de software baseada no
kernel Linux, que era relativamente novo naquela época. O Debian foi feito para
ser cuidadosa e conscienciosamente construído, e para ser mantido e apoiado
com cuidado semelhante, abraçando um design aberto, contribuições e suporte da
comunidade do Software Livre. </p>

<p>Começou como um grupo pequeno e bem unido de hackers de Software Livre e
gradualmente cresceu para se tornar uma grande e bem organizada comunidade de
desenvolvedores(as), colaboradores(as) e usuários(as).</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="$(DOC)/manuals/project-history/">Leia a história completa</a></button></p>

<h2><a id="devcont">Desenvolvedores(as) e contribuidores(as)</a></h2>

<p>O Debian é uma organização totalmente voluntária. Mais de mil
desenvolvedores(as) ativos(as) espalhados(as)
<a href="$(DEVEL)/developers.loc">por todo o mundo</a> trabalham no Debian em
seu tempo livre. Poucos(as) de nós realmente nos conhecemos pessoalmente.
Como alternativa, nos comunicamos principalmente por e-mail (listas de discussão
em <a href="https://lists.debian.org/">lists.debian.org</a>) e IRC
(canal #debian-br em português, ou #debian em inglês, no irc.debian.org).
</p>

<p>
A lista completa de membros(as) oficiais do Debian pode ser encontrada em
<a href="https://nm.debian.org/members">nm.debian.org</a>, e
<a href="https://contributors.debian.org">contributors.debian.org</a>
mostra uma lista de todos(as) os(as) contribuidores(as) e equipes que trabalham
na distribuição Debian. </p> 

<p>
O Projeto Debian tem uma <a href="organization">estrutura organizada</a>
cuidadosamente. Para mais informações sobre como o projeto Debian parece
internamente, por favor visite o
<a href="$(DEVEL)/">canto dos(as) desenvolvedores(as)</a>.</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="philosophy">Leia mais sobre nossa filosofia</a></button></p>

<h2><a id="supporters">Pessoas e organizações que apoiam o Debian</a></h2>

<p>Além de desenvolvedores(as) e contribuidores(as), muitas outras pessoas e
organizações fazem parte da comunidade Debian:</p> 

<ul>
  <li><a href="https://db.debian.org/machines.cgi">Patrocinadores(as) de hospedagem e hardware</a></li>
  <li><a href="../mirror/sponsors">Patrocinadores(as) de espelhos</a></li>
  <li><a href="../partners/">Parceiros(as) de desenvolvimento e serviço</a></li>
  <li><a href="../consultants">Consultores(as)</a></li>
  <li><a href="../CD/vendors">Vendedores(as) de mídias de instalação do Debian</a></li>
  <li><a href="../distrib/pre-installed">Vendedores(as) que oferecem computadores com Debian pré-instalado</a></li>
  <li><a href="../events/merchandise">Vendedores(as) de merchandising</a></li>
</ul>

<h2><a id="users">Usuários(as) Debian</a></h2>

<p>
O Debian é utilizado por um amplo número de organizações, grandes e pequenas,
bem como por milhares de pessoas. Veja nossa página
<a href="../users/">quem está usando o Debian?</a> para uma lista de
organizações educacionais, comerciais e sem fins lucrativos, bem como agências
governamentais que enviaram descrições curtas de como e porque usam o Debian.
</p>
