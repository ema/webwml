#use wml::debian::template title="보안 정보" GEN_TIME="yes" MAINPAGE="true"
#use wml::debian::toc
#use wml::debian::recent_list_security
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="c99908e0effa46ed240da78ee4a1b5ea8e13fcd4"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
<li><a href="#keeping-secure">데비안 시스템을 안전하게 지키기</a></li>
<li><a href="#DSAS">최근 권고</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> 데비안은 보안을 매우 중요하게 생각합니다.
우리는 모든 보안 문제를 다루고 합리적인 시간 내에 수정하도록 보장합니다.</p>
</aside>

<p>
경험상 <q>숨기는 것에 의한 보안(security by obscruity)</q>은 동작하지 않는 것으로 나타났습니다.
공개는 보안 문제에 대해 더 빠르고 좋은 해결책을 제공합니다.
그런 맥락에서, 이 페이지는 데비안의 상태를 다양한 알려진 보안 구멍과 관련하여 다루는데,
잠재적으로 데비안에 영향을 줄 수 있습니다.
</p>

<p>
데비안 프로젝트는 많은 보안 권고 사항을 다른 무료 소프트웨어 공급업체와 조정하며, 결과적으로 이러한 권고 사항들은 취약성이 공개된 날 게시됩니다.
</p>
  
# "reasonable timeframe" might be too vague, but we don't have 
# accurate statistics. For older (out of date) information and data
# please read:
# https://www.debian.org/News/2004/20040406  [ Year 2004 data ]
# and (older)
# https://people.debian.org/~jfs/debconf3/security/ [ Year 2003 data ]
# https://lists.debian.org/debian-security/2001/12/msg00257.html [ Year 2001]
# If anyone wants to do up-to-date analysis please contact me (jfs)
# and I will provide scripts, data and database schemas.

<p>
데비안은 보안 표준화 노력에도 참여합니다:
</p>

<ul>
  <li><a href="#DSAS">데비안 보안 권고</a>는 <a href="cve-compatibility">CVE-호환</a> (<a href="crossreferences">cross references</a> 참조)입니다.</li>
  <li>Debian is represented in the Board of the <a href="https://oval.cisecurity.org/">Open Vulnerability Assessment Language</a> project.</li>
</ul>

<h2><a id="keeping-secure">데비안 시스템을 안전하게 지키기</a></h2>

<p>
최근 데비안 보안 권고를 받으려면, 
<a href="https://lists.debian.org/debian-security-announce/">\
debian-security-announce</a> 메일링 리스트를 구독하세요.
</p>

<p>
꼭대기에서 <a href="https://packages.debian.org/stable/admin/apt">APT</a>를 써서 최근 보안 업데이트를 받습니다. 데비안 운영체제를 보안 패치를 가진 최근으로 지키려면 아래 행을 <code>/etc/apt/sources.list</code> 파일에 추가하십시오:
</p>

<pre>
deb http://security.debian.org/debian-security <current_release_security_name> main contrib non-free
</pre>

<p>
변화를 저장하고, 아래 두 명령을 실행하여 보류 업데이트를 다운로드하고 설치하세요.
</p>

<pre>
apt-get update &amp;&amp; apt-get upgrade
</pre> 

<p>
보안 아카이브는 일반적인 <a href="https://ftp-master.debian.org/keys.html">Debian archive keys</a>로 서명합니다.
</p>

<p>
데비안 보안 이슈에 대한 더 많은 정보 보실 곳은 우리 FAQ 및 문서:
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="faq">보안 FAQ</a></button> <button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="../doc/user-manuals#securing">데비안을 안전하게</a></button></p>

<aside class="light">
  <span class="fa fa-rss fa-5x"></span>
</aside>

<h2><a id="DSAS">최근 권고</a></h2>

<p>이 웹 페이지는 보안 권고와 <a href="https://lists.debian.org/debian-security-announce/">\
debian-security-announce</a>에 게시된 요약 아카이브를 포함합니다

<p>
<:= get_recent_security_list( '1m', '6', '.', '$(ENGLISHDIR)/security' ) :>
</p>

{#rss#:
<link rel="alternate" type="application/rss+xml"
 title="Debian Security Advisories (titles only)" href="dsa">
<link rel="alternate" type="application/rss+xml"
 title="Debian Security Advisories (summaries)" href="dsa-long">
:#rss#}

<p>
최근 데비안 보안 권고는 <a href="dsa">RDF files</a>에서도 가능. 해당 권고의 첫 구절인 들어간 좀 더 <a href="dsa-long">긴 버전</a>도 있습니다. 그 방법은 권고에 대해 쉽게 알 수 있습니다.
</p>

#include "$(ENGLISHDIR)/security/index.include"
<p>오래된 보안 권고도 가능:
<:= get_past_sec_list(); :>

<p>데비안 배포판은 모든 보안 문제에 취약하지는 않습니다. 
<a href="https://security-tracker.debian.org/">Debian Security Tracker</a>는 데비안 패키지의 취약 상태에 대한 모든 정보를 모읍니다.
CVE 이름 또는 패키지로 검색할 수 있습니다.
</p>
