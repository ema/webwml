# templates webwml Catalan template.
# Copyright (C) 2002, 2003, 2004, 2005 Free Software Foundation, Inc.
# Jordi Mallach <jordi@debian.org>, 2002, 2003.
# Guillem Jover <guillem@debian.org>, 2004-2007, 2018.
#
msgid ""
msgstr ""
"Project-Id-Version: webwml\n"
"PO-Revision-Date: 2018-07-15 02:22+0200\n"
"Last-Translator: Guillem Jover <guillem@debian.org>\n"
"Language-Team: Catalan <debian-l10n-catalan@lists.debian.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/template/debian/wnpp.wml:8
msgid "No requests for adoption"
msgstr "Sense demandes d'adopció"

#: ../../english/template/debian/wnpp.wml:12
msgid "No orphaned packages"
msgstr "Sense paquets deixats orfes"

#: ../../english/template/debian/wnpp.wml:16
msgid "No packages waiting to be adopted"
msgstr "Sense paquets esperant ser adoptats"

#: ../../english/template/debian/wnpp.wml:20
msgid "No packages waiting to be packaged"
msgstr "Sense paquets esperant ser empaquetats"

#: ../../english/template/debian/wnpp.wml:24
msgid "No Requested packages"
msgstr "Sense paquets demanats"

#: ../../english/template/debian/wnpp.wml:28
msgid "No help requested"
msgstr "Sense demandes d'ajuda"

#. define messages for timespans
#. first the ones for being_adopted (ITAs)
#: ../../english/template/debian/wnpp.wml:34
msgid "in adoption since today."
msgstr "en adopció des d'avui."

#: ../../english/template/debian/wnpp.wml:38
msgid "in adoption since yesterday."
msgstr "en adopció des d'ahir."

#: ../../english/template/debian/wnpp.wml:42
msgid "%s days in adoption."
msgstr "%s dies en adopció."

#: ../../english/template/debian/wnpp.wml:46
msgid "%s days in adoption, last activity today."
msgstr "%s dies en adopció, última activitat avui."

#: ../../english/template/debian/wnpp.wml:50
msgid "%s days in adoption, last activity yesterday."
msgstr "%s dies en adopció, última activitat ahir."

#: ../../english/template/debian/wnpp.wml:54
msgid "%s days in adoption, last activity %s days ago."
msgstr "%s dies en adopció, última activitat fa %s dies."

#. timespans for being_packaged (ITPs)
#: ../../english/template/debian/wnpp.wml:60
msgid "in preparation since today."
msgstr "en preparació des d'avui."

#: ../../english/template/debian/wnpp.wml:64
msgid "in preparation since yesterday."
msgstr "en preparació des d'ahir."

#: ../../english/template/debian/wnpp.wml:68
msgid "%s days in preparation."
msgstr "%s dies en preparació."

#: ../../english/template/debian/wnpp.wml:72
msgid "%s days in preparation, last activity today."
msgstr "%s dies en preparació, última activitat avui."

#: ../../english/template/debian/wnpp.wml:76
msgid "%s days in preparation, last activity yesterday."
msgstr "%s dies en preparació, última activitat ahir."

#: ../../english/template/debian/wnpp.wml:80
msgid "%s days in preparation, last activity %s days ago."
msgstr "%s dies en preparació, última activitat fa %s dies."

#. timespans for request for adoption (RFAs)
#: ../../english/template/debian/wnpp.wml:85
msgid "adoption requested since today."
msgstr "en petició d'adopció des d'avui."

#: ../../english/template/debian/wnpp.wml:89
msgid "adoption requested since yesterday."
msgstr "en petició d'adopció des d'ahir."

#: ../../english/template/debian/wnpp.wml:93
msgid "adoption requested since %s days."
msgstr "en petició d'adopció des de fa %s dies."

#. timespans for orphaned packages (Os)
#: ../../english/template/debian/wnpp.wml:98
msgid "orphaned since today."
msgstr "orfe des d'avui."

#: ../../english/template/debian/wnpp.wml:102
msgid "orphaned since yesterday."
msgstr "orfe des d'ahir."

#: ../../english/template/debian/wnpp.wml:106
msgid "orphaned since %s days."
msgstr "orfe des de fa %s dies."

#. time spans for requested (RFPs) and help requested (RFHs)
#: ../../english/template/debian/wnpp.wml:111
msgid "requested today."
msgstr "demanat avui."

#: ../../english/template/debian/wnpp.wml:115
msgid "requested yesterday."
msgstr "demanat ahir."

#: ../../english/template/debian/wnpp.wml:119
msgid "requested %s days ago."
msgstr "demanat %s dies enrere."

#: ../../english/template/debian/wnpp.wml:133
msgid "package info"
msgstr "informació del paquet"

#. popcon rank for RFH bugs
#: ../../english/template/debian/wnpp.wml:139
msgid "rank:"
msgstr "rang:"
